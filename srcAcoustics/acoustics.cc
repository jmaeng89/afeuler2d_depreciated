// Wrapper file for barotropic acoustics equations for AF
// 
// 8 May 2015 - Initial creation (Maeng)
// 31 March 2016 - Clean up acoustics_ (Maeng)
//
//
//
/* Include required C++ libraries. */
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <fstream>
#include <sstream>

using namespace std;

// Include header files.
#include "Mesh2D.h"

// prototypes
extern "C" 
{
    void testcarray_(int *nCells, 
                int *nEqns,
                int *nNodes,
                double *dtIn,
                double nodeDataN[],
                double nodeDataN2[][4],
                int nodeBC[]);

    void acoustics_(int *iter,
                    int *nCells, 
                    int *nDim,
                    int *nEqns,
                    int *nNodes,
                    int *nEdges,
                    int *maxNodesPerCell,
                    int *maxNodesPerFace,
                    int *maxFacesPerCell,
                    double *dtIn,
                    double *nodeDataN, 
                    double *edgeDataN,
                    double *primAvgN,
                    double *nodeDataH, 
                    double *edgeDataH,
                    double *nodeData, 
                    double *edgeData,
                    double *nodeCoord,
                    double *edgeCoord,
                    int *cellNodes,
                    int *cellFaces,
                    int *acNodeCells,
                    int *acEdgeCells,
                    int *nodeBC,
                    int *edgeBC);

    void acousticsnobc_(int *iter,
                        int *nCells, 
                        int *nDim,
                        int *nEqns,
                        int *nNodes,
                        int *nEdges,
                        int *maxNodesPerCell,
                        int *maxNodesPerFace,
                        int *maxFacesPerCell,
                        double *dtIn,
                        double *nodeData, 
                        double *edgeData,
                        double *nodeSignal,
                        double *edgeSignal,
                        double *nodeCoord,
                        double *edgeCoord,
                        int *cellNodes,
                        int *cellFaces);

    void isentropicacoustics_(int *iter,
                              int *nCells, 
                              int *nDim,
                              int *nEqns,
                              int *nNodes,
                              int *nEdges,
                              int *maxNodesPerCell,
                              int *maxNodesPerFace,
                              int *maxFacesPerCell,
                              double *dtIn,
                              double *nodeDataN, 
                              double *edgeDataN,
                              double *primAvgN,
                              double *nodeDataH, 
                              double *edgeDataH,
                              double *nodeData, 
                              double *edgeData,
                              double *nodeCoord,
                              double *edgeCoord,
                              int *cellNodes,
                              int *cellFaces,
                              int *acNodeCells,
                              int *acEdgeCells,
                              int *nodeBC,
                              int *edgeBC); 

}

void acoustics_(int *iter,
                int *nCells, 
                int *nDim,
                int *nEqns,
                int *nNodes,
                int *nEdges,
                int *maxNodesPerCell,
                int *maxNodesPerFace,
                int *maxFacesPerCell,
                double *dtIn,
                double *nodeDataN, // global node data at time step n
                double *edgeDataN, // global edge data at time step n
                double *primAvgN,  // global cell averages at time step n
                double *nodeDataH,  // global node data at n+0.5*dt
                double *edgeDataH,  // global edge data at n+0.5*dt
                double *nodeData,  // global node data at n+dt
                double *edgeData,  // global edge data at n+dt
                double *nodeCoord, // global mesh node coordinate
                double *edgeCoord, // global mesh edge coordinate
                int *cellNodes,    // global node index in a cell
                int *cellFaces,    // global face index in a cell
                int *acNodeCells,  // global cells that neighbor global node
                int *acEdgeCells,  // global cells that neighbor global edge
                int *nodeBC,       // global node boundary condition flag
                int *edgeBC        // global edge boundary condition flag
                )       
{
    // set variables consistent with C++ code 
    int citer = *iter;           // iteration number
    int cnCells = *nCells;       // number of cells  
    int cnDim = *nDim;           // number of dimensions
    int cnEqns = *nEqns;         // number of equations 
    int cnNodes = *nNodes;       // total number of nodes 
    int cnEdges = *nEdges;       // total number of edges 
    int cmaxNodesPerCell = *maxNodesPerCell; // max number of nodes per cell
    int cmaxFacesPerCell = *maxFacesPerCell; // max number of faces per cell
    int cmaxNodesPerFace = *maxNodesPerFace; // max number of nodes per face
    double dt = *dtIn;           // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[cnEqns];
    dA2d_t1 cNodeDataN = (dA2d_t1) nodeDataN;
    dA2d_t1 cNodeDataH = (dA2d_t1) nodeDataH;
    dA2d_t1 cNodeData = (dA2d_t1) nodeData;    // Use this data as initial conditions
    dA2d_t1 cEdgeDataN = (dA2d_t1) edgeDataN;
    dA2d_t1 cEdgeDataH = (dA2d_t1) edgeDataH;
    dA2d_t1 cEdgeData = (dA2d_t1) edgeData;    // Use this data as initial conditions
    dA2d_t1 cPrimAvgN = (dA2d_t1) primAvgN;
    typedef double (*dA2d_t2)[cnDim];
    dA2d_t2 cNodeCoord = (dA2d_t2) nodeCoord;
    dA2d_t2 cEdgeCoord = (dA2d_t2) edgeCoord;
    typedef double (*dA2d_t3)[2*cnEqns];
    typedef int (*iA2d_t1)[cmaxNodesPerCell];
    iA2d_t1 cCellNodes = (iA2d_t1) cellNodes;
    typedef int (*iA2d_t2)[cmaxFacesPerCell];
    iA2d_t2 cCellFaces = (iA2d_t2) cellFaces;
    typedef int (*iA3d_t1)[cnNodes][2];
    iA3d_t1 cAcNodeCells = (iA3d_t1) acNodeCells;
    typedef int (*iA3d_t2)[cnEdges][2];
    iA3d_t2 cAcEdgeCells = (iA3d_t2) acEdgeCells;
    //int cNodeBC = *nodeBC; 
    //int cEdgeBC = *edgeBC; 
    
    // local variables
    int iCell, iNode, jNode, iEdge, gNode, cgNode, gFace, cgFace, gEdge, cgEdge; 
    int maxelem = 10;  // number of cells that neighbor the same node, unstructured grid 
    //int maxelem = 6;  // number of cells that neighbor the same node, fixed for structured grid 

    // Initialize mesh elements
    Mesh2D *E;   // full time step
    Mesh2D *EH;  // half time step              
    E = new Mesh2D [cnCells];
    EH = new Mesh2D [cnCells];
    
    // set GLOBAL NODE coordinate data
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++ ){
            gNode = cCellNodes[iCell][iNode];   // global node
            cgNode = gNode - 1;                 // c index style of gNode
            E[iCell].nx[iNode] = cNodeCoord[cgNode][0]; 
            E[iCell].ny[iNode] = cNodeCoord[cgNode][1]; 
            E[iCell].nid[iNode] = cgNode; // global node number
        }
        E[iCell].InitAngle();
        EH[iCell].CopyGeometry(E[iCell]);
    }

    // set ELEMENT reconstruction states
    double dprime, uprime, vprime, pprime, pH;
    int fortiEdge;   
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        // initialize elements 
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++){
            gNode = cCellNodes[iCell][iNode];
            cgNode = gNode - 1;                 // c index style of gNode
            // full time step
            dprime = cNodeData[cgNode][0];
            uprime = cNodeData[cgNode][1];
            vprime = cNodeData[cgNode][2];
            pprime = cNodeData[cgNode][3];
            E[iCell].Q[2*iNode] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iNode].setgas();

            // half time step
            dprime = cNodeDataH[cgNode][0];
            uprime = cNodeDataH[cgNode][1];
            vprime = cNodeDataH[cgNode][2];
            pprime = cNodeDataH[cgNode][3];
            EH[iCell].Q[2*iNode] = Euler2D(dprime,uprime,vprime,pprime);
            EH[iCell].Q[2*iNode].setgas();

        }
        for ( iEdge = 0; iEdge < cmaxFacesPerCell; iEdge++){
            gEdge = cCellFaces[iCell][iEdge];
            cgEdge = gEdge - 1;                 // c index style of gEdge
            // full time step
            dprime = cEdgeData[cgEdge][0];
            uprime = cEdgeData[cgEdge][1];
            vprime = cEdgeData[cgEdge][2];
            pprime = cEdgeData[cgEdge][3];
            E[iCell].Q[2*iEdge+1] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iEdge+1].setgas();

            // half time step
            dprime = cEdgeDataH[cgEdge][0];
            uprime = cEdgeDataH[cgEdge][1];
            vprime = cEdgeDataH[cgEdge][2];
            pprime = cEdgeDataH[cgEdge][3];
            EH[iCell].Q[2*iEdge+1] = Euler2D(dprime,uprime,vprime,pprime);
            EH[iCell].Q[2*iEdge+1].setgas();
    
        }
        // we assume Lax-Wendroff expansion for acoustic stage, meaning
        // that all values are not part of conservative update. 
        // initialize average
        E[iCell].InitAverage();
        EH[iCell].InitAverage();
        dprime = cPrimAvgN[iCell][0];
        uprime = cPrimAvgN[iCell][1];
        vprime = cPrimAvgN[iCell][2];
        pprime = cPrimAvgN[iCell][3];
        //E[iCell].Qavg = Euler2D(dprime,uprime,vprime,pprime);
        //EH[iCell].Qavg = Euler2D(dprime,uprime,vprime,pprime);
        //E[iCell].SetBubbleFunction();
        //EH[iCell].SetBubbleFunction();
    }

    // global state variables
    Euler2D *QNode;
    Euler2D *QHNode;            // Global nodal states at half timestep
    Euler2D *QEdge;
    Euler2D *QHEdge;            // Global edge states at half timestep
    QNode = new Euler2D [cnNodes]; // global node state variables
    QHNode = new Euler2D [cnNodes];
    QEdge = new Euler2D [cnEdges]; // global edge state variables
    QHEdge = new Euler2D [cnEdges];
    for ( iNode = 0; iNode < cnNodes; iNode++ ){
        // full time
        dprime = cNodeData[iNode][0];
        uprime = cNodeData[iNode][1];
        vprime = cNodeData[iNode][2];
        pprime = cNodeData[iNode][3];
        QNode[iNode] = Euler2D(dprime,uprime,vprime,pprime);
        QNode[iNode].setgas();
        // half time 
        dprime = cNodeDataH[iNode][0];
        uprime = cNodeDataH[iNode][1];
        vprime = cNodeDataH[iNode][2];
        pprime = cNodeDataH[iNode][3];
        QHNode[iNode] = Euler2D(dprime,uprime,vprime,pprime);
        QHNode[iNode].setgas();    
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ){
        // full time
        dprime = cEdgeData[iEdge][0];
        uprime = cEdgeData[iEdge][1];
        vprime = cEdgeData[iEdge][2];
        pprime = cEdgeData[iEdge][3];
        QEdge[iEdge] = Euler2D(dprime,uprime,vprime,pprime);
        QEdge[iEdge].setgas();
        // half time 
        dprime = cEdgeDataH[iEdge][0];
        uprime = cEdgeDataH[iEdge][1];
        vprime = cEdgeDataH[iEdge][2];
        pprime = cEdgeDataH[iEdge][3];
        QHEdge[iEdge] = Euler2D(dprime,uprime,vprime,pprime);
        QHEdge[iEdge].setgas();
    }

    // Time related solution variables
    Euler2D *QNodetemp;             // Global nodal states at full timestep
    Euler2D *QHNodetemp;             // Global nodal states at full timestep
    Euler2D *QEdgetemp;             // Global nodal states at full timestep
    Euler2D *QHEdgetemp;             // Global nodal states at full timestep
    QNodetemp = new Euler2D [cnNodes];
    QHNodetemp = new Euler2D [cnNodes];
    QEdgetemp = new Euler2D [cnEdges];
    QHEdgetemp = new Euler2D [cnEdges];

    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        QNodetemp[iNode].Zero(); 
        QHNodetemp[iNode].Zero();
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        QEdgetemp[iEdge].Zero(); 
        QHEdgetemp[iEdge].Zero();
    }

    // edge acoustic updates
    // Edge based update to avoid evaluating edges more than once
    int lCell, cjCell, rCell, crCell;
    int lEdge, jEdge, clEdge, rEdge, crEdge;
    int lNode, clNode, jCell;
    Euler2D Q, QH;             // Global nodal states at full timestep
    Euler2D gradx, grady, gradxH, gradyH, tempx, tempy, tempxH, tempyH;
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        gradx.Zero();
        grady.Zero();
        gradxH.Zero();
        gradyH.Zero();

        // for each edge, update using all the elements around it (2 elements for edge)
        for ( jEdge = 0; jEdge < 2; jEdge++ ){
            lCell = cAcEdgeCells[jEdge][iEdge][0];      // left element number 
            cjCell = lCell - 1;                     // c style index for eL 
            lEdge = cAcEdgeCells[jEdge][iEdge][1];      // local edge numbers, in fortran 1-3 
            clEdge = 2*(lEdge)-1;                   // c style index for local edge number, 1,3,5
       
            if ( cjCell >= 0 ) {
                QHEdgetemp[iEdge] += EH[cjCell].EdgeFluxResidual(clEdge, HALF*dt, 
                                                    tempxH, tempyH);

                QEdgetemp[iEdge] += E[cjCell].EdgeFluxResidual(clEdge, dt, 
                                                       tempx, tempy);

                gradx += tempx;
                grady += tempy;
                gradxH += tempxH;
                gradyH += tempyH;

            }
        }

        // corrections 
        QHEdgetemp[iEdge].u += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHEdge[iEdge].d*gradxH.p*(gradxH.u+gradyH.v);
        QHEdgetemp[iEdge].v += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHEdge[iEdge].d*gradyH.p*(gradxH.u+gradyH.v);
        QHEdgetemp[iEdge].p += HALF*(HALF*dt)*(HALF*dt)*GAMMA*(GAMMA-1.0)*QHEdge[iEdge].p*(gradxH.u+gradyH.v)*(gradxH.u+gradyH.v);

        QEdgetemp[iEdge].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[iEdge].d*gradx.p*(gradx.u+grady.v);
        QEdgetemp[iEdge].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[iEdge].d*grady.p*(gradx.u+grady.v);
        QEdgetemp[iEdge].p += HALF*(dt)*(dt)*GAMMA*(GAMMA-1.0)*QEdge[iEdge].p*(gradx.u+grady.v)*(gradx.u+grady.v);

        // interaction terms
	    QHEdgetemp[iEdge].d -= HALF*(HALF*dt)*(HALF*dt)*QHEdge[iEdge].d*2.0*(gradyH.u*gradxH.v-gradxH.u*gradyH.v);
        QHEdgetemp[iEdge].u -= HALF*(HALF*dt)*(HALF*dt)/QHEdge[iEdge].d*(gradyH.p*(gradxH.v-gradyH.u));
        QHEdgetemp[iEdge].v -= HALF*(HALF*dt)*(HALF*dt)/QHEdge[iEdge].d*(gradxH.p*(gradyH.u-gradxH.v));  
        QHEdgetemp[iEdge].p += HALF*(HALF*dt)*(HALF*dt)*( (gradxH.p*gradxH.p+gradyH.p*gradyH.p)/QHEdge[iEdge].d -
                                GAMMA*QHEdge[iEdge].p/QHEdge[iEdge].d*(gradxH.p*gradxH.d+gradyH.p*gradyH.d)/QHEdge[iEdge].d +
                                GAMMA*QHEdge[iEdge].p*2.0*(gradxH.u*gradyH.v-gradyH.u*gradxH.v) );
                
        QEdgetemp[iEdge].d -= HALF*(dt)*(dt)*QEdge[iEdge].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        QEdgetemp[iEdge].u -= HALF*(dt)*(dt)/QEdge[iEdge].d*(grady.p*(gradx.v-grady.u));
        QEdgetemp[iEdge].v -= HALF*(dt)*(dt)/QEdge[iEdge].d*(gradx.p*(grady.u-gradx.v));
        QEdgetemp[iEdge].p += HALF*(dt)*(dt)*( (gradx.p*gradx.p+grady.p*grady.p)/QEdge[iEdge].d - 
                                GAMMA*QEdge[iEdge].p/QEdge[iEdge].d*(gradx.p*gradx.d+grady.p*grady.d)/QEdge[iEdge].d +
                                GAMMA*QEdge[iEdge].p*2.0*(gradx.u*grady.v-grady.u*gradx.v) );

    }

    // INTERIOR node acoustic updates
    // Node based update to avoid evaluating nodes more than once
    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        
        gradx.Zero();
        grady.Zero();
        gradxH.Zero();
        gradyH.Zero();
        // for each node, update using all the elements around it 
        for ( jNode = 0; jNode < maxelem; jNode++ ){

            jCell = cAcNodeCells[jNode][iNode][0];  // global element number
            cjCell = jCell - 1;                     // c style global element number
            lNode = cAcNodeCells[jNode][iNode][1]; // local node number, in fortran 1-3
            clNode = 2*(lNode-1);                 // c style local node number, 0,2,4

            if ( cjCell >= 0 ) { 
                
                // half time step
                QHNodetemp[iNode] += EH[cjCell].NodeFluxResidual(clNode, HALF*dt,
                                                         tempxH, tempyH);

                // full time step
                QNodetemp[iNode] += E[cjCell].NodeFluxResidual(clNode, dt,
                                                        tempx, tempy);

                gradx += tempx;
                grady += tempy;
                gradxH += tempxH;
                gradyH += tempyH;

            }

        }
        
        // corrections 
        QHNodetemp[iNode].u += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHNode[iNode].d*gradxH.p*(gradxH.u+gradyH.v);
        QHNodetemp[iNode].v += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHNode[iNode].d*gradyH.p*(gradxH.u+gradyH.v);
        QHNodetemp[iNode].p += HALF*(HALF*dt)*(HALF*dt)*GAMMA*(GAMMA-1.0)*QHNode[iNode].p*(gradxH.u+gradyH.v)*(gradxH.u+gradyH.v);

        QNodetemp[iNode].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[iNode].d*gradx.p*(gradx.u+grady.v);
        QNodetemp[iNode].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[iNode].d*grady.p*(gradx.u+grady.v);
        QNodetemp[iNode].p += HALF*(dt)*(dt)*GAMMA*(GAMMA-1.0)*QNode[iNode].p*(gradx.u+grady.v)*(gradx.u+grady.v);

        // interaction terms
	    QHNodetemp[iNode].d -= HALF*(HALF*dt)*(HALF*dt)*QHNode[iNode].d*2.0*(gradyH.u*gradxH.v-gradxH.u*gradyH.v);
        QHNodetemp[iNode].u -= HALF*(HALF*dt)*(HALF*dt)/QHNode[iNode].d*(gradyH.p*(gradxH.v-gradyH.u));
        QHNodetemp[iNode].v -= HALF*(HALF*dt)*(HALF*dt)/QHNode[iNode].d*(gradxH.p*(gradyH.u-gradxH.v));  
        QHNodetemp[iNode].p += HALF*(HALF*dt)*(HALF*dt)*( (gradxH.p*gradxH.p+gradyH.p*gradyH.p)/QHNode[iNode].d -
                                GAMMA*QHNode[iNode].p/QHNode[iNode].d*(gradxH.p*gradxH.d+gradyH.p*gradyH.d)/QHNode[iNode].d +
                                GAMMA*QHNode[iNode].p*2.0*(gradxH.u*gradyH.v-gradyH.u*gradxH.v) );

        QNodetemp[iNode].d -= HALF*(dt)*(dt)*QNode[iNode].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        QNodetemp[iNode].u -= HALF*(dt)*(dt)/QNode[iNode].d*(grady.p*(gradx.v-grady.u));
        QNodetemp[iNode].v -= HALF*(dt)*(dt)/QNode[iNode].d*(gradx.p*(grady.u-gradx.v));
        QNodetemp[iNode].p += HALF*(dt)*(dt)*( (gradx.p*gradx.p+grady.p*grady.p)/QNode[iNode].d -
                                GAMMA*QNode[iNode].p/QNode[iNode].d*(gradx.p*gradx.d+grady.p*grady.d)/QNode[iNode].d +
                                GAMMA*QNode[iNode].p*2.0*(gradx.u*grady.v-grady.u*gradx.v) );
        
    }
     

    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        // full time
        cNodeData[iNode][0] += QNodetemp[iNode].d; 
        cNodeData[iNode][1] += QNodetemp[iNode].u; 
        cNodeData[iNode][2] += QNodetemp[iNode].v; 
        cNodeData[iNode][3] += QNodetemp[iNode].p; 
        // half time
        cNodeDataH[iNode][0] += QHNodetemp[iNode].d; 
        cNodeDataH[iNode][1] += QHNodetemp[iNode].u; 
        cNodeDataH[iNode][2] += QHNodetemp[iNode].v; 
        cNodeDataH[iNode][3] += QHNodetemp[iNode].p; 
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        // full time
        cEdgeData[iEdge][0] += QEdgetemp[iEdge].d; 
        cEdgeData[iEdge][1] += QEdgetemp[iEdge].u; 
        cEdgeData[iEdge][2] += QEdgetemp[iEdge].v; 
        cEdgeData[iEdge][3] += QEdgetemp[iEdge].p; 
        // half time
        cEdgeDataH[iEdge][0] += QHEdgetemp[iEdge].d; 
        cEdgeDataH[iEdge][1] += QHEdgetemp[iEdge].u; 
        cEdgeDataH[iEdge][2] += QHEdgetemp[iEdge].v; 
        cEdgeDataH[iEdge][3] += QHEdgetemp[iEdge].p; 
    }

    // clean up the memory
    delete[] E;
    delete[] EH;
    delete[] QNode;
    delete[] QHNode;
    delete[] QEdge;
    delete[] QHEdge;
    delete[] QNodetemp;
    delete[] QHNodetemp;
    delete[] QEdgetemp;
    delete[] QHEdgetemp;

}

void acousticsnobc_(int *iter,
                    int *nCells, 
                    int *nDim,
                    int *nEqns,
                    int *nNodes,
                    int *nEdges,
                    int *maxNodesPerCell,
                    int *maxNodesPerFace,
                    int *maxFacesPerCell,
                    double *dtIn,
                    double *nodeData,  // global node data at t+dt
                    double *edgeData,  // global edge data at t+dt
                    double *nodeSignal,// global node data signal
                    double *edgeSignal,// global edge data signal
                    double *nodeCoord, // global mesh node coordinate
                    double *edgeCoord, // global mesh edge coordinate
                    int *cellNodes,    // global node index in a cell
                    int *cellFaces     // global face index in a cell
                    )       
{
    // set variables consistent with C++ code 
    int citer = *iter;           // iteration number
    int cnCells = *nCells;       // number of cells  
    int cnDim = *nDim;           // number of dimensions
    int cnEqns = *nEqns;         // number of equations 
    int cnNodes = *nNodes;       // total number of nodes 
    int cnEdges = *nEdges;       // total number of edges 
    int cmaxNodesPerCell = *maxNodesPerCell; // max number of nodes per cell
    int cmaxFacesPerCell = *maxFacesPerCell; // max number of faces per cell
    int cmaxNodesPerFace = *maxNodesPerFace; // max number of nodes per face
    double dt = *dtIn;           // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[cnEqns];
    dA2d_t1 cNodeData = (dA2d_t1) nodeData;    // Use this data as initial conditions
    dA2d_t1 cEdgeData = (dA2d_t1) edgeData;    // Use this data as initial conditions
    dA2d_t1 cNodeSignal = (dA2d_t1) nodeSignal;    // Use this data as initial conditions
    dA2d_t1 cEdgeSignal = (dA2d_t1) edgeSignal;    // Use this data as initial conditions
    typedef double (*dA2d_t2)[cnDim];
    dA2d_t2 cNodeCoord = (dA2d_t2) nodeCoord;
    dA2d_t2 cEdgeCoord = (dA2d_t2) edgeCoord;
    typedef double (*dA2d_t3)[2*cnEqns];
    typedef int (*iA2d_t1)[cmaxNodesPerCell];
    iA2d_t1 cCellNodes = (iA2d_t1) cellNodes;
    typedef int (*iA2d_t2)[cmaxFacesPerCell];
    iA2d_t2 cCellFaces = (iA2d_t2) cellFaces;
    
    // local variables
    int iCell, iNode, jNode, iEdge, gNode, cgNode, gFace, cgFace, gEdge, cgEdge; 

    // Initialize mesh elements
    Mesh2D *E;   
    E = new Mesh2D [cnCells];
    
    // set GLOBAL NODE coordinate data
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++ ){
            gNode = cCellNodes[iCell][iNode];   // global node
            cgNode = gNode - 1;                 // c index style of gNode
            E[iCell].nx[iNode] = cNodeCoord[cgNode][0]; 
            E[iCell].ny[iNode] = cNodeCoord[cgNode][1]; 
            E[iCell].nid[iNode] = cgNode; // global node number
        }
        E[iCell].InitAngle();
    }

    // set ELEMENT reconstruction states
    double dprime, uprime, vprime, pprime;
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        // initialize elements 
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++){
            gNode = cCellNodes[iCell][iNode];
            cgNode = gNode - 1;                 // c index style of gNode

            dprime = cNodeData[cgNode][0];
            uprime = cNodeData[cgNode][1];
            vprime = cNodeData[cgNode][2];
            pprime = cNodeData[cgNode][3];
            E[iCell].Q[2*iNode] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iNode].setgas();
        }
        for ( iEdge = 0; iEdge < cmaxFacesPerCell; iEdge++){
            gEdge = cCellFaces[iCell][iEdge];
            cgEdge = gEdge - 1;                 // c index style of gEdge

            dprime = cEdgeData[cgEdge][0];
            uprime = cEdgeData[cgEdge][1];
            vprime = cEdgeData[cgEdge][2];
            pprime = cEdgeData[cgEdge][3];
            E[iCell].Q[2*iEdge+1] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iEdge+1].setgas();

        }
        // we assume Lax-Wendroff expansion for acoustic stage, meaning
        // that all values are not part of conservative update. 
        // initialize average
        E[iCell].InitAverage();
    }

    // global state variables
    Euler2D *QNode;
    Euler2D *QEdge;
    QNode = new Euler2D [cnNodes]; // global node state variables
    QEdge = new Euler2D [cnEdges]; // global edge state variables
    for ( iNode = 0; iNode < cnNodes; iNode++ ){
        dprime = cNodeData[iNode][0];
        uprime = cNodeData[iNode][1];
        vprime = cNodeData[iNode][2];
        pprime = cNodeData[iNode][3];
        QNode[iNode] = Euler2D(dprime,uprime,vprime,pprime);
        QNode[iNode].setgas();
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ){
        dprime = cEdgeData[iEdge][0];
        uprime = cEdgeData[iEdge][1];
        vprime = cEdgeData[iEdge][2];
        pprime = cEdgeData[iEdge][3];
        QEdge[iEdge] = Euler2D(dprime,uprime,vprime,pprime);
        QEdge[iEdge].setgas();
    }

    // Time related solution variables
    Euler2D *QNodetemp;             // Global nodal states at full timestep
    Euler2D *QEdgetemp;             // Global nodal states at full timestep
    QNodetemp = new Euler2D [cnNodes];
    QEdgetemp = new Euler2D [cnEdges];
    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        QNodetemp[iNode].Zero(); 
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        QEdgetemp[iEdge].Zero(); 
    }

    // Cell based acoustics updates
    int clEdge, clNode;
    Euler2D gradx, grady;
    for ( iCell = 0; iCell < cnCells; iCell++ ){

        // update 3 nodes in one cell
        for ( iNode = 0; iNode < 3; iNode++ ){
            gNode = cCellNodes[iCell][iNode];   // fortran index
            cgNode = gNode - 1;                 // c index style of gEdge
            clNode = 2*(iNode);                 // c style local node number, 0,2,4

            gradx.Zero();
            grady.Zero();

            QNodetemp[cgNode] += E[iCell].NodeFluxResidual(clNode, dt,
                                                    gradx, grady);

            // corrections 
            QNodetemp[cgNode].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[cgNode].d*gradx.p*(gradx.u+grady.v);
            QNodetemp[cgNode].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[cgNode].d*grady.p*(gradx.u+grady.v);
            QNodetemp[cgNode].p += HALF*(dt)*(dt)*GAMMA*(GAMMA-1.0)*QNode[cgNode].p*(gradx.u+grady.v)*(gradx.u+grady.v);

            // interaction terms
            QNodetemp[cgNode].d -= HALF*(dt)*(dt)*QNode[cgNode].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
            QNodetemp[cgNode].u -= HALF*(dt)*(dt)/QNode[cgNode].d*(grady.p*(gradx.v-grady.u));
            QNodetemp[cgNode].v -= HALF*(dt)*(dt)/QNode[cgNode].d*(gradx.p*(grady.u-gradx.v));
            QNodetemp[cgNode].p += HALF*(dt)*(dt)*( (gradx.p*gradx.p+grady.p*grady.p)/QNode[cgNode].d -
                                    GAMMA*QNode[cgNode].p/QNode[cgNode].d*(gradx.p*gradx.d+grady.p*grady.d)/QNode[cgNode].d +
                                    GAMMA*QNode[cgNode].p*2.0*(gradx.u*grady.v-grady.u*gradx.v) );

        }

        // update 3 edges in one cell
        for ( iEdge = 0; iEdge < 3; iEdge++ ){
            gEdge = cCellFaces[iCell][iEdge];   // fortran index
            cgEdge = gEdge - 1;                 // c index style of gEdge
            clEdge = 2*(iEdge) + 1;             // c style index for local edge number, 1,3,5
            
            gradx.Zero();
            grady.Zero();

            QEdgetemp[cgEdge] += E[iCell].EdgeFluxResidual(clEdge, dt, 
                                                   gradx, grady);

            // corrections 
            QEdgetemp[cgEdge].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[cgEdge].d*gradx.p*(gradx.u+grady.v);
            QEdgetemp[cgEdge].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[cgEdge].d*grady.p*(gradx.u+grady.v);
            QEdgetemp[cgEdge].p += HALF*(dt)*(dt)*GAMMA*(GAMMA-1.0)*QEdge[cgEdge].p*(gradx.u+grady.v)*(gradx.u+grady.v);

            // interaction terms
            QEdgetemp[cgEdge].d -= HALF*(dt)*(dt)*QEdge[cgEdge].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
            QEdgetemp[cgEdge].u -= HALF*(dt)*(dt)/QEdge[cgEdge].d*(grady.p*(gradx.v-grady.u));
            QEdgetemp[cgEdge].v -= HALF*(dt)*(dt)/QEdge[cgEdge].d*(gradx.p*(grady.u-gradx.v));
            QEdgetemp[cgEdge].p += HALF*(dt)*(dt)*( (gradx.p*gradx.p+grady.p*grady.p)/QEdge[cgEdge].d - 
                                    GAMMA*QEdge[cgEdge].p/QEdge[cgEdge].d*(gradx.p*gradx.d+grady.p*grady.d)/QEdge[cgEdge].d +
                                    GAMMA*QEdge[cgEdge].p*2.0*(gradx.u*grady.v-grady.u*gradx.v) );

        }
        

    }
    
    // update the nodes and edges and return 
    // signals are returned for periodic boundary condition 
    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        // updated node value
        cNodeData[iNode][0] += QNodetemp[iNode].d; 
        cNodeData[iNode][1] += QNodetemp[iNode].u; 
        cNodeData[iNode][2] += QNodetemp[iNode].v; 
        cNodeData[iNode][3] += QNodetemp[iNode].p; 
        // signals
        cNodeSignal[iNode][0] = QNodetemp[iNode].d; 
        cNodeSignal[iNode][1] = QNodetemp[iNode].u; 
        cNodeSignal[iNode][2] = QNodetemp[iNode].v; 
        cNodeSignal[iNode][3] = QNodetemp[iNode].p; 
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        // updated edge value 
        cEdgeData[iEdge][0] += QEdgetemp[iEdge].d; 
        cEdgeData[iEdge][1] += QEdgetemp[iEdge].u; 
        cEdgeData[iEdge][2] += QEdgetemp[iEdge].v; 
        cEdgeData[iEdge][3] += QEdgetemp[iEdge].p; 
        // signals
        cEdgeSignal[iEdge][0] = QEdgetemp[iEdge].d; 
        cEdgeSignal[iEdge][1] = QEdgetemp[iEdge].u; 
        cEdgeSignal[iEdge][2] = QEdgetemp[iEdge].v; 
        cEdgeSignal[iEdge][3] = QEdgetemp[iEdge].p; 
    }

    // clean up the memory
    delete[] E;
    delete[] QNode;
    delete[] QEdge;
    delete[] QNodetemp;
    delete[] QEdgetemp;

}

void isentropicacoustics_(int *iter,
                          int *nCells, 
                          int *nDim,
                          int *nEqns,
                          int *nNodes,
                          int *nEdges,
                          int *maxNodesPerCell,
                          int *maxNodesPerFace,
                          int *maxFacesPerCell,
                          double *dtIn,
                          double *nodeDataN, // global node data at time step n
                          double *edgeDataN, // global edge data at time step n
                          double *primAvgN,  // global cell averages at time step n
                          double *nodeDataH,  // global node data at n+0.5*dt
                          double *edgeDataH,  // global edge data at n+0.5*dt
                          double *nodeData,  // global node data at n+dt
                          double *edgeData,  // global edge data at n+dt
                          double *nodeCoord, // global mesh node coordinate
                          double *edgeCoord, // global mesh edge coordinate
                          int *cellNodes,    // global node index in a cell
                          int *cellFaces,    // global face index in a cell
                          int *acNodeCells,  // global cells that neighbor global node
                          int *acEdgeCells,  // global cells that neighbor global edge
                          int *nodeBC,       // global node boundary condition flag
                          int *edgeBC        // global edge boundary condition flag
                          )       
{
    // set variables consistent with C++ code 
    int citer = *iter;           // iteration number
    int cnCells = *nCells;       // number of cells  
    int cnDim = *nDim;           // number of dimensions
    int cnEqns = *nEqns;         // number of equations 
    int cnNodes = *nNodes;       // total number of nodes 
    int cnEdges = *nEdges;       // total number of edges 
    int cmaxNodesPerCell = *maxNodesPerCell; // max number of nodes per cell
    int cmaxFacesPerCell = *maxFacesPerCell; // max number of faces per cell
    int cmaxNodesPerFace = *maxNodesPerFace; // max number of nodes per face
    double dt = *dtIn;           // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[cnEqns];
    dA2d_t1 cNodeDataN = (dA2d_t1) nodeDataN;
    dA2d_t1 cNodeDataH = (dA2d_t1) nodeDataH;
    dA2d_t1 cNodeData = (dA2d_t1) nodeData;    // Use this data as initial conditions
    dA2d_t1 cEdgeDataN = (dA2d_t1) edgeDataN;
    dA2d_t1 cEdgeDataH = (dA2d_t1) edgeDataH;
    dA2d_t1 cEdgeData = (dA2d_t1) edgeData;    // Use this data as initial conditions
    dA2d_t1 cPrimAvgN = (dA2d_t1) primAvgN;
    //dA2d_t1 cNodeDataDBAco2 = (dA2d_t1) nodeDataDBAco2;
    //dA2d_t1 cEdgeDataDBAco2 = (dA2d_t1) edgeDataDBAco2;
    typedef double (*dA2d_t2)[cnDim];
    dA2d_t2 cNodeCoord = (dA2d_t2) nodeCoord;
    dA2d_t2 cEdgeCoord = (dA2d_t2) edgeCoord;
    //typedef double (*dA2d_t3)[2*cnEqns];
    //dA2d_t3 cNodeGradDB = (dA2d_t3) nodeGradDB;
    //dA2d_t3 cEdgeGradDB = (dA2d_t3) edgeGradDB;
    //typedef double (*dA2d_t4)[3*cnEqns];
    //dA2d_t4 cNodeSGradDB = (dA2d_t4) nodeSGradDB;
    //dA2d_t4 cEdgeSGradDB = (dA2d_t4) edgeSGradDB;
    typedef int (*iA2d_t1)[cmaxNodesPerCell];
    iA2d_t1 cCellNodes = (iA2d_t1) cellNodes;
    typedef int (*iA2d_t2)[cmaxFacesPerCell];
    iA2d_t2 cCellFaces = (iA2d_t2) cellFaces;
    typedef int (*iA3d_t1)[cnNodes][2];
    iA3d_t1 cAcNodeCells = (iA3d_t1) acNodeCells;
    typedef int (*iA3d_t2)[cnEdges][2];
    iA3d_t2 cAcEdgeCells = (iA3d_t2) acEdgeCells;
    
    // local variables
    int iCell, iNode, jNode, iEdge, gNode, cgNode, gFace, cgFace, gEdge, cgEdge; 
    int maxelem = 10;  // number of cells that neighbor the same node, unstructured grid 
    //int maxelem = 6;  // number of cells that neighbor the same node, fixed for structured grid 

    // Initialize mesh elements
    Mesh2D *E;   // full time step
    Mesh2D *EH;  // half time step              
    E = new Mesh2D [cnCells];
    EH = new Mesh2D [cnCells];
    
    // set GLOBAL NODE coordinate data
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++ ){
            gNode = cCellNodes[iCell][iNode];   // global node
            cgNode = gNode - 1;                 // c index style of gNode
            E[iCell].nx[iNode] = cNodeCoord[cgNode][0]; 
            E[iCell].ny[iNode] = cNodeCoord[cgNode][1]; 
            E[iCell].nid[iNode] = cgNode; // global node number
        }
        E[iCell].InitAngle();
        EH[iCell].CopyGeometry(E[iCell]);
    }

    // set ELEMENT reconstruction states
    double dprime, uprime, vprime, pprime, pH;
    int fortiEdge;   
    for ( iCell = 0; iCell < cnCells; iCell++ ){
        // initialize elements 
        for ( iNode = 0; iNode < cmaxNodesPerCell; iNode++){
            gNode = cCellNodes[iCell][iNode];
            cgNode = gNode - 1;                 // c index style of gNode
            // full time step
            dprime = cNodeData[cgNode][0];
            uprime = cNodeData[cgNode][1];
            vprime = cNodeData[cgNode][2];
            pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
            E[iCell].Q[2*iNode] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iNode].setgas();

            // half time step
            dprime = cNodeDataH[cgNode][0];
            uprime = cNodeDataH[cgNode][1];
            vprime = cNodeDataH[cgNode][2];
            pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
            EH[iCell].Q[2*iNode] = Euler2D(dprime,uprime,vprime,pprime);
            EH[iCell].Q[2*iNode].setgas();
        }
        for ( iEdge = 0; iEdge < cmaxFacesPerCell; iEdge++){
            gEdge = cCellFaces[iCell][iEdge];
            cgEdge = gEdge - 1;                 // c index style of gEdge
            // full time step
            dprime = cEdgeData[cgEdge][0];
            uprime = cEdgeData[cgEdge][1];
            vprime = cEdgeData[cgEdge][2];
            pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
            E[iCell].Q[2*iEdge+1] = Euler2D(dprime,uprime,vprime,pprime);
            E[iCell].Q[2*iEdge+1].setgas();

            // half time step
            dprime = cEdgeDataH[cgEdge][0];
            uprime = cEdgeDataH[cgEdge][1];
            vprime = cEdgeDataH[cgEdge][2];
            pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
            EH[iCell].Q[2*iEdge+1] = Euler2D(dprime,uprime,vprime,pprime);
            EH[iCell].Q[2*iEdge+1].setgas();
        }
        // we assume Lax-Wendroff expansion for acoustic stage, meaning
        // that all values are not part of conservative update. 
        // initialize average
        E[iCell].InitAverage();
        EH[iCell].InitAverage();
    }

    // global state variables
    Euler2D *QNode;
    Euler2D *QHNode;            // Global nodal states at half timestep
    Euler2D *QEdge;
    Euler2D *QHEdge;            // Global edge states at half timestep
    QNode = new Euler2D [cnNodes]; // global node state variables
    QHNode = new Euler2D [cnNodes];
    QEdge = new Euler2D [cnEdges]; // global edge state variables
    QHEdge = new Euler2D [cnEdges];
    for ( iNode = 0; iNode < cnNodes; iNode++ ){
        // full time
        dprime = cNodeData[iNode][0];
        uprime = cNodeData[iNode][1];
        vprime = cNodeData[iNode][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
        QNode[iNode] = Euler2D(dprime,uprime,vprime,pprime);
        QNode[iNode].setgas();
        // half time 
        dprime = cNodeDataH[iNode][0];
        uprime = cNodeDataH[iNode][1];
        vprime = cNodeDataH[iNode][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
        QHNode[iNode] = Euler2D(dprime,uprime,vprime,pprime);
        QHNode[iNode].setgas();    
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ){
        // full time
        dprime = cEdgeData[iEdge][0];
        uprime = cEdgeData[iEdge][1];
        vprime = cEdgeData[iEdge][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
        QEdge[iEdge] = Euler2D(dprime,uprime,vprime,pprime);
        QEdge[iEdge].setgas();
        // half time 
        dprime = cEdgeDataH[iEdge][0];
        uprime = cEdgeDataH[iEdge][1];
        vprime = cEdgeDataH[iEdge][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA); 
        QHEdge[iEdge] = Euler2D(dprime,uprime,vprime,pprime);
        QHEdge[iEdge].setgas();
    }

    // Time related solution variables
    Euler2D *QNodetemp;             // Global nodal states at full timestep
    Euler2D *QHNodetemp;             // Global nodal states at full timestep
    Euler2D *QEdgetemp;             // Global nodal states at full timestep
    Euler2D *QHEdgetemp;             // Global nodal states at full timestep
    QNodetemp = new Euler2D [cnNodes];
    QHNodetemp = new Euler2D [cnNodes];
    QEdgetemp = new Euler2D [cnEdges];
    QHEdgetemp = new Euler2D [cnEdges];

    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        QNodetemp[iNode].Zero(); 
        QHNodetemp[iNode].Zero();
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        QEdgetemp[iEdge].Zero(); 
        QHEdgetemp[iEdge].Zero();
    }

    // edge acoustic updates
    // Edge based update to avoid evaluating edges more than once
    int lCell, cjCell, rCell, crCell;
    int lEdge, jEdge, clEdge, rEdge, crEdge;
    int lNode, clNode, jCell;
    Euler2D Q, QH;             // Global nodal states at full timestep
    Euler2D gradx, grady, gradxH, gradyH, tempx, tempy, tempxH, tempyH;
    double isentGradPxH, isentGradPyH, isentGradPx, isentGradPy;
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        gradx.Zero();
        grady.Zero();
        gradxH.Zero();
        gradyH.Zero();

        // for each edge, update using all the elements around it (2 elements for edge)
        for ( jEdge = 0; jEdge < 2; jEdge++ ){
            lCell = cAcEdgeCells[jEdge][iEdge][0];      // left element number 
            cjCell = lCell - 1;                     // c style index for eL 
            lEdge = cAcEdgeCells[jEdge][iEdge][1];      // local edge numbers, in fortran 1-3 
            clEdge = 2*(lEdge)-1;                   // c style index for local edge number, 1,3,5
       
            if ( cjCell >= 0 ) {
                QHEdgetemp[iEdge] += EH[cjCell].EdgeFluxResidualIsentEuler(clEdge, HALF*dt, 
                                                    tempxH, tempyH);

                QEdgetemp[iEdge] += E[cjCell].EdgeFluxResidualIsentEuler(clEdge, dt, 
                                                       tempx, tempy);

                gradx += tempx;
                grady += tempy;
                gradxH += tempxH;
                gradyH += tempyH;

            }
        }

        // pressure gradients in isentropic euler
        isentGradPxH = ISENTROPIC_CONST*GAMMA*pow(QHEdge[iEdge].d,GAMMA-1.0)*gradxH.d;
        isentGradPyH = ISENTROPIC_CONST*GAMMA*pow(QHEdge[iEdge].d,GAMMA-1.0)*gradyH.d;
        isentGradPx = ISENTROPIC_CONST*GAMMA*pow(QEdge[iEdge].d,GAMMA-1.0)*gradx.d;
        isentGradPy = ISENTROPIC_CONST*GAMMA*pow(QEdge[iEdge].d,GAMMA-1.0)*grady.d;
        // corrections 
        QHEdgetemp[iEdge].u += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHEdge[iEdge].d*(isentGradPxH)*(gradxH.u+gradyH.v);
        QHEdgetemp[iEdge].v += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHEdge[iEdge].d*(isentGradPyH)*(gradxH.u+gradyH.v);

        QEdgetemp[iEdge].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[iEdge].d*(isentGradPx)*(gradx.u+grady.v);
        QEdgetemp[iEdge].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QEdge[iEdge].d*(isentGradPy)*(gradx.u+grady.v);

        // interaction terms
	    QHEdgetemp[iEdge].d -= HALF*(HALF*dt)*(HALF*dt)*QHEdge[iEdge].d*2.0*(gradyH.u*gradxH.v-gradxH.u*gradyH.v);
        QHEdgetemp[iEdge].u -= HALF*(HALF*dt)*(HALF*dt)/QHEdge[iEdge].d*(isentGradPyH*(gradxH.v-gradyH.u));
        QHEdgetemp[iEdge].v -= HALF*(HALF*dt)*(HALF*dt)/QHEdge[iEdge].d*(isentGradPxH*(gradyH.u-gradxH.v));  

        QEdgetemp[iEdge].d -= HALF*(dt)*(dt)*QEdge[iEdge].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        QEdgetemp[iEdge].u -= HALF*(dt)*(dt)/QEdge[iEdge].d*(isentGradPy*(gradx.v-grady.u));
        QEdgetemp[iEdge].v -= HALF*(dt)*(dt)/QEdge[iEdge].d*(isentGradPx*(grady.u-gradx.v));

    }

    // Node based update to avoid evaluating nodes more than once
    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        
        gradx.Zero();
        grady.Zero();
        gradxH.Zero();
        gradyH.Zero();
        // for each node, update using all the elements around it (6 elements for node)
        for ( jNode = 0; jNode < maxelem; jNode++ ){
            jCell = cAcNodeCells[jNode][iNode][0];  // global element number
            cjCell = jCell - 1;                     // c style global element number
            lNode = cAcNodeCells[jNode][iNode][1]; // local node number, in fortran 1-3
            clNode = 2*(lNode-1);                 // c style local node number, 0,2,4

            if ( cjCell >= 0 ) { 
                
                // half time step
                QHNodetemp[iNode] += EH[cjCell].NodeFluxResidualIsentEuler(clNode, HALF*dt,
                                                         tempxH, tempyH);

                // full time step
                QNodetemp[iNode] += E[cjCell].NodeFluxResidualIsentEuler(clNode, dt,
                                                        tempx, tempy);

                gradx += tempx;
                grady += tempy;
                gradxH += tempxH;
                gradyH += tempyH;

            }

        }
        
        // pressure gradients in isentropic euler
        isentGradPxH = ISENTROPIC_CONST*GAMMA*pow(QHNode[iNode].d,GAMMA-1.0)*gradxH.d;
        isentGradPyH = ISENTROPIC_CONST*GAMMA*pow(QHNode[iNode].d,GAMMA-1.0)*gradyH.d;
        isentGradPx = ISENTROPIC_CONST*GAMMA*pow(QNode[iNode].d,GAMMA-1.0)*gradx.d;
        isentGradPy = ISENTROPIC_CONST*GAMMA*pow(QNode[iNode].d,GAMMA-1.0)*grady.d;
        // corrections 
        QHNodetemp[iNode].u += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHNode[iNode].d*isentGradPxH*(gradxH.u+gradyH.v);
        QHNodetemp[iNode].v += HALF*(HALF*dt)*(HALF*dt)*(GAMMA-1.0)/QHNode[iNode].d*isentGradPyH*(gradxH.u+gradyH.v);

        QNodetemp[iNode].u += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[iNode].d*isentGradPx*(gradx.u+grady.v);
        QNodetemp[iNode].v += HALF*(dt)*(dt)*(GAMMA-1.0)/QNode[iNode].d*isentGradPy*(gradx.u+grady.v);

        // interaction terms
	    QHNodetemp[iNode].d -= HALF*(HALF*dt)*(HALF*dt)*QHNode[iNode].d*2.0*(gradyH.u*gradxH.v-gradxH.u*gradyH.v);
        QHNodetemp[iNode].u -= HALF*(HALF*dt)*(HALF*dt)/QHNode[iNode].d*(isentGradPyH*(gradxH.v-gradyH.u));
        QHNodetemp[iNode].v -= HALF*(HALF*dt)*(HALF*dt)/QHNode[iNode].d*(isentGradPxH*(gradyH.u-gradxH.v));  

        QNodetemp[iNode].d -= HALF*(dt)*(dt)*QNode[iNode].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        QNodetemp[iNode].u -= HALF*(dt)*(dt)/QNode[iNode].d*(isentGradPy*(gradx.v-grady.u));
        QNodetemp[iNode].v -= HALF*(dt)*(dt)/QNode[iNode].d*(isentGradPx*(grady.u-gradx.v));
    }
     

    for ( iNode = 0; iNode < cnNodes; iNode++ ) {
        // full time
        cNodeData[iNode][0] += QNodetemp[iNode].d; 
        cNodeData[iNode][1] += QNodetemp[iNode].u; 
        cNodeData[iNode][2] += QNodetemp[iNode].v; 
        //cNodeData[iNode][3] += QNodetemp[iNode].p; 
        // half time
        cNodeDataH[iNode][0] += QHNodetemp[iNode].d; 
        cNodeDataH[iNode][1] += QHNodetemp[iNode].u; 
        cNodeDataH[iNode][2] += QHNodetemp[iNode].v; 
        //cNodeDataH[iNode][3] += QHNodetemp[iNode].p; 
    }
    for ( iEdge = 0; iEdge < cnEdges; iEdge++ ) {
        // full time
        cEdgeData[iEdge][0] += QEdgetemp[iEdge].d; 
        cEdgeData[iEdge][1] += QEdgetemp[iEdge].u; 
        cEdgeData[iEdge][2] += QEdgetemp[iEdge].v; 
        //cEdgeData[iEdge][3] += QEdgetemp[iEdge].p; 
        // half time
        cEdgeDataH[iEdge][0] += QHEdgetemp[iEdge].d; 
        cEdgeDataH[iEdge][1] += QHEdgetemp[iEdge].u; 
        cEdgeDataH[iEdge][2] += QHEdgetemp[iEdge].v; 
        //cEdgeDataH[iEdge][3] += QHEdgetemp[iEdge].p; 
    }

    // clean up the memory
    delete[] E;
    delete[] EH;
    delete[] QNode;
    delete[] QHNode;
    delete[] QEdge;
    delete[] QHEdge;
    delete[] QNodetemp;
    delete[] QHNodetemp;
    delete[] QEdgetemp;
    delete[] QHEdgetemp;

}

void testcarray_(int *nCells, 
            int *nEqns,
            int *nNodes,
            double *dtIn,
            double nodeDataN[], 
            double nodeDataN2[][4], 
            int nodeBC[] )
{
    typedef double (*dA2d_t1)[*nEqns];
    dA2d_t1 cNodeDataN = (dA2d_t1) nodeDataN;

    cout.precision(15);
    cout << "c" << endl;
    cout << "number of cells " << *nCells << endl;
    cout << "number of equations " << *nEqns << endl;
    cout << "dt " << *dtIn << endl;
    cout << cNodeDataN[123][0] << " " << cNodeDataN[123][1] << " " << cNodeDataN[123][2] << " " << cNodeDataN[123][3] << endl;
    cout << nodeDataN2[123][0] << " " << nodeDataN2[123][1] << " " << nodeDataN2[123][2] << " " << nodeDataN2[123][3] << endl;
    cout << nodeBC[4] << endl;

    //cNodeDataN[123][0] += 1.0;
    //cout << "number of elements in array " << (sizeof((nodeDataN))/sizeof((*nodeDataN))) << endl;
    // TODO: need to figura out how to count number of elements
    // TODO: how to use vector class with this?
    //cout << "number of elements in array " << (sizeof((nodeDataN))) << endl;
    //cout << "number of elements in array " << (sizeof((nodeBC))/sizeof((nodeBC[0]))) << endl;

}
