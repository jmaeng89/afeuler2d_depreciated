/* MeshGeometry.cc:  Geometry subroutines for 2D nonlinear Mesh Class. */
   
/* Include 2D mesh elements header file. */

#ifndef _MESH_INCLUDED
#include "Mesh2D.h"
#endif // _MESH_INCLUDED

//! Straight edge lengths.
double Mesh2D::l1(void) {
        double length;
        length = sqrt((nx[2]-nx[1])*(nx[2]-nx[1]) + (ny[2]-ny[1])*(ny[2]-ny[1]));
        return (length);
};
double Mesh2D::l1(void) const {
        double length;
        length = sqrt((nx[2]-nx[1])*(nx[2]-nx[1]) + (ny[2]-ny[1])*(ny[2]-ny[1]));
        return (length);
};

double Mesh2D::l2(void) {
        double length;
        length = sqrt((nx[0]-nx[2])*(nx[0]-nx[2]) + (ny[0]-ny[2])*(ny[0]-ny[2]));
        return (length);
};
double Mesh2D::l2(void) const {
        double length;
        length = sqrt((nx[0]-nx[2])*(nx[0]-nx[2]) + (ny[0]-ny[2])*(ny[0]-ny[2]));
        return (length);
};

double Mesh2D::l3(void){
        double length;
        length = sqrt((nx[1]-nx[0])*(nx[1]-nx[0]) + (ny[1]-ny[0])*(ny[1]-ny[0]));
        return (length);
};
double Mesh2D::l3(void) const {
        double length;
        length = sqrt((nx[1]-nx[0])*(nx[1]-nx[0]) + (ny[1]-ny[0])*(ny[1]-ny[0]));
        return (length);
};

//! Straight edge lengths squared.
double Mesh2D::l1sq(void) {
        return ((nx[2]-nx[1])*(nx[2]-nx[1]) + (ny[2]-ny[1])*(ny[2]-ny[1]));
};
double Mesh2D::l1sq(void) const {
        return ((nx[2]-nx[1])*(nx[2]-nx[1]) + (ny[2]-ny[1])*(ny[2]-ny[1]));
};

double Mesh2D::l2sq(void) {
        return ((nx[0]-nx[2])*(nx[0]-nx[2]) + (ny[0]-ny[2])*(ny[0]-ny[2]));
};
double Mesh2D::l2sq(void) const {
        return ((nx[0]-nx[2])*(nx[0]-nx[2]) + (ny[0]-ny[2])*(ny[0]-ny[2]));
};

double Mesh2D::l3sq(void){
        return ((nx[1]-nx[0])*(nx[1]-nx[0]) + (ny[1]-ny[0])*(ny[1]-ny[0]));
};
double Mesh2D::l3sq(void) const {
        return ((nx[1]-nx[0])*(nx[1]-nx[0]) + (ny[1]-ny[0])*(ny[1]-ny[0]));
}

 //! Edge normal vectors.
// Edge 1 normal vectors
double Mesh2D::norm1x(void) {
        double dx, dy, l;
        dy = ny[2] - ny[1];
        dx = nx[2] - nx[1];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};
double Mesh2D::norm1x(void) const {
        double dx, dy, l;
        dy = ny[2] - ny[1];
        dx = nx[2] - nx[1];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};

double Mesh2D::norm1y(void) {
        double dx, dy, l;
        dy = ny[2] - ny[1];
        dx = nx[2] - nx[1];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};
double Mesh2D::norm1y(void) const {
        double dx, dy, l;
        dy = ny[2] - ny[1];
        dx = nx[2] - nx[1];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};

double Mesh2D::tan1(void) {
        return ((ny[2] - ny[1])/(nx[2] - nx[1]));
};
double Mesh2D::tan1(void) const {
        return ((ny[2] - ny[1])/(nx[2] - nx[1]));
};

double Mesh2D::norm1xl1(void) {
        return (ny[2]-ny[1]);
};
double Mesh2D::norm1xl1(void) const {
        return (ny[2]-ny[1]);
};
double Mesh2D::norm1yl1(void) {
        return (nx[1]-nx[2]);
};
double Mesh2D::norm1yl1(void) const {
        return (nx[1]-nx[2]);
};
        
// Edge 2 normal vectors
double Mesh2D::norm2x(void) {
        double dx, dy, l;
        dy = ny[0] - ny[2];
        dx = nx[0] - nx[2];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};
double Mesh2D::norm2x(void) const {
        double dx, dy, l;
        dy = ny[0] - ny[2];
        dx = nx[0] - nx[2];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};

double Mesh2D::norm2y(void) {
        double dx, dy, l;
        dy = ny[0] - ny[2];
        dx = nx[0] - nx[2];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};
double Mesh2D::norm2y(void) const {
        double dx, dy, l;
        dy = ny[0] - ny[2];
        dx = nx[0] - nx[2];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};

double Mesh2D::tan2(void) {
        return ((ny[0] - ny[2])/(nx[0] - nx[2]));
};
double Mesh2D::tan2(void) const {
        return ((ny[0] - ny[2])/(nx[0] - nx[2]));
};

double Mesh2D::norm2xl2(void) {
        return (ny[0]-ny[2]);
};
double Mesh2D::norm2xl2(void) const {
        return (ny[0]-ny[2]);
};
double Mesh2D::norm2yl2(void) {
        return (nx[2]-nx[0]);
};
double Mesh2D::norm2yl2(void) const {
        return (nx[2]-nx[0]);
};
        
// Edge 3 normal vectors
double Mesh2D::norm3x(void) {
        double dx, dy, l;
        dy = ny[1] - ny[0];
        dx = nx[1] - nx[0];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};
double Mesh2D::norm3x(void) const {
        double dx, dy, l;
        dy = ny[1] - ny[0];
        dx = nx[1] - nx[0];
        l = sqrt(dx*dx + dy*dy);
        return (dy/l);
};

double Mesh2D::norm3y(void) {
        double dx, dy, l;
        dy = ny[1] - ny[0];
        dx = nx[1] - nx[0];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};
double Mesh2D::norm3y(void) const {
        double dx, dy, l;
        dy = ny[1] - ny[0];
        dx = nx[1] - nx[0];
        l = sqrt(dx*dx + dy*dy);
        return (-dx/l);
};

double Mesh2D::tan3(void) {
        return ((ny[1] - ny[0])/(nx[1] - nx[0]));
};
double Mesh2D::tan3(void) const {
        return ((ny[1] - ny[0])/(nx[1] - nx[0]));
};

double Mesh2D::norm3xl3(void) {
        return (ny[1]-ny[0]);
};
double Mesh2D::norm3xl3(void) const {
        return (ny[1]-ny[0]);
};
double Mesh2D::norm3yl3(void) {
        return (nx[0]-nx[1]);
};
double Mesh2D::norm3yl3(void) const {
        return (nx[0]-nx[1]);
};

//! Area.
double Mesh2D::A(void) {
        double P = HALF*(l1() + l2() + l3());
        return (sqrt( P*(P - l1())*(P - l2())*(P - l3()) ));
};
double Mesh2D::A(void) const {
        double P = HALF*(l1() + l2() + l3());
        return (sqrt( P*(P - l1())*(P - l2())*(P - l3()) ));
};

//! Jacobian of coordinate transformation matrix.
// Determinant
double Mesh2D::J(void) {
        double det;
        det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return (det);
};
double Mesh2D::J(void) const {
        double det;
        det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return (det);
};

// Matrix entries
double Mesh2D::J11(void) {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (ny[2]-ny[0])/det );
};
double Mesh2D::J11(void) const {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (ny[2]-ny[0])/det );
};
double Mesh2D::J12(void) {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( -(nx[2]-nx[0])/det );
};
double Mesh2D::J12(void) const {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( -(nx[2]-nx[0])/det );
};
double Mesh2D::J21(void) {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (ny[0]-ny[1])/det );
};
double Mesh2D::J21(void) const {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (ny[0]-ny[1])/det );
};
double Mesh2D::J22(void) {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (nx[1]-nx[0])/det );
};
double Mesh2D::J22(void) const {
        double det = (nx[1]-nx[0])*(ny[2]-ny[0]) - (nx[2]-nx[0])*(ny[1]-ny[0]);
        return ( (nx[1]-nx[0])/det );
};

