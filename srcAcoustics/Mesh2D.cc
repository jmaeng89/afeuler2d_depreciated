/* Mesh2D.cc:  External subroutines for 2D Mesh Class. */

/* Include 2D mesh elements header file. */
#ifndef _MESH2D_INCLUDED
#include "Mesh2D.h"
#endif // _MESH2D_INCLUDED

#include "MeshGeometry.cc"
#include "MeshStates.cc"
#include "Sphmeans.cc"
#include "FunctionSM.cc"
#include "ResSM.cc"

#define BC_INFLOW       -3
#define BC_WALL         -2
#define BC_SUPERSONIC   -4
#define BC_SYMMETRY     -1

/********************************************************
 * Routine: AverageFluxResidual                         *
 *                                                      *
 * This function returns the total flux residual of     *
 * a single element, Ei, given time levels: t,          *
 * t + dt/2, t + dt.                                    *
 *                                                      *
 ********************************************************/
// for linear acoustics
Euler2D AverageFluxResidual(const Mesh2D &En,
                            const Mesh2D &Enhalf,
                            const Mesh2D &En1,
                            const double &dt) {
        Euler2D Flux, Qaverage;
        Euler2D favg, gavg;

        // Side 1
        favg = FluxAverage_x(En.Q[2], En.Q[1], En.Q[4],
                             Enhalf.Q[2], Enhalf.Q[1], Enhalf.Q[4],
                             En1.Q[2], En1.Q[1], En1.Q[4]);
        gavg = FluxAverage_y(En.Q[2], En.Q[1], En.Q[4],
                             Enhalf.Q[2], Enhalf.Q[1], Enhalf.Q[4],
                             En1.Q[2], En1.Q[1], En1.Q[4]);
        Flux = favg*En.norm1xl1() + gavg*En.norm1yl1();
        // Side 2
        favg = FluxAverage_x(En.Q[4], En.Q[3], En.Q[0],
                             Enhalf.Q[4], Enhalf.Q[3], Enhalf.Q[0],
                             En1.Q[4], En1.Q[3], En1.Q[0]);
        gavg = FluxAverage_y(En.Q[4], En.Q[3], En.Q[0],
                             Enhalf.Q[4], Enhalf.Q[3], Enhalf.Q[0],
                             En1.Q[4], En1.Q[3], En1.Q[0]);
        Flux += favg*En.norm2xl2() + gavg*En.norm2yl2();
        // Side 3
        favg = FluxAverage_x(En.Q[0], En.Q[5], En.Q[2],
                             Enhalf.Q[0], Enhalf.Q[5], Enhalf.Q[2],
                             En1.Q[0], En1.Q[5], En1.Q[2]);
        gavg = FluxAverage_y(En.Q[0], En.Q[5], En.Q[2],
                             Enhalf.Q[0], Enhalf.Q[5], Enhalf.Q[2],
                             En1.Q[0], En1.Q[5], En1.Q[2]);
        Flux += favg*En.norm3xl3() + gavg*En.norm3yl3();

        Qaverage = En.Qavg - dt/En.A()*Flux;

        return (Qaverage);
}

Euler2D FluxResidual(const Mesh2D &En,
                     const Mesh2D &Enhalf,
                     const Mesh2D &En1,
                     const int &inode) {
        Euler2D Flux;
        Euler2D favg, gavg;

        assert (inode%2 == 1);
        switch (inode) {
        case 1:
                // Side 1
                favg = FluxAverage_x(En.Q[2], En.Q[1], En.Q[4],
                                     Enhalf.Q[2], Enhalf.Q[1], Enhalf.Q[4],
                                     En1.Q[2], En1.Q[1], En1.Q[4]);
                gavg = FluxAverage_y(En.Q[2], En.Q[1], En.Q[4],
                                     Enhalf.Q[2], Enhalf.Q[1], Enhalf.Q[4],
                                     En1.Q[2], En1.Q[1], En1.Q[4]);
                Flux = favg*En.norm1xl1() + gavg*En.norm1yl1();
                break;
        case 3: 
                // Side 2
                favg = FluxAverage_x(En.Q[4], En.Q[3], En.Q[0],
                                     Enhalf.Q[4], Enhalf.Q[3], Enhalf.Q[0],
                                     En1.Q[4], En1.Q[3], En1.Q[0]);
                gavg = FluxAverage_y(En.Q[4], En.Q[3], En.Q[0],
                                     Enhalf.Q[4], Enhalf.Q[3], Enhalf.Q[0],
                                     En1.Q[4], En1.Q[3], En1.Q[0]);
                Flux = favg*En.norm2xl2() + gavg*En.norm2yl2();
                
                break;
        case 5:
                // Side 3
                favg = FluxAverage_x(En.Q[0], En.Q[5], En.Q[2],
                                     Enhalf.Q[0], Enhalf.Q[5], Enhalf.Q[2],
                                     En1.Q[0], En1.Q[5], En1.Q[2]);
                gavg = FluxAverage_y(En.Q[0], En.Q[5], En.Q[2],
                                     Enhalf.Q[0], Enhalf.Q[5], Enhalf.Q[2],
                                     En1.Q[0], En1.Q[5], En1.Q[2]);
                Flux = favg*En.norm3xl3() + gavg*En.norm3yl3();
                break;
        }
        return (Flux);
}


/********************************************************
 * Routine: UpdateEdgeFlux                              *
 *                                                      *
 * This function updates the edge value associated      *
 * with two neighboring elements using spherical        *
 * means.                                               *
 *                                                      *
 ********************************************************/
void UpdateEdgeFlux(Mesh2D E1,
                    const int &inode1,
                    Mesh2D E2,
                    const int &inode2,
                    const double &dt,
                    Mesh2D &E1temp,
                    Mesh2D &E2temp,
                    const int &iNonlin,
                    const int &iVort,
                    Euler2D &gradx, 
                    Euler2D &grady){
        double flux_x, flux_y;
        double flux_x3, flux_y3, flux_x2y, flux_xy2;
        Euler2D MR1, MRdx1, MRdy1, MRdd1, dMRdR1, nonlinMR1;
        Euler2D MR2, MRdx2, MRdy2, MRdd2, dMRdR2, nonlinMR2;
        double R;

        //if (E1.Q[inode1].c2() == E2.Q[inode2].c2()) {
        //        R = E1.Q[inode1].c()*dt;
        //        E1.EdgeMRflux(inode1, R, flux_x, flux_y,
        //                      flux_x3, flux_y3, flux_x2y, flux_xy2);
        //        //E1.EdgeMR(R, inode1, flux_x, flux_y, flux_x3, flux_y3, flux_x2y, flux_xy2,
        //        //          MR1, MRdx1, MRdy1, dMRdR1, nonlinMR1, iNonlin, iVort);
        //        //E2.EdgeMR(R, inode2, -flux_x, -flux_y, -flux_x3, -flux_y3, -flux_x2y, -flux_xy2,
        //        //MR2, MRdx2, MRdy2, dMRdR2, nonlinMR2, iNonlin, iVort); 
        //        //MR1 += MR2;
        //        //dMRdR1 += dMRdR2;
        //        E1.EdgeMR(R, inode1, flux_x, flux_y, flux_x3, flux_y3, flux_x2y, flux_xy2,
        //                  MRdx1, MRdy1, MRdd1, nonlinMR1, iNonlin, iVort);
        //        E2.EdgeMR(R, inode2, -flux_x, -flux_y, -flux_x3, -flux_y3, -flux_x2y, -flux_xy2,
        //                  MRdx2, MRdy2, MRdd2, nonlinMR2, iNonlin, iVort);

        //        MRdx1 += MRdx2;
        //        MRdy1 += MRdy2;
        //        MRdd1 += MRdd2;
        //        nonlinMR1 += nonlinMR2;
        //        
        //        // Update Q[iedge1] for element 1 & Q[iedge2] for element 2
        //        double factor = E1temp.Q[inode1].d;
        //        //E1temp.Q[inode1].d = MR1.d + R*dMRdR1.d - factor*R*(MRdx1.u + MRdy1.v);
        //        //E1temp.Q[inode1].u = MR1.u + R*dMRdR1.u - R*MRdx1.d/factor;
        //        //E1temp.Q[inode1].v = MR1.v + R*dMRdR1.v - R*MRdy1.d/factor;
        //        //E1temp.Q[inode1] += nonlinMR1;
        //        //E1temp.Q[inode1].d -= dt*E1temp.Q[inode1].d*(MRdx1.u + MRdy1.v);
        //        //E1temp.Q[inode1].p -= R*factor*(MRdx1.u + MRdy1.v);
        //        E1temp.Q[inode1].u -= dt*MRdx1.pH/factor;
        //        E1temp.Q[inode1].v -= dt*MRdy1.pH/factor;
        //        E1temp.Q[inode1] += MRdd1 + nonlinMR1;
        //} else { 
                //cout << "nonequal c" << endl;
                //cout << E1.Q[inode1] << endl;
                //cout << E2.Q[inode2] << endl;
                Euler2D gradx1, gradx2, grady1, grady2;
                Euler2D laplacex1, laplacex2, laplacey1, laplacey2, laplacexy1, laplacexy2;
                int iterr;
                //gradx1 = 0.0, gradx2 = 0.0, grady1 = 0.0, grady2 = 0.0;
                E1temp.Q[inode1] += E1.EdgeFluxResidual(inode1, dt,
                        gradx1, grady1);
                E1temp.Q[inode1] += E2.EdgeFluxResidual(inode2, dt, 
                        gradx2, grady2);
                gradx = gradx1 + gradx2;
                grady = grady1 + grady2;
        //}

        E2temp.Q[inode2].CopyState(E1temp.Q[inode1]); 
}

//// Boundary edges
//void UpdateEdgeFlux(Mesh2D E1,
//                    const int &inode1,
//                    const double &dt,
//                    Mesh2D &E1temp,
//                    const int &iNonlin,
//                    const int &iVort){
//        
//        E1temp.Q[inode1] += E1.EdgeFluxResidual(inode1, dt, iNonlin, iVort);
//}

// Update each global nodal state using spherical means
void UpdateNodeGlobal(int *INode,
                      Mesh2D *Ei,
                      const double &dt, 
                      int *nINode,
                      const int &ninode, 
                      const int &maxelem,
                      Euler2D *QNtemp,
                      const int &iNonlin,
                      const int &iVort) {
        int index;
        int elemid, nodeid;

  
        Euler2D tempx, tempy;
        for (int i = 0; i < ninode; i++) {
                index = nINode[i];
                //QNtemp[index].Zero();
                // for each node, update using the elements around it
                for (int k = 0; k < maxelem; k++) {
                        elemid = INode[index*2*maxelem + 2*k];
                        nodeid = INode[index*2*maxelem + 2*k+1];
                        if (elemid >= 0) 
                                QNtemp[index] += Ei[elemid].NodeFluxResidual(2*nodeid, dt,
                                                                             tempx, tempy);
                }
        }
        // cout << "Nodal update successful" << endl;
};

