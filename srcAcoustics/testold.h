// Wrapper test file to check passing arrays into and out of c
// 
// 8 May 2015 - Initial creation (Maeng)
//
#ifndef _TEST_INCLUDED
#define _TEST_INCLUDED

/* Include required C++ libraries. */

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cmath>

using namespace std;

extern "C"
{
    void testfunc_(int *scalarval ); 
    void test1darray_(int *array, int *n);
    void test2darray_(double *array, int *n);
    void test3darray_(double *array, int *n);
    void test4darray_(double *array, int *n);
}

#endif
