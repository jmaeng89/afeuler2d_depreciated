/* Sphmeans.cc:  Spherical means subroutines for 2D nonlinear Mesh Class. */

/* Include 2D mesh elements header file. */
#ifndef _MESH2D_INCLUDED
#include "Mesh2D.h"
#endif // _MESH2D_INCLUDED

double Mesh2D::constflux (const int &inode) {
        if (inode == NP-1)
                return (ONE);
        else if (inode%2 == 0) 
                return (HALF*angle[inode/2]/PI);
        else 
                return (HALF);
};

double Mesh2D::constflux (const int &inode) const {
        if (inode == NP-1)
                return (ONE);
        else if (inode%2 == 0) 
                return (HALF*angle[inode/2]/PI);
        else 
                return (HALF);
};
        
double Mesh2D::xiflux (const double &flux_x,
                       const double &flux_y,
                       const int &inode) {
        double constL = constflux(inode);
        double xi = J11()*flux_x + J12()*flux_y + xip(inode)*constL;
        return (xi);
};
double Mesh2D::xiflux (const double &flux_x,
                       const double &flux_y,
                       const int &inode) const {
        double constL = constflux(inode);
        double xi = J11()*flux_x + J12()*flux_y + xip(inode)*constL;
        return (xi);
};
  
double Mesh2D::etaflux (const double &flux_x,
                        const double &flux_y,
                        const int &inode) {
        double constL = constflux(inode);
        double eta = J21()*flux_x + J22()*flux_y + etap(inode)*constL;
        return (eta);
};
double Mesh2D::etaflux (const double &flux_x,
                        const double &flux_y,
                        const int &inode) const {
        double constL = constflux(inode);
        double eta = J21()*flux_x + J22()*flux_y + etap(inode)*constL;
        return (eta);
};
  
double Mesh2D::xi2flux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const double &flux_x2,
                        const double &flux_y2,
                        const double &flux_xy,
                        const int &inode) {
        double A = J11();    double B = J12();
        double xi2, x2, y2, xic;
        // Adding Lagrangian approximation
        xic = xip(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi2 = (A*A*x2 + 2*A*B*flux_xy + B*B*y2) + 2*xic*(A*flux_x + B*flux_y) + xic*xic*constL;
        return (xi2);
};
double Mesh2D::xi2flux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const double &flux_x2,
                        const double &flux_y2,
                        const double &flux_xy,
                        const int &inode) const {
        double A = J11();    double B = J12();
        double xi2, x2, y2, xic;
        // Adding Lagrangian approx
        xic = xip(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi2 = (A*A*x2 + 2*A*B*flux_xy + B*B*y2) + 2*xic*(A*flux_x + B*flux_y) + xic*xic*constL;
        return (xi2);
};

double Mesh2D::eta2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) {
        double A = J21();    double B = J22();
        double eta2, x2, y2, etac;
        // Add Lagrangian approximation
        etac = etap(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        eta2 = (A*A*x2 + 2*A*B*flux_xy + B*B*y2) + 2*etac*(A*flux_x + B*flux_y) + etac*etac*constL;
        return (eta2); 
};
double Mesh2D::eta2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) const {
        double A = J21();    double B = J22();
        double eta2, x2, y2, etac;
        // Add Lagrangian approximation
        etac = etap(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        eta2 = (A*A*x2 + 2*A*B*flux_xy + B*B*y2) + 2*etac*(A*flux_x + B*flux_y) + etac*etac*constL;
        return (eta2); 
};

double Mesh2D::xietaflux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double xieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xieta = (A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + etap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*(A3*flux_x + A4*flux_y)
                + xietap(inode)*constL;
        return (xieta);
};
double Mesh2D::xietaflux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double xieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xieta = (A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + etap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*(A3*flux_x + A4*flux_y) 
                + xietap(inode)*constL;
        return (xieta);
};

double Mesh2D::xi3flux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const double &flux_x2,
                        const double &flux_y2,
                        const double &flux_xy,
                        const double &flux_x3,
                        const double &flux_y3,
                        const double &flux_x2y,
                        const double &flux_xy2,
                        const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double xi3, x2, y2;
        double xi_p = xip(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi3 = (A1*A1*A1*flux_x3 + A2*A2*A2*flux_y3 + 3*A1*A1*A2*flux_x2y + 3*A1*A2*A2*flux_xy2)
                + 3.0*xi_p*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 3.0*xi_p*xi_p*(A1*flux_x + A2*flux_y)
                + xi_p*xi_p*xi_p*constL;
        return (xi3);
};
double Mesh2D::xi3flux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const double &flux_x2,
                        const double &flux_y2,
                        const double &flux_xy,
                        const double &flux_x3,
                        const double &flux_y3,
                        const double &flux_x2y,
                        const double &flux_xy2,
                        const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double xi3, x2, y2;
        double xi_p = xip(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi3 = (A1*A1*A1*flux_x3 + A2*A2*A2*flux_y3 + 3*A1*A1*A2*flux_x2y + 3*A1*A2*A2*flux_xy2)
                + 3.0*xi_p*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 3.0*xi_p*xi_p*(A1*flux_x + A2*flux_y)
                + xi_p*xi_p*xi_p*constL;
        return (xi3);
};

double Mesh2D::eta3flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const double &flux_x3,
                         const double &flux_y3,
                         const double &flux_x2y,
                         const double &flux_xy2,
                         const int &inode) {
        double A3 = J21();    double A4 = J22();
        double eta3, x2, y2;
        double eta_p = etap(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        eta3 = (A3*A3*A3*flux_x3 + A4*A4*A4*flux_y3 + 3*A3*A3*A4*flux_x2y + 3*A3*A4*A4*flux_xy2)
                + 3.0*eta_p*(A3*A3*x2 + 2*A3*A4*flux_xy + A4*A4*y2)
                + 3.0*eta_p*eta_p*(A3*flux_x + A4*flux_y)
                + eta_p*eta_p*eta_p*constL;
        return (eta3);
};
double Mesh2D::eta3flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const double &flux_x3,
                         const double &flux_y3,
                         const double &flux_x2y,
                         const double &flux_xy2,
                         const int &inode) const {
        double A3 = J21();    double A4 = J22();
        double eta3, x2, y2;
        double eta_p = etap(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        eta3 = (A3*A3*A3*flux_x3 + A4*A4*A4*flux_y3 + 3*A3*A3*A4*flux_x2y + 3*A3*A4*A4*flux_xy2)
                + 3.0*eta_p*(A3*A3*x2 + 2*A3*A4*flux_xy + A4*A4*y2)
                + 3.0*eta_p*eta_p*(A3*flux_x + A4*flux_y)
                + eta_p*eta_p*eta_p*constL;
        return (eta3);
};
        
double Mesh2D::xi2etaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const double &flux_x3,
                           const double &flux_y3,
                           const double &flux_x2y,
                           const double &flux_xy2,
                           const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double xi2eta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi2eta = (A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                  + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y)
                + xip(inode)*xietap(inode)*constL;
        return (xi2eta);
};
double Mesh2D::xi2etaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const double &flux_x3,
                           const double &flux_y3,
                           const double &flux_x2y,
                           const double &flux_xy2,
                           const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double xi2eta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xi2eta = (A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                  + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y)
                + xip(inode)*xietap(inode)*constL;
        return (xi2eta);
};

double Mesh2D::xieta2flux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const double &flux_x3,
                           const double &flux_y3,
                           const double &flux_x2y,
                           const double &flux_xy2,
                           const int &inode) {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double xieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xieta2 = (A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                  + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y)
                + etap(inode)*xietap(inode)*constL;
        return (xieta2);
};
double Mesh2D::xieta2flux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const double &flux_x3,
                           const double &flux_y3,
                           const double &flux_x2y,
                           const double &flux_xy2,
                           const int &inode) const {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double C1 = A3*nx[0] + A4*ny[0];
        double C2 = A1*nx[0] + A2*ny[0];
        double xieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        xieta2 = (A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                  + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y)
                + etap(inode)*xietap(inode)*constL;
        return (xieta2);
};

double Mesh2D::dxiflux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const int &inode) {
        double dxi;
        dxi = (J11()*flux_x + J12()*flux_y)/R;
        return (dxi);
};
double Mesh2D::dxiflux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const int &inode) const {
        double dxi;
        dxi = (J11()*flux_x + J12()*flux_y)/R;
        return (dxi);
};
  
double Mesh2D::detaflux (const double &R, 
                         const double &flux_x,
                         const double &flux_y,
                         const int &inode){
        double deta;
        deta = (J21()*flux_x + J22()*flux_y)/R;
        return (deta);
};
double Mesh2D::detaflux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const int &inode) const{
        double deta;
        deta = (J21()*flux_x + J22()*flux_y)/R;
        return (deta);
};
  
double Mesh2D::dxi2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) {
        double A = J11();    double B = J12();
        double dxi2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxi2 = 2.0*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*xip(inode)*(A*flux_x + B*flux_y);
        dxi2 /= R;
        return (dxi2);
};
double Mesh2D::dxi2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) const {
        double A = J11();    double B = J12();
        double dxi2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        // Add correction term
        dxi2 = 2.0*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*xip(inode)*(A*flux_x + B*flux_y);
        dxi2 /= R;
        return (dxi2);
};

double Mesh2D::deta2flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) {
        double A = J21();    double B = J22();
        double deta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        deta2 = 2.0*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*etap(inode)*(A*flux_x + B*flux_y);
        deta2 /= R;
        return (deta2); 
};
double Mesh2D::deta2flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) const {
        double A = J21();    double B = J22();
        double deta2, x2, y2;
        // Add Lagrangian approx
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        deta2 = 2.0*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*etap(inode)*(A*flux_x + B*flux_y);
        deta2 /= R;
        return (deta2); 
};

double Mesh2D::dxietaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double dxieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxieta = 2.0*(A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + etap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*(A3*flux_x + A4*flux_y);
        dxieta /= R;
        return (dxieta);
};
double Mesh2D::dxietaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double dxieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxieta = 2.0*(A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + etap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*(A3*flux_x + A4*flux_y);
        dxieta /= R;
        return (dxieta);
};

double Mesh2D::dxi2etaflux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double dxi2eta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxi2eta = 3.0*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 2.0*etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2.0*2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y);
        dxi2eta /= R;
        return (dxi2eta);
};
double Mesh2D::dxi2etaflux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double dxi2eta, x2, y2;
        // Add Lagranginan approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxi2eta = 3.0*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 2.0*etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2.0*2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y);
        dxi2eta /= R;
        return (dxi2eta);
};

double Mesh2D::dxieta2flux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double dxieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxieta2 = 3.0*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 2.0*xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2.0*2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y);
        dxieta2 /= R;
        return (dxieta2);
};
double Mesh2D::dxieta2flux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) const {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double dxieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        dxieta2 = 3.0*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 2.0*xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 2.0*2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + 2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y);
        dxieta2 /= R;
        return (dxieta2);
};


double Mesh2D::rconstflux (const double &R,
                           const int &inode) {
        if (inode == NP-1)
                return (HALF*R*R);
        else if (inode%2 == 0) 
                return (0.25*R*R*angle[inode/2]/PI);
        else 
                return (0.25*R*R);
};
double Mesh2D::rconstflux (const double &R,
                           const int &inode) const {
        if (inode == NP-1)
                return (HALF*R*R);
        else if (inode%2 == 0) 
                return (0.25*R*R*angle[inode/2]/PI);
        else 
                return (0.25*R*R);
};

double Mesh2D::rxiflux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const int &inode) {
        double rxi = THIRD*(J11()*flux_x + J12()*flux_y)
                + HALF*xip(inode)*constflux(inode);
        rxi *= R*R;
        return (rxi);
};
double Mesh2D::rxiflux (const double &R,
                        const double &flux_x,
                        const double &flux_y,
                        const int &inode) const {
        double rxi = THIRD*(J11()*flux_x + J12()*flux_y)
                + HALF*xip(inode)*constflux(inode);
        rxi *= R*R;
        return (rxi);
};
  
double Mesh2D::retaflux (const double &R, 
                         const double &flux_x,
                         const double &flux_y,
                         const int &inode) {
        double reta = THIRD*(J21()*flux_x + J22()*flux_y) 
                + HALF*etap(inode)*constflux(inode);
        reta *= R*R;
        return (reta);
};
double Mesh2D::retaflux (const double &R, 
                         const double &flux_x,
                         const double &flux_y,
                         const int &inode) const {
        double reta = THIRD*(J21()*flux_x + J22()*flux_y)
                + HALF*etap(inode)*constflux(inode);
        reta *= R*R;
        return (reta);
};
  
double Mesh2D::rxi2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) {
        double A = J11();    double B = J12();
        double rxi2, x2, y2, xic;
        // Adding Lagrangian approximation
        xic = xip(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi2 = 0.25*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*THIRD*xic*(A*flux_x + B*flux_y)
                + HALF*xic*xic*constL;
        rxi2 *= R*R;
        return (rxi2);
};
double Mesh2D::rxi2flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const int &inode) const {
        double A = J11();    double B = J12();
        double rxi2, x2, y2, xic;
        // Adding Lagrangian approx
        xic = xip(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi2 = 0.25*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*THIRD*xic*(A*flux_x + B*flux_y)
                + HALF*xic*xic*constL;
        rxi2 *= R*R;
        return (rxi2);
};

double Mesh2D::reta2flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) {
        double A = J21();    double B = J22();
        double reta2, x2, y2, etac;
        // Add Lagrangian approximation
        etac = etap(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        reta2 = 0.25*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*THIRD*etac*(A*flux_x + B*flux_y)
                + HALF*etac*etac*constL;
        reta2 *= R*R;
        return (reta2); 
};
double Mesh2D::reta2flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const int &inode) const {
        double A = J21();    double B = J22();
        double reta2, x2, y2, etac;
        // Add Lagrangian approximation
        etac = etap(inode);
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        reta2 = 0.25*(A*A*x2 + 2*A*B*flux_xy + B*B*y2)
                + 2*THIRD*etac*(A*flux_x + B*flux_y)
                + HALF*etac*etac*constL;
        reta2 *= R*R;
        return (reta2); 
};

double Mesh2D::rxietaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double rxieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxieta = 0.25*(A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*etap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*xip(inode)*(A3*flux_x + A4*flux_y)
                + HALF*xietap(inode)*constL;
        rxieta *= R*R;
        return (rxieta);
};
double Mesh2D::rxietaflux (const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy,
                           const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double rxieta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxieta = 0.25*(A1*A3*x2 + (A1*A4 + A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*etap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*xip(inode)*(A3*flux_x + A4*flux_y) 
                + HALF*xietap(inode)*constL;
        rxieta *= R*R;
        return (rxieta);
};

double Mesh2D::rxi3flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const double &flux_x3,
                         const double &flux_y3,
                         const double &flux_x2y,
                         const double &flux_xy2,
                         const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double rxi3, x2, y2;
        double xic = xip(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi3 = 0.2*(A1*A1*A1*flux_x3 + A2*A2*A2*flux_y3 + 3*A1*A1*A2*flux_x2y + 3*A1*A2*A2*flux_xy2)
                + 0.25*3*xic*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + xic*xic*(A1*flux_x + A2*flux_y)
                + HALF*xic*xic*xic*constL;
        rxi3 *= R*R;
        return (rxi3);
};
double Mesh2D::rxi3flux (const double &R,
                         const double &flux_x,
                         const double &flux_y,
                         const double &flux_x2,
                         const double &flux_y2,
                         const double &flux_xy,
                         const double &flux_x3,
                         const double &flux_y3,
                         const double &flux_x2y,
                         const double &flux_xy2,
                         const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double rxi3, x2, y2;
        double xic = xip(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi3 = 0.2*(A1*A1*A1*flux_x3 + A2*A2*A2*flux_y3 + 3*A1*A1*A2*flux_x2y + 3*A1*A2*A2*flux_xy2)
                + 0.25*3.0*xic*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + xic*xic*(A1*flux_x + A2*flux_y)
                + HALF*xic*xic*xic*constL;
        rxi3 *= R*R;
        return (rxi3);
};

double Mesh2D::reta3flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const double &flux_x3,
                          const double &flux_y3,
                          const double &flux_x2y,
                          const double &flux_xy2,
                          const int &inode) {
        double A3 = J21();    double A4 = J22();
        double reta3, x2, y2;
        double etac = etap(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        reta3 = 0.2*(A3*A3*A3*flux_x3 + A4*A4*A4*flux_y3 + 3*A3*A3*A4*flux_x2y + 3*A3*A4*A4*flux_xy2)
                + 0.25*3.0*etac*(A3*A3*x2 + 2*A3*A4*flux_xy + A4*A4*y2)
                + etac*etac*(A3*flux_x + A4*flux_y)
                + HALF*etac*etac*etac*constL;
        reta3 *= R*R;
        return (reta3);
};
double Mesh2D::reta3flux (const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const double &flux_x3,
                          const double &flux_y3,
                          const double &flux_x2y,
                          const double &flux_xy2,
                          const int &inode) const {
        double A3 = J21();    double A4 = J22();
        double reta3, x2, y2;
        double etac = etap(inode);
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        reta3 = 0.2*(A3*A3*A3*flux_x3 + A4*A4*A4*flux_y3 + 3*A3*A3*A4*flux_x2y + 3*A3*A4*A4*flux_xy2)
                + 0.25*3.0*etac*(A3*A3*x2 + 2*A3*A4*flux_xy + A4*A4*y2)
                + etac*etac*(A3*flux_x + A4*flux_y)
                + HALF*etac*etac*etac*constL;
        reta3 *= R*R;
        return (reta3);
};
        
double Mesh2D::rxi2etaflux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double rxi2eta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi2eta = 0.2*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 0.25*etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 0.25*2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y)
                + HALF*xip(inode)*xietap(inode)*constL;
        rxi2eta *= R*R;
        return (rxi2eta);
};
double Mesh2D::rxi2etaflux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) const {
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double rxi2eta, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxi2eta = 0.2*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 0.25*etap(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 0.25*2*xip(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*xip(inode)*xip(inode)*(A3*flux_x + A4*flux_y)
                + HALF*xip(inode)*xietap(inode)*constL;
        rxi2eta *= R*R;
        return (rxi2eta);
};

double Mesh2D::rxieta2flux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double rxieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxieta2 = 0.2*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 0.25*xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 0.25*2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y)
                + HALF*etap(inode)*xietap(inode)*constL;
        rxieta2 *= R*R;
        return (rxieta2);
};
double Mesh2D::rxieta2flux (const double &R,
                            const double &flux_x,
                            const double &flux_y,
                            const double &flux_x2,
                            const double &flux_y2,
                            const double &flux_xy,
                            const double &flux_x3,
                            const double &flux_y3,
                            const double &flux_x2y,
                            const double &flux_xy2,
                            const int &inode) const {
        double A1 = J21();    double A2 = J22();
        double A3 = J11();    double A4 = J12();
        double C1 = A3*nx[0] + A4*ny[0];
        double C2 = A1*nx[0] + A2*ny[0];
        double rxieta2, x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;
        rxieta2 = 0.2*(A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                       + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2)
                + 0.25*xip(inode)*(A1*A1*x2 + 2*A1*A2*flux_xy + A2*A2*y2)
                + 0.25*2*etap(inode)*(A1*A3*x2 + (A1*A4+A2*A3)*flux_xy + A2*A4*y2)
                + THIRD*2*xietap(inode)*(A1*flux_x + A2*flux_y)
                + THIRD*etap(inode)*etap(inode)*(A3*flux_x + A4*flux_y)
                + HALF*etap(inode)*xietap(inode)*constL;
        rxieta2 *= R*R;
        return (rxieta2);
};


