// Solve barotropic acoustics equations using Active Flux Solver
// Doreen Fan

#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

#define    TEST_ON    1234
#define    TEST_OFF   1235

using namespace std;

// Include header files.
#include "Mesh2D.h"


// Determine total Q (conservaton)
Euler2D totalQ(Mesh2D *Ei, const int &nelem) {
        Euler2D QEtot = Ei[0].Qavg*Ei[0].A();
        for (int i = 1; i < nelem; i++) {
                QEtot += Ei[i].Qavg*Ei[i].A();
        }
        return (QEtot);
}

// Determine total integral (accuracy test)
double totalIntegral(Mesh2D *Ei, const int &nelem) {
        double Qint = Ei[0].IntegralFunction(1.0);
        for (int i = 1; i < nelem; i++) {
                Qint += Ei[i].IntegralFunction(1.0);
        }
        return (Qint);
}
    
int main ()
{
        // Initialize initial variables
        int n;                    // number of elements
        Mesh2D *Ei;               // Total elements
  
        // ------------------------  Reading input from files --------------------- /
        // Node coordinates (n1, n2, n3) of each element
        ifstream e2nfile; 
        e2nfile.open("../../data/mesh.E2NC");

        // if file couldn't be opened
        if(!e2nfile) { 
                cerr << "Error: E2NC file could not be opened" << endl;
                exit(1);
        }
        e2nfile >> n;

        // INITIALIZE MESH ELEMENTS
        Ei = new Mesh2D [n];        // elements
  
        // Temporary variables
        int count = 0;
        int subcount = 0;
        double nx, ny, nid;
        while ( !e2nfile.eof() ) {
                e2nfile >> nx >> ny >> nid;
                if (count < n) {
                        Ei[count].nx[subcount] = nx;
                        Ei[count].ny[subcount] = ny;
                        Ei[count].nid[subcount] = nid - 1;   // node # starts at 0
                        subcount += 1;
    
                        if (subcount%NQ == 0) {
                                Ei[count].InitAngle();       // Calculate vertex angles
                                subcount = 0;
                                count += 1;
                        }
                }
        }
        e2nfile.close();

        // Interior edge to elements relations
        ifstream i2efile;
        int nedge;
        i2efile.open("../../data/mesh.I2E");
        if(!i2efile) { 
                cerr << "Error: Euler2D file could not be opened" << endl;
                exit(1);
        }
        i2efile >> nedge;  

        // Declare variables
        count = 0;
        int eL, nodeL, eR, nodeR;
        int *IEdgeL;     // Left elements of the edge
        int *IEdgeR;     // Right elements of the edge
        IEdgeL = new int [2*nedge];  
        IEdgeR = new int [2*nedge];
        while ( !i2efile.eof() ) {
                i2efile >> eL >> nodeL >> eR >> nodeR;
                if (count < nedge) {
                        IEdgeL[2*count] = eL - 1;         // Element number starts at 0
                        IEdgeL[2*count+1] = nodeL - 1;    // Node number starts at 0
                        IEdgeR[2*count] = eR - 1;         // Element number starts at 0
                        IEdgeR[2*count+1] = nodeR - 1;
                        count += 1;
                }
        }
        i2efile.close();

        // Conservative cells
        ifstream celemfile;
        int cn;
        celemfile.open("../../data/mesh.cE");
        if(!celemfile) { 
                cerr << "Error: Euler2D cE file could not be opened" << endl;
                exit(1);
        }
        celemfile >> cn;  
        
        // Declare variables
        count = 0;
        int *cElem = new int [cn];
        while ( !celemfile.eof() ) {
                celemfile >> eL;
                if (count < cn) {
                        cElem[count] = eL - 1;           // Global node number starts at 0
                        count += 1;
                }
        }
        celemfile.close(); 

        // Interior nodes
        ifstream intnfile;
        int ninode;
        intnfile.open("../../data/mesh.Nin");
        if(!intnfile) { 
                cerr << "Error: Euler2D file could not be opened" << endl;
                exit(1);
        }
        intnfile >> ninode;  
        
        // Declare variables
        count = 0;
        int *nINode = new int [ninode];
        while ( !intnfile.eof() ) {
                intnfile >> nodeL;
                if (count < ninode) {
                        nINode[count] = nodeL - 1;           // Global node number starts at 0
                        count += 1;
                }
        }
        intnfile.close(); 
        
        // Node to element relation
        ifstream n2efile;
        int nnode, maxelem;
        n2efile.open("../../data/mesh.N2E");
        if(!n2efile) { 
                cerr << "Error: Euler2D file could not be opened" << endl;
                exit(1);
        }
        n2efile >> nnode;
        n2efile >> maxelem;

        // Declare variables
        count = 0;
        subcount = 0;
        int *INode = new int [2*nnode*maxelem];        // (nnode x 2*maxelem) matrix
        while ( !n2efile.eof() ) {
                n2efile >> eL >> nodeL;
                if (count < nnode) {
                        INode[2*maxelem*count + 2*subcount] = eL - 1;           // Element number starts at 0
                        INode[2*maxelem*count + 2*subcount+1] = nodeL - 1;      // Local node number starts at 0
                        subcount += 1;
      
                        if (subcount%maxelem == 0) {
                                subcount = 0;
                                count += 1;
                        }
                }
        }
        n2efile.close();

        
        // Initial state of each global node
        ifstream QNfile;
        double cmax;
        QNfile.open("../../data/QN.in");
        if(!QNfile) { 
                cerr << "Error: Euler2D file could not be opened" << endl;
                exit(1);
        }
        QNfile >> nnode;  // default = 3*n
        QNfile >> cmax;

        // Declare variables
        count = 0;
        double din, uin, vin, pin;
        Euler2D *QN;
        QN = new Euler2D [nnode];  // global nodes

        while ( !QNfile.eof() ) {
                QNfile >> din >> uin >> vin >> pin;
                if (count < nnode) {
                        QN[count] = Euler2D(din, uin, vin, pin);
                        QN[count].setgas();
                        count += 1;
                }
        }
        QNfile.close();

        // Nodal initial states of each element
        ifstream QEfile;
        QEfile.open("../../data/QE.in");
        if(!QEfile) { 
                cerr << "Error: Euler2D file could not be opened" << endl;
                exit(1);
        }
        QEfile >> n; 
        QEfile >> cmax;

        count = 0;
        subcount = 0;
        while ( !QEfile.eof() ) {
                QEfile >> din >> uin >> vin >> pin;
                if (count < n) {
                        Ei[count].Q[subcount] = Euler2D(din, uin, vin, pin);
                        Ei[count].Q[subcount].setgas();
                        subcount += 1;
    
                        if (subcount%(NP-1) == 0) {
                                Ei[count].InitAverage();
                                subcount = 0;
                                count += 1;
                        }
                }
        }
        QEfile.close();

        cout << Ei[0].Qavg << endl;
        cout << n << " " << nedge << " " << ninode << " / " << nnode << endl;
  
        // -------------------------------- Initialize variables ------------------------- /
        double CFL = 1.0;           // CFL number (<= 1)
        int maxtn;                  // maximum number of timesteps taken
        double telap = 0.1*(0.1/173.6397131994867);         // time elapsed
        int iNonlinear = NONLINEAR_ON;
        int iVorticity = VORTICITY_ON;

        Euler2D *QNthalf;           // Global nodal states at half timestep
        Euler2D *QNt1;              // Global nodal states at full timestep
        Mesh2D *Etemp;              // Temporary set of elements
        Mesh2D *Ethalf;             // Temporary set of elements used at half timestep
        Mesh2D *Et1;                // Temporary set of elements used at full timestep
        QNthalf = new Euler2D [nnode];
        QNt1 = new Euler2D [nnode];
        Etemp = new Mesh2D [n];
        Ethalf = new Mesh2D [n];
        Et1 = new Mesh2D [n];

        Euler2D newQavg;           // New average cell state after full timestep
        Euler2D Flux;
        Euler2D origQtot, newQtot; // Original and new total Q value of domain (conservation)
  
        double dt;                  // timestep
        double s, sg;               // global maximum wave speed

        int i, j, k;
        count = 0;                  // reset counter


        // ------------------------------------ PROGRAM ---------------------------------- /

        origQtot = totalQ(Ei, n);
        
        // Determine global timestep
        sg = ONE;
        for (i = 0; i < n; i++) {
                // Initial copy of temporary set of elements
                Etemp[i].Copy(Ei[i]);
                Ethalf[i].Copy(Ei[i]);
                Et1[i].Copy(Ei[i]);

                s = Ei[i].lmin();
                sg = min(s, sg);
        }
        
        dt = CFL*sg/cmax;
        maxtn = telap/dt;
        dt = telap/(maxtn + 1);    // resetting time step to equal steps to reach final time
        cout << QN[0].c() << " " << Ei[0].Q[6].c() << "  " << cmax << endl;
        cout << telap/(maxtn + 1) << " / " << dt << " " << maxtn << endl;
        // dt = 0.025;
        // maxtn = 19;

        /* ------------------ DETERMINE RESIDUAL -- Explicit time=marching ----------------- */

        for (j = 0; j < 1+maxtn; j++) {
                
                // Setting temporary set of elements to values in previous step
                for (i = 0; i < n; i++) {
                        Etemp[i].CopyState(Ei[i]);
                        Ethalf[i].CopyState(Ei[i]);
                }
                for (i = 0; i < nnode; i++) {
                        QNthalf[i].Copy(QN[i]);
                        QNt1[i].Copy(QN[i]);
                }

                // ~~~~~~~~~~~~~~~~~~ Update interior edge nodes ~~~~~~~~~~~~~~~~ //
                // At half timestep
                for (i = 0; i < nedge; i++) {
                        eL = IEdgeL[2*i];
                        eR = IEdgeR[2*i];
                        if (eR < 0) {
                                nodeL = IEdgeL[2*i+1];
                                Ethalf[eL].Q[nodeL] += Ei[eL].EdgeFluxResidual(nodeL, HALF*dt,
                                                                             iNonlinear, iVorticity);
                        }
                        else {
                                UpdateEdgeFlux(Ei[eL], IEdgeL[2*i+1],
                                               Ei[eR], IEdgeR[2*i+1],
                                               HALF*dt, 
                                               Ethalf[eL], Ethalf[eR],
                                               iNonlinear, iVorticity);
                        }
                }
                //cout << "half timestep edge update complete" << endl;
                        
                // for (i = 0; i < nedge; i++) {
                //         eL = IEdgeL[2*i];
                //         eR = IEdgeR[2*i];
                //         nodeL = IEdgeL[2*i+1];
                //         nodeR = IEdgeR[2*i+1];
                //         Ethalf[eL].Q[nodeL] = Etemp[eL].Q[nodeL];
                //         Ethalf[eR].Q[nodeR] = Etemp[eR].Q[nodeR];
                // }
    
                // At full timestep
                for (i = 0; i < nedge; i++) {
                        eL = IEdgeL[2*i];
                        eR = IEdgeR[2*i];
                        if (eR < 0) {
                                nodeL = IEdgeL[2*i+1];
                                Et1[eL].Q[nodeL] += Ei[eL].EdgeFluxResidual(nodeL, dt,
                                                                            iNonlinear, iVorticity);
                        }
                        else {
                                UpdateEdgeFlux(Ei[eL], IEdgeL[2*i+1],
                                               Ei[eR], IEdgeR[2*i+1],
                                               dt, 
                                               Et1[eL], Et1[eR],
                                               iNonlinear, iVorticity);
                        }
                }
                //cout << "full timestep edge update complete" << endl;
                        
                // for (i = 0; i < nedge; i++) {
                //         eL = IEdgeL[2*i];
                //         eR = IEdgeR[2*i];
                //         nodeL = IEdgeL[2*i+1];
                //         nodeR = IEdgeR[2*i+1];
                //         Et1[eL].Q[nodeL] = Etemp[eL].Q[nodeL];
                //         Et1[eR].Q[nodeR] = Etemp[eR].Q[nodeR];
                // }
                // cout << "Edge Update Successful" << endl;
                
                
                // ~~~~~~~~~~~~~~ Update global vertex nodes ~~~~~~~~~~~~~~ //
                int nodeid;
                // Update each element at vertex nodes (no bubble function)
                UpdateNodeGlobal(INode, Ei, HALF*dt, nINode, ninode, maxelem, QNthalf,
                                 iNonlinear, iVorticity);
                for (i = 0; i < n; i++) {
                        for (k = 0; k < NQ; k++) {
                                nodeid = Ei[i].nid[k];
                                Ethalf[i].Q[2*k].CopyState(QNthalf[nodeid]);
                        }
                }

                UpdateNodeGlobal(INode, Ei, dt, nINode, ninode, maxelem, QNt1,
                                 iNonlinear, iVorticity);
                for (i = 0; i < n; i++) {
                        for (k = 0; k < NQ; k++) {
                                nodeid = Ei[i].nid[k];
                                Et1[i].Q[2*k].CopyState(QNt1[nodeid]);
                        }
                }
                
                // ~~~~~~~ Determine cell-averages after full timestep and update bubble function ~~~~~~~ //
                // looping through conservative elements
                int elemid;
                for (i = 0; i < n; i++) {
                        //elemid = cElem[i];
                        elemid = i;
                        newQavg = AverageFluxResidual(Ei[elemid], Ethalf[elemid], Et1[elemid], dt);
                        //Et1[elemid].SetBubbleFunction(newQavg);
                        Et1[elemid].SetAverage(newQavg);
                }
                
                // Copy new state values to old state values
                for (i = 0; i < nnode; i++) {
                        QN[i] = QNt1[i];
                }
                for (i = 0; i < n; i++) {
                        Ei[i].CopyState(Et1[i]);
                }
    
                // Outputting progress
                if (j%10 == 0)
                        cout << ".";
                if (j%500 == 0)
                        cout << " " << endl;
        }

        
        // -------------------------- Outputting to files --------------------------- /
        cout << " " << endl;
        cout << "loops: "<< j << endl;
        cout << "time elapsed: " << j*dt << endl;

        newQtot = totalQ(Ei, n);
        cout.precision(15);
        cout << "Original conservation value: " << origQtot << endl;
        cout << "New conservation value: " << newQtot << endl;
        cout << "Conservation difference: " << newQtot - origQtot << endl;
        
        // Solution state
        // global node
        ofstream QNoutput;
        QNoutput.open("../../data/QN.out");

        if (!QNoutput) {
                cerr << "Can't open output file " << QNoutput << endl;
                exit(1);
        }
        QNoutput.precision(15);
        for (i=0; i<nnode; i++) {
                QNoutput << QN[i] << endl;
        }
        QNoutput.close();
  
        // per element
        double xout, yout;
        int temp;
        ofstream QEoutput;
        QEoutput.open("../../data/QE.out");

        if (!QEoutput) {
                cerr << "Can't open output file " << QEoutput << endl;
                exit(1);
        }
        QEoutput.precision(15);
        for (i=0; i<n; i++) {
                for (j=0; j<(NP-1); j++) {
                        temp = ((j+1)%(NP-1))/2;
                        xout = HALF*(Ei[i].nx[j/2]+Ei[i].nx[temp]);
                        yout = HALF*(Ei[i].ny[j/2]+Ei[i].ny[temp]);
                        QEoutput << xout << " " << yout << " " << Ei[i].Q[j] << endl;
                        //QEoutput << Ei[i].Q[j] << endl;
                }
        }
        QEoutput.close();

        QEoutput.open("../../data/QEbubb.out");
        if (!QEoutput) {
                cerr << "Can't open output file " << QEoutput << endl;
                exit(1);
        }
        QEoutput.precision(15);
        for (i=0; i<n; i++) {
                QEoutput << Ei[i].Q[NP-1] << endl;
        }
        QEoutput.close();

        QEoutput.open("../../data/QEavg.out");
        if (!QEoutput) {
                cerr << "Can't open output file " << QEoutput << endl;
                exit(1);
        }
        QEoutput.precision(15);
        for (i=0; i<n; i++) {
                QEoutput << Ei[i].Qavg << endl;
        }
        QEoutput.close();

        // Integral calculations (convergence test)
        double Qtotint = totalIntegral(Ei, n);
        cout << "Total integral: " << Qtotint << endl;
        
        return 0;
}
