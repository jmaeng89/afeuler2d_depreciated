#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <string>
#include <stdio.h>

using namespace std;

extern "C" {
    void cppfunc_(double *a, double *b);
    void cppfuncarrayint_(int *array);
    void cppfuncarrayreal_(double *array);
    void cppfuncarraymulti_(double *array, const int *row, const int *col);
    void cppfuncarraymulti2_(double *array, const int *r1, const int *r2, const int *r3);
    void test1darray_(double *array, int *n);
    void test2darray_(double *array, double *array2, int *n, int *nn);
    void test3darray_(double *array, int *n);
    void test4darray_(double *array, int *n);
}

void cppfunc_(double *a, double *b) {
    *a = 3.0;
    *b = 4.0;
}

void cppfuncarrayint_(int *array) {
    //array[0] = 1;
    array[1] = array[1]+1;
}

void cppfuncarrayreal_(double *array) {
    array[0] = array[0]+1.0;
    array[1] = array[1]+1.0;
}

void cppfuncarraymulti_(double *array, const int *row, const int *col) {
    int r = *row;
    int c = *col; 
    for ( int i = 0; i < r; i++ ) {
        for ( int j = 0; j < c; j++ ) {
            //array[r*i+j] = array[r*i+j]+1.0;
            cout << "index i: " << i+1 << endl;
            cout << "index j: " << j+1 << endl;
            cout << "array(" << i+1 << "," << j+1 << "): " << array[r*j+i] << endl;

        }
    }
    //int i = 2;  int j = 1; 
    //cout << array[i+j*r]  << endl;
    //cout << array[1] << endl;
    //cout << array[2] << endl;
    //cout << array[3] << endl;
    //cout << array[4] << endl;
    //cout << array[5] << endl;
}
void cppfuncarraymulti2_(double *array, const int *r1, const int *r2, const int *r3) {
    int I = *r1; 
    int J = *r2; 
    int K = *r3; 
    for ( int i = 0; i < I; i++ ) {
        for ( int j = 0; j < J; j++ ) {
            for ( int k = 0; k < K; k++ ) {
                //array[I*i+J*j+k] = array[I*i+J*j+k]+1.0;
                //cout << "index i :" << i << endl;
                //cout << "index j :" << j << endl;
                //cout << "index k :" << k << endl;
                //cout << "array(" << i+1 << ","<< j+1 << "," << k+1 << "): " << array[I*(i+J*j)+k] << endl;
                cout << "array(" << i+1 << ","<< j+1 << "," << k+1 << "): " << array[I*(J*k+j)+i] << endl;
            }
        }
    }
}

void test1darray_(double *array, int *n) {
    //int len = *n;
    for ( int i = 0; i < *n; i++ ) {
        array[i] = array[i]+1.1;
    }
    cout.precision(10);
    cout << array[1] << endl;
}

// test casting fortran continuous arrays into C++ multidimensional arrays
void test2darray_(double *array, double *array2, int *n, int *nn) {
    //typedef double (*A2d_t)[n[0]];
    typedef double (*A2d_t)[*nn];
    A2d_t A2d = (A2d_t) array; // cast to 2d array
    A2d_t A2d2 = (A2d_t) array2; // cast to 2d array

    for ( int j = 0; j < n[1]; j++ ) {
        for ( int i = 0; i < n[0]; i++ ) {
            A2d[j][i] = A2d[j][i]+1.0;
        }
    }

    //A2d2[0][1] = A2d2[0][1] + 1.0;
    //A2d2[0][2] = A2d2[0][2] - 1.0;
}

void test3darray_(double *array, int *n) {
    typedef double (*A3d_t)[n[1]][n[0]];
    A3d_t A3d = (A3d_t) array; // cast to 3d array

    for ( int k = 0; k < n[2]; k++ ) {
        for ( int j = 0; j < n[1]; j++ ) {
            for ( int i = 0; i < n[0]; i++ ) {
                A3d[k][j][i] = A3d[k][j][i]+1.0;
            }
        }
    }
}

void test4darray_(double *array, int *n) {
    typedef double (*A4d_t)[n[2]][n[1]][n[0]];
    A4d_t A4d = (A4d_t) array; // cast to 4d array

    for ( int l = 0; l < n[3]; l++ ) {
        for ( int k = 0; k < n[2]; k++ ) {
            for ( int j = 0; j < n[1]; j++ ) {
                for ( int i = 0; i < n[0]; i++ ) {
                    A4d[l][k][j][i] = A4d[l][k][j][i]+3.0;
                }
            }
        }
    }
}
