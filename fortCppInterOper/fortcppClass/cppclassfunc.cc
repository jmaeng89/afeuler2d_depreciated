// crectangle_class.cc
#include <iostream>
using namespace std;

class CRectangle {
  public:
    int width, height;
    CRectangle ();
    CRectangle (int,int);
    int area () {return (width*height);}
};
CRectangle::CRectangle (){
}
CRectangle::CRectangle (int a, int b) {
  width = a;
  height = b;
}

/* C wrapper interfaces to C++ routines */
extern "C" {
 /* CRectangle *CRectangle__new (int a, int b) {
    return new CRectangle(a, b);
  }
  int CRectangle__area (CRectangle *This) {
    return This->area();
  }
  void CRectangle__delete (CRectangle *This) {
    delete This;
  }*/
  void cfunc_();
}

void cfunc_() {
    cout << "hello " << endl;
    // CRectangle rect(3,4);

    int nx = 4; 
    CRectangle *rect;
    rect = new CRectangle[nx];

    for(int k=0;k<nx;k++)
    {
        rect[k].width  = k; 
        rect[k].height = k; 
    }

    for(int k=0;k<nx;k++)
    cout << k<< " area: " << rect[k].area() << endl;
}
/* example main() written in C++:
int main () {
  CRectangle rect;
  rect.set_values (3,4);
  cout << "area: " << rect.area();
  return 0;
}
*/
