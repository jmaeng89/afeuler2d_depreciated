program fprogram

    implicit none

    ! integer parameters
    integer, parameter :: FP = selected_real_kind(15,307)

    ! local variables
    integer :: i, j, k, l
    integer :: arrayI(2)
    !real*8 :: array1d(2)
    real(FP) :: array1d(2)
    real(FP) :: array2d(4,2)
    real(FP) :: array2d2(4,1)
    real(FP) :: array3d(4,3,2)
    real(FP) :: array4d(4,3,2,1)
    integer :: ndims1d(1) 
    integer :: ndims2d(2) 
    integer :: ndims3d(3) 
    integer :: ndims4d(4) 
    
    arrayI(1) = 12
    arrayI(2) = 12
    write(*,*) 
    write(*,*) 'int type array'
    write(*,*) 'before'
    write(*,*) arrayI
    call cppfuncarrayInt(arrayI) 
    write(*,*) 'after'
    write(*,*) arrayI

    ! 1d array
    ndims1d(:) = (/ (size(array1d, 1)) /)
    write(*,*) 
    write(*,*) '1d matrix'
    write(*,*) 'fortran indexing'
    do i = 1,ndims1d(1)
        array1d(i) = i
        write(*,*) 'array(', i, '): ', array1d(i)
    end do
    call test1darray(array1d, ndims1d)
    write(*,*) 'cpp indexing'
    do i = 1,ndims1d(1)
        write(*,*) 'array(', i, '): ', array1d(i)
    end do

    ! 2d array
    ndims2d(:) = (/ (size(array2d, i), i = 1,2) /)
    write(*,*) 
    write(*,*) '2d matrix'
    write(*,*) 'fortran indexing'
    do i = 1,ndims2d(1)
        do j = 1,ndims2d(2)
            ! assign array values
            array2d(i,j) = ndims2d(2)*(i-1)+(j-1) 
            write(*,*) 'array(', i, ',', j, '): ', array2d(i,j)
        end do
    end do
    array2d2(1,1) = 1.0
    array2d2(2,1) = 2.0
    array2d2(3,1) = 3.0
    array2d2(4,1) = 4.0
    write(*,*) array2d2
    write(*,*) 'cpp indexing'
    !call cppfuncarrayMulti(array2d, ndims2d(1), ndims2d(2)) 
    call test2darray(array2d, array2d2, ndims2d, 4)
    do i = 1,ndims2d(1)
        do j = 1,ndims2d(2)
            write(*,*) 'array(', i, ',', j, '): ', array2d(i,j)
        end do
    end do
    write(*,*) array2d2
    write(*,*)

    ! 3d
    ndims3d(:) = (/ (size(array3d, i), i = 1,3) /)
    write(*,*) 
    write(*,*) '3d matrix'
    write(*,*) 'fortran indexing'
    do i = 1,ndims3d(1)
        do j = 1,ndims3d(2)
            do k = 1,ndims3d(3)
                ! assign array values
                array3d(i,j,k) = ndims3d(2)*(ndims3d(3)*(j-1)+(k-1))+(i-1) 
                write(*,*) 'array(', i, ',', j, ',', k, '): ', array3d(i,j,k)
            end do
        end do
    end do
    write(*,*) 'cpp indexing'
    !call cppfuncarrayMulti2(array3d, 2,2,2)
    call test3darray(array3d, ndims3d)
    do i = 1,ndims3d(1)
        do j = 1,ndims3d(2)
            do k = 1,ndims3d(3)
                write(*,*) 'array(', i, ',', j, ',', k, '): ', array3d(i,j,k)
            end do
        end do
    end do

    ! 4d
    ndims4d(:) = (/ (size(array4d, i), i = 1,4) /)
    write(*,*) 
    write(*,*) '4d matrix'
    write(*,*) 'fortran indexing'
    do i = 1,ndims4d(1)
        do j = 1,ndims4d(2)
            do k = 1,ndims4d(3)
                do l = 1,ndims4d(4)
                    ! assign array values
                    array4d(i,j,k,l) = ndims4d(2)*(ndims4d(3)*(j-1)+(ndims4d(4)*(k-1)+(l-1)))+(i-1) 
                    write(*,*) 'array(', i, ',', j, ',', k, ',', l, '): ', array4d(i,j,k,l)
                end do
            end do
        end do
    end do
    write(*,*) 'cpp indexing'
    call test4darray(array4d, ndims4d)
    do i = 1,ndims4d(1)
        do j = 1,ndims4d(2)
            do k = 1,ndims4d(3)
                do l = 1,ndims4d(4)
                    write(*,*) 'array(', i, ',', j, ',', k, ',', l, '): ', array4d(i,j,k,l)
                end do
            end do
        end do
    end do


    stop
end program
