function[] = plotErrVortexPEAK(baseDir,caseDir)
% PLOTERRVORTEXPEAK(BASEDIR,CASEDIR) returns the vortex peak error convergence
%   result for isentropic vortex problem solved by Euler solver
%
%   J. Brad Maeng
%   22/4/2016 - created
%
    close all;

    %baseDir = '../cases/isenteuler/mvortex_unstL';
    %baseDir = '../cases/isenteuler/mvortv2_unstPhsError';
    %baseDir = '../cases/isenteuler/svortex_test';

    %baseDir = '../cases/euler/mvortex_cons2_unstLv2';
    %baseDir = '../cases/euler/mvortex_cons2_unstL2';
    %baseDir = '../cases/euler/mvortv2_unst';
    %baseDir = '../cases/euler/mvortv2_unstL2';
    %baseDir = '../cases/euler/mvortv2_unstL3'
    %baseDir = '../cases/euler/mvortv2_unstL4'
    %baseDir = '../cases/euler/mvortv2_unstL5'
    %baseDir = '../cases/euler/mvortex_unstL3'

    %baseDir = '../cases/euler/fastvort'
    %baseDir = '../cases/euler/fastvort_diag'
    %baseDir = '../cases/euler/mvortex_unstL4';
    baseDir = '../cases/euler_test/mvortex_unstL';
    caseDir = {''};

    %baseDir = '../cases/euler/'
    %caseDir = {'fastvort', 'fastvort_diag'};
    %caseDir = {'mvortex_unstL4', 'mvortex_diagL4'};

    % output flag
    outputOn = false;     
    %outputOn = true;

    % variables names
    pvNames = {'\rho', 'u', 'v', 'p'};

    % legend names
    legendNames = pvNames;

    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            FN1 = [baseDir,'/','vortexExtremaData','.dat'];    
        else
            FN1 = [baseDir,caseDir{i},'/','vortexExtremaData','.dat'];    
        end 
        [fh, fxMin0, fqPt0, fxMin, fqPt] = readVortexPeakData(FN1);
        h{i} = fh;          % 1/dof^(-1/2)
        xMin0{i} = fxMin0;  % minimum pressure location, initial     
        xMin{i} = fxMin;    % minimum pressure location, final 
        qPt0{i} = fqPt0;    % state variables at minimum pressure location, initial
        qPt{i} = fqPt;      % state variables at minimum pressure location, final   

        % evaluate error quantities
        for lev = 1:size(fqPt,2)
            for iEq = 1:size(fqPt,1)
                AmpErr(iEq) = abs(fqPt(iEq,lev)-fqPt0(iEq,lev)); %/fqPt0(iEq,lev);
                PhsErr(iEq) = abs(norm(fxMin(:,lev)));
            end
            AmpError{i}(:,lev) = AmpErr(:);
            PhsError{i}(:,lev) = PhsErr(:);
        end 

    end

    [nEqns,totalLevs] = size(qPt{1});
    % refinement levels
    refLevel = 5;
    % hard code
    legendNames = [legendNames(1),legendNames(4)];
     
    if outputOn
        % open a file to store the convergence study data
        for i = 1:fileNum
            [fid] = fopen([baseDir,'/','vortexPeakErrConvergence',caseDir{i},'.dat'],'w+');
            for iEq = 1:nEqns
                for lev = 1:totalLevs
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            'Level','h','Amp Error','Order','Phase Error','Order');
                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            '-----','-----','------------','-------','------------','-------');
                        fprintf(fid, '%5d %s %e %s %e %s %7s %s %e %s %7s %s\n', ...
                            lev,'&', h{i}(lev),'&',AmpError{i}(iEq,lev),'&','','&',PhsError{i}(iEq,lev),'&','','\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
                            lev,h{i}(lev),AmpError{i}(iEq,lev), ...
                            '       ', ...
                            PhsError{i}(iEq,lev), ...
                            '       ')
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %e %s %e %s %7.4f %s %e %s %7.4f %s\n',...
                                lev,'&',h{i}(lev),'&',AmpError{i}(iEq,lev),'&', ...
                                log(AmpError{i}(iEq,lev-1)/AmpError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
                                PhsError{i}(iEq,lev),'&', ...
                                log(PhsError{i}(iEq,lev-1)/PhsError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
            fclose(fid);    
        end
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','Amp Error','Order','Phase Error','Order')
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
                    fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h{i}(lev),AmpError{i}(iEq,lev),'',PhsError{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
                        lev,h{i}(lev),AmpError{i}(iEq,lev), ...
                        '       ', ...
                        PhsError{i}(iEq,lev), ...
                        '       ')
                else
                    fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                            lev,h{i}(lev),AmpError{i}(iEq,lev), ...
                            log(AmpError{i}(iEq,lev-1)/AmpError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            PhsError{i}(iEq,lev), ... 
                            log(PhsError{i}(iEq,lev-1)/PhsError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end
    
    hRef = h{1};
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    style2 = ['bo-.';'rs-.';'k*-.';'gd-.';'m<-.'];
    lWidth = 2;
    fSize = 18;
    % AmpError norm error
    for i = 1:fileNum
        figure(1)
        %for iEq = 1:nEqns
        for iEq = [1,4]
            if ( i == 1 )
                loglog(h{i}(:),AmpError{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            else
                loglog(h{i}(:),AmpError{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
            end

            hold on
            if ( iEq == nEqns && i == fileNum )
                %loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*AmpError{i}(1,1),'k--','linewidth',lWidth)    % 1st order
                %loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*AmpError{i}(1,1),'k-','linewidth',lWidth)    % 3rd order
                loglog(hRef(:),0.05*(hRef(:)).^3/(hRef(1)^3)*AmpError{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.05*(hRef(end)).^3/(hRef(1)^3)*AmpError{i}(1,1),'3','FontSize',fSize)
                legend(legendNames,'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('Peak Amplitude |\epsilon|_m_a_x');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if outputOn
                    fname1 = [baseDir,'/','peakAmpError.eps'];
                    print('-depsc2', '-r300', fname1);
                end
            end
        end
    end

    
    %% PhsError norm error
    %for i = 1:fileNum
    %    %figure(2)
    %    figure(i+fileNum)        
    %    for iEq = 1:nEqns
    %        loglog(h{i}(:),PhsError{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %        if ( iEq == nEqns )
    %            loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*PhsError{i}(1,1), 'k:')    % 1st order
    %            loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*PhsError{i}(1,1), 'k-')    % 3rd order
    %            legend([legendNames],'location','best')
    %            hx = xlabel('DOF^-^1^/^2'); hy = ylabel('Phase Error');
%   %              title(sprintf('File%d',i))
%   %              hold off
    %            set(gca,'FontSize',fSize)
    %            set(hx,'FontSize',fSize)
    %            set(hy,'FontSize',fSize)
    %            if outputOn
    %                fname2 = [baseDir,'/','euler',caseDir{i},'_errorPhsError.eps'];
    %                print('-depsc2', '-r300', fname2);
    %            end
    %        end
    %    end
    %end
    
end

function [h, xMin0, qPt0, xMin, qPt] = readVortexPeakData(file)
% SOLVORTEX(FILE) returns vortex peak data 

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    xMin0 = zeros(2,totalLevs);
    xMin = zeros(2,totalLevs);
    qPt0 = zeros(4,totalLevs);
    qPt = zeros(4,totalLevs);
        
    for lev = 1:totalLevs
        %h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        h(lev) = 1.0/sqrt(A(lev,2)-A(lev,1));   % cell size, 1/sqrt(dof)
        %h(lev) = 1.0/sqrt(3.0*A(lev,1));   % cell size, 1/sqrt(dof)
        xMin0(:,lev) = A(lev,4:5);
        qPt0(:,lev) = A(lev,6:9);
        xMin(:,lev) = A(lev,10:11);
        qPt(:,lev) = A(lev,12:15);
    end
            
end

