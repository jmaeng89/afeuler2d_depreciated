function[] = plotResidConvergence(baseDir,fileNames)
% PLOTRESIDCONVERGENCE(BASEDIR,FILENAMES) plots the residual 
%   convergence history. 
%
%   J. Brad Maeng
%   3/31/2016 - created
%
    %close all;
    
    %baseDir = '../cases/plesseuler/freestream';
    %baseDir = '../cases/linadv/sin_unst_constx_rec';
    baseDir = '../cases/linadv/sin_diag_bf';
    baseDir = '../cases/linadv/sin_unst_rec_smallnu';
    baseDir = '../cases/linadv/sin_unst_rec';
    baseDir = '../cases/linadv';
    baseDir = '../cases/linadv/sin_unst_rec_longt';
    baseDir = '../cases/linadv/sin_unst_bf_longt';
    
    fileNames = {'residdiag002','residdiag008','residdiag032','residdiag128','residdiag512'};
    fileNames = {'residunst002','residunst008','residunst032','residunst128','residunst512'};
    %fileNames = {'residunst002'};
    %fileNames = {'residunst008'};
    %fileNames = {'residunst032'};
    %fileNames = {'residunst128'};
    %fileNames = {'residunst512'};
    
    %fileNames = {'sin_unst_rec_longt/residunst128','sin_unst_rec/residunst128'};

    [~,fileNum] = size(fileNames);
        
    % legend names
    legendNames = {'\rho', 'u', 'v', 'p'};   
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [baseDir,'/',fileNames{i},'.dat'];     % complete file location
        [fIter, fT, fResid, fResidMax] = readResidSol(FN);
        iT{i} = fIter;
        t{i} = fT; 
        resid{i} = fResid;
        residMax{i} = fResidMax;  
    end
    
    [nEqns,totalLevs] = size(resid{1});
    nEqns = 1;

    style = ['b-';'r-';'k-';'g-';'m-'];
    %style = ['b:';'r:';'k:';'g:';'m:'];
    lWidth = 2;
    fSize = 16;
    
    % u residual convergence history
    figure(1)
    for i = 1:fileNum
        for iEq = 1:nEqns
            semilogy(t{i}(:),resid{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            hold on
        end 
    end
    hx = xlabel('Time'); 
    %hy = ylabel('|Resid|_1');
    hy = ylabel('|E|_1');
    hold off
    legend([legendNames],'location','best')
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    %fname1 = [dir,'/','residConvg',solType,'.eps'];
    %print('-depsc2', fname1);    

    %% maximum residual convergence history
    %figure(2)
    %for i = 1:fileNum
    %    for iEq = 1:nEqns
    %        semilogy(t{i}(:),residMax{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %    end 
    %end
    %hx = xlabel('Time'); 
    %hy = ylabel('|Resid|max');
    %hold off
    %legend([legendNames],'location','best')
    %set(gca,'FontSize',fSize)
    %set(hx,'FontSize',fSize)
    %set(hy,'FontSize',fSize)
    %fname1 = [dir,'/','bubbleConvg',solType,'.eps'];
    %print('-depsc2', fname1);    
        
end

function [fIter, fT, fResid, fResidMax] = readResidSol(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    nEqns = (cols-2)/2;
    
    % initialize output variables
    fIter = zeros(totalLevs,1); 
    fT = zeros(totalLevs,1);
    fResid = zeros(nEqns,totalLevs,1);
    fResidMax = zeros(nEqns,totalLevs,1);

    for lev = 1:totalLevs
        fIter(lev) = A(lev,1); 
        fT(lev) = A(lev,2);
        for iEq = 1:nEqns
            fResid(iEq,lev) = A(lev,2+iEq);
            fResidMax(iEq,lev) = A(lev,2+nEqns+iEq);
        end
    end

end
