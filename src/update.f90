!-------------------------------------------------------------------------------
!> @purpose 
!>  Routines that advance the solution in time
!>  Advection updates using Characteristic Tracing scheme
!>
!> @history
!>  3 October 2014 - Implementation of RD scheme (Maeng)
!>  26 January 2015 - Started looking into pressure-less Euler 2d (Maeng)
!>  5 May 2015 - Barotropic Euler 2d (Maeng)
!>  9 October 2015 - Isentropic Euler equations 2d (Maeng)
!>  
module update

    use solverVars, only: nEqns, dtIn, DP, FP, waveSpeed
    use meshUtil, only: nDim, nCells, nEdges, nNodes
    implicit none

    interface coefficients
        module procedure coefficients, coeffFromVal
    end interface coefficients

    ! Shared module variables 
    integer, parameter :: ADVECTION_EQ = 0, & !< flag for linear advection solution
                          PLESSEULER_EQ = 1,& !< flag for pressureless Euler solution
                          ISENTEULER_EQ = 2,& !< flag for isentropic Euler solution
                          EULER_EQ = 3        !< flag for Euler solution

    integer :: eqFlag   !< flag for equation type

    real(FP), allocatable, target :: cellAvg(:,:),    & !< conserved state variable at centroids
                                     cellAvgN(:,:),   & !< conserved variable at timestep "n"
                                     primAvg(:,:),    & !< primitive variable average at cell centroids
                                     primAvgN(:,:),   & !< primitive variable average at timestep "n"
                                     edgeDataN(:,:),  & !< edge solution at iteration n
                                     edgeData(:,:),   & !< edge solution at iteration n+1
                                     edgeDataH(:,:),  & !< edge solution at iteration n+1/2
                                     nodeDataN(:,:),  & !< node solution at iteration n
                                     nodeData(:,:),   & !< node solution at iteration n+1
                                     nodeDataH(:,:)     !< node solution at iteration n+1/2

    real(FP), pointer :: edgePtr(:,:),   & !< edge solution pointer
                         nodePtr(:,:)      !< node solution pointer
    
    real(FP), allocatable :: residMax(:),   & !< max flux residual 
                             residNorm(:),  & !< L1 norm of flux residual over all cells
                             primNorm(:),   & !< L1 norm of primitive variables
                             conservNorm(:)   !< L1 norm of conserved variables

    real(FP), allocatable :: edgeChOrg(:,:),    & !< edge char. org. 
                             nodeChOrg(:,:)       !< node char. org.

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D AF Euler solver   
!>  Characteristic origin tracing convection augmented by 
!>      nonlinear acoustics + nonlinear interaction 
!>
!> @history
!>  9 October 2015 - Initial creation (Maeng)
!>
subroutine eulerUpdate(nDim,nEqns,iter)

    use physics, only: ADVECTION_FLUX, PLESSEULER_FLUX, &
                       ISENTEULER_FLUX, EULER_FLUX, &
                       isentropicConst, gam 
    use meshUtil, only: nodeCoord, edgeCoord   

    implicit none

    !> Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           iter       !< iteration number

    
    ! Set all values equal in time
    nodeDataH = nodeDataN
    nodeData = nodeDataN
    edgeDataH = edgeDataN
    edgeData = edgeDataN

    ! initialize ChOrg with code and edge coordinates
    nodeChOrg(:,:) = nodeCoord(:,:)
    edgeChOrg(:,:) = edgeCoord(:,:)

    select case ( eqFlag )

        case ( ADVECTION_EQ ) 
            ! linear advection equations 
            ! nonconservative update
            call advectionUpdate( nDim,nEqns,iter,waveSpeed )

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,ADVECTION_FLUX,cellAvgN,cellAvg,waveSpeed )
            call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                                  nodeData,edgeData,cellAvg,primAvg )

            ! Detect any inconsistency in conservation and redistribute
            call reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges, &
                                    cellAvg,nodeData,edgeData )

            primAvg = cellAvg

        case ( PLESSEULER_EQ )
            ! pressureless euler 
            ! nonconservative update
            call advectionUpdate( nDim,nEqns,iter )
            nodeData(4,:) = 1.0_FP
            edgeData(4,:) = 1.0_FP
            nodeDataH(4,:) = 1.0_FP
            edgeDataH(4,:) = 1.0_FP

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,PLESSEULER_FLUX,cellAvgN,cellAvg )
            call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                                  nodeData,edgeData,cellAvg,primAvg )

            ! Detect any inconsistency in conservation and redistribute
            call reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges, &
                                    cellAvg,nodeData,edgeData )
        
        case ( ISENTEULER_EQ )
            ! isentropic euler equations 

            ! advection operator
            ! Interpolation of variables to the characteristic origin 
            call advectionUpdate( nDim,nEqns,iter )
            
            !! acoustics operator
            !! Take partially updated variables to apply acoustics operations
            call isentropicAcousticsUpdate( nDim,nEqns,iter )
            nodeData(4,:) = isentropicConst*nodeData(1,:)**gam
            edgeData(4,:) = isentropicConst*edgeData(1,:)**gam
            nodeDataH(4,:) = isentropicConst*nodeDataH(1,:)**gam
            edgeDataH(4,:) = isentropicConst*edgeDataH(1,:)**gam

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,ISENTEULER_FLUX,cellAvgN,cellAvg )
            call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                                  nodeData,edgeData,cellAvg,primAvg )

            ! Detect any inconsistency in conservation and redistribute
            call reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges, &
                                    cellAvg,nodeData,edgeData )

            ! set pressure according to isentropic relation
            edgeData(4,:) = isentropicConst*edgeData(1,:)**gam
            edgeDataH(4,:) = isentropicConst*edgeDataH(1,:)**gam
            nodeData(4,:) = isentropicConst*nodeData(1,:)**gam
            nodeDataH(4,:) = isentropicConst*nodeDataH(1,:)**gam

        case ( EULER_EQ )
            ! euler equations 

            ! advection operator
            ! Interpolation of variables to the characteristic origin 
            call advectionUpdate( nDim,nEqns,iter )

            ! acoustics operator
            ! Take partially updated variables to apply acoustics operations
            call acousticsUpdate( nDim,nEqns,iter )

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,EULER_FLUX,cellAvgN,cellAvg )
            call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                                  nodeData,edgeData,cellAvg,primAvg )

            ! Detect any inconsistency in conservation and redistribute
            call reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges, &
                                    cellAvg,nodeData,edgeData )
        
    end select

    ! Store for next time step
    edgeDataN = edgeData 
    nodeDataN = nodeData 
    cellAvgN = cellAvg 
    primAvgN = primAvg 

    ! check conservation
    call conservationCheck(nEqns,nCells,primAvg,cellAvg)

end subroutine eulerUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update values of reconData array
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  13 June 2012 - Initial creation
!>
subroutine updateReconData()

    use reconstruction, only: reconData

    implicit none

    ! Local variables
    integer :: iCell,       & !< cell index
               iEq

    ! set reconstruction coefficients
    do iCell = 1, nCells
        do iEq = 1, nEqns
            reconData(iEq,:,iCell) = coefficients(nDim,iEq,iCell)
        end do
    end do

end subroutine updateReconData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply acoustics operator. Import functions and subroutines written in C from
!>  srcAcoustics directory.
!>  
!>  Acoustics corrections require different attention from advection updates
!>  as the operator is elliptic in nature. 
!>  All nodal and edge data must be updated at the same time
!>  to reduce chance of corrupting reconstruction for other nodes.
!>
!> @history
!>  7 May 2015 - Initial creation (Maeng)
!>  4 May 2016 - Modularized acoustics correction (Maeng)
!>
subroutine acousticsUpdate(nDim,nEqns,iter)

    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord
    use boundaryConditions, only: applyAcBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge              !< global edge index

    real(FP) :: cellRecon(nEqns,nDim*(nDim+1)), & !< cell quadratic reconstruction 
                nodeAcSignal(nEqns,3),          & !< signals on node
                edgeAcSignal(nEqns,3),          & !< signals on edge 
                cellNodeCoord(nDim,3)             !< cell vertex

    ! TODO: think about the size
    real(FP) :: reconDataH(nEqns,nDim*(nDim+1),nCells), & !< half time recon. coeffs
                reconDataF(nEqns,nDim*(nDim+1),nCells)    !< full time recon. coeffs

    ! set cell reconstruction coefficients
    do iCell = 1, nCells
        
        ! half time step
        reconDataH(:,1,iCell) = nodeDataH(:,cellNodes(1,iCell))
        reconDataH(:,3,iCell) = nodeDataH(:,cellNodes(2,iCell))
        reconDataH(:,5,iCell) = nodeDataH(:,cellNodes(3,iCell))
        reconDataH(:,2,iCell) = edgeDataH(:,cellFaces(1,iCell))
        reconDataH(:,4,iCell) = edgeDataH(:,cellFaces(2,iCell))
        reconDataH(:,6,iCell) = edgeDataH(:,cellFaces(3,iCell))

        ! full time step
        reconDataF(:,1,iCell) = nodeData(:,cellNodes(1,iCell))
        reconDataF(:,3,iCell) = nodeData(:,cellNodes(2,iCell))
        reconDataF(:,5,iCell) = nodeData(:,cellNodes(3,iCell))
        reconDataF(:,2,iCell) = edgeData(:,cellFaces(1,iCell))
        reconDataF(:,4,iCell) = edgeData(:,cellFaces(2,iCell))
        reconDataF(:,6,iCell) = edgeData(:,cellFaces(3,iCell))

    end do

    ! acoustics solver with cell loop based update 
    do iCell = 1, nCells
        
        ! set cell node coordinates 
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            cellNodeCoord(:,iNode) = nodeCoord(:,gNode) 
        end do

        ! half time step
        cellRecon = reconDataH(:,:,iCell)
        call acSignalUpdate(nEqns,nDim,iCell,0.5_FP*dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            nodePtr => nodeDataH
            call applyAcBC(nEqns,gNode,nodeAcSignal(:,iNode),nodePtr,0.5_FP*dtIn)
        end do
        ! apply signals
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            edgePtr => edgeDataH
            call applyAcBC(nEqns,-gEdge,edgeAcSignal(:,iEdge),edgePtr,0.5_FP*dtIn)
        end do

        ! full time step
        cellRecon = reconDataF(:,:,iCell)
        call acSignalUpdate(nEqns,nDim,iCell,dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeData(:,gNode) = nodeData(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            nodePtr => nodeData
            call applyAcBC(nEqns,gNode,nodeAcSignal(:,iNode),nodePtr,dtIn)
        end do
        ! apply signals
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeData(:,gEdge) = edgeData(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            edgePtr => edgeData
            call applyAcBC(nEqns,-gEdge,edgeAcSignal(:,iEdge),edgePtr,dtIn)
        end do

    end do

    ! nullify pointers
    nullify( edgePtr, nodePtr )
    
end subroutine acousticsUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply acoustics operator. Import functions and subroutines written in C from
!>  srcAcoustics directory.
!>  TODO: Major candidate for improved efficiency
!>
!> @history
!>  7 May 2015 - Initial creation (Maeng)
!>
subroutine isentropicAcousticsUpdate(nDim,nEqns,iter)

    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord
    use boundaryConditions, only: applyAcBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge              !< global edge index

    real(FP) :: cellRecon(nEqns,nDim*(nDim+1)), & !< cell quadratic reconstruction 
                nodeAcSignal(nEqns,3),          & !< signals on node
                edgeAcSignal(nEqns,3),          & !< signals on edge 
                cellNodeCoord(nDim,3)             !< cell vertex

    real(FP) :: reconDataH(nEqns,nDim*(nDim+1),nCells), & !< half time recon. coeffs
                reconDataF(nEqns,nDim*(nDim+1),nCells)    !< full time recon. coeffs

    ! set cell reconstruction coefficients
    do iCell = 1, nCells
        
        ! half time step
        reconDataH(:,1,iCell) = nodeDataH(:,cellNodes(1,iCell))
        reconDataH(:,3,iCell) = nodeDataH(:,cellNodes(2,iCell))
        reconDataH(:,5,iCell) = nodeDataH(:,cellNodes(3,iCell))
        reconDataH(:,2,iCell) = edgeDataH(:,cellFaces(1,iCell))
        reconDataH(:,4,iCell) = edgeDataH(:,cellFaces(2,iCell))
        reconDataH(:,6,iCell) = edgeDataH(:,cellFaces(3,iCell))

        ! full time step
        reconDataF(:,1,iCell) = nodeData(:,cellNodes(1,iCell))
        reconDataF(:,3,iCell) = nodeData(:,cellNodes(2,iCell))
        reconDataF(:,5,iCell) = nodeData(:,cellNodes(3,iCell))
        reconDataF(:,2,iCell) = edgeData(:,cellFaces(1,iCell))
        reconDataF(:,4,iCell) = edgeData(:,cellFaces(2,iCell))
        reconDataF(:,6,iCell) = edgeData(:,cellFaces(3,iCell))

    end do

    ! acoustics solver with cell loop based update 
    do iCell = 1, nCells
        
        ! set cell node coordinates 
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            cellNodeCoord(:,iNode) = nodeCoord(:,gNode) 
        end do

        ! half time step
        cellRecon = reconDataH(:,:,iCell)
        call isentropicAcSignalUpdate(nEqns,nDim,iCell,0.5_FP*dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            nodePtr => nodeDataH
            call applyAcBC(nEqns,gNode,nodeAcSignal(:,iNode),nodePtr,0.5_FP*dtIn)
        end do
        ! apply signals
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            edgePtr => edgeDataH
            call applyAcBC(nEqns,-gEdge,edgeAcSignal(:,iEdge),edgePtr,0.5_FP*dtIn)
        end do

        ! full time step
        cellRecon = reconDataF(:,:,iCell)
        call isentropicAcSignalUpdate(nEqns,nDim,iCell,dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeData(:,gNode) = nodeData(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            nodePtr => nodeData
            call applyAcBC(nEqns,gNode,nodeAcSignal(:,iNode),nodePtr,dtIn)
        end do
        ! apply signals
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeData(:,gEdge) = edgeData(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            edgePtr => edgeData
            call applyAcBC(nEqns,-gEdge,edgeAcSignal(:,iEdge),edgePtr,dtIn)
        end do

    end do

    ! nullify pointers
    nullify( edgePtr, nodePtr )

end subroutine isentropicAcousticsUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply advection operator using Characteristic Origin Tracing scheme.
!>  Advection speed is determined by node/edge velocity
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  4 September 2013 - Linear advection constant wave speed is added (Maeng)
!>  13 January 2015 - 2D Burgers' equations (Maeng) 
!>  1 February 2015 - 2D Pressureless Euler equations (Maeng) 
!>  5 May 2015 - 2D Euler Equations (Maeng)
!>
subroutine advectionUpdate(nDim,nEqns,iter,lam)

    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord, edgeCoord
    use boundaryConditions, only: applyBC, nodeBC, edgeBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               neglectedEdgesH, & !< number of edges without an update
               neglectedNodesH, & !< number of nodes without an update
               neglectedEdges,  & !< number of edges without an update
               neglectedNodes     !< number of nodes without an update

    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
                       nodeUpdated(nNodes),  & !< flag that a node has been updated
                       edgeUpdated(nEdges)     !< flag that an edge has been updated

    logical, pointer :: updatedPtr(:)         !< node/edge updated flag pointer

    real(FP) :: dt,             & !< time step
                vel(nDim),      & !< local advection velocity 
                signal(nEqns)     !< cell's contribution to node/edge value

    nodeUpdatedH = .false.
    edgeUpdatedH = .false.
    nodeUpdated = .false.
    edgeUpdated = .false.

    ! default
    dt = dtIn

    ! Set reconstruction coefficients for primitive variables
    call updateReconData()

    !write(*,*) 'nonconservative stage'
    do iCell = 1,nCells

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)

            ! euler equations advection velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = nodeDataN(2:3,gNode) 
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            0.5_FP*dt,vel,nodeDataN(:,gNode), &
                                 nodeUpdatedH(gNode) )
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal
            ! apply boundary condition
            nodePtr => nodeDataH
            updatedPtr => nodeUpdatedH
            call applyBC(nEqns,gNode,signal,nodePtr,0.5_FP*dt, &
                                updatedPtr)

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            dt,vel,nodeDataN(:,gNode), &
                                 nodeUpdated(gNode) )
            nodeData(:,gNode) = nodeData(:,gNode) + signal
            ! apply boundary condition
            nodePtr => nodeData
            updatedPtr => nodeUpdated
            call applyBC(nEqns,gNode,signal,nodePtr,dt, &
                                updatedPtr)

        end do

        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)

            ! euler equations advection velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = edgeDataN(2:3,gEdge) 
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            0.5_FP*dt,vel,edgeDataN(:,gEdge), &
                                 edgeUpdatedH(gEdge) )
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal
            ! apply boundary condition
            edgePtr => edgeDataH
            updatedPtr => edgeUpdatedH
            call applyBC(nEqns,-gEdge,signal,edgePtr,0.5_FP*dt, &
                                updatedPtr)

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            dt,vel,edgeDataN(:,gEdge),  &
                                 edgeUpdated(gEdge) )
            edgeData(:,gEdge) = edgeData(:,gEdge) + signal
            ! apply boundary condition
            edgePtr => edgeData
            updatedPtr => edgeUpdated
            call applyBC(nEqns,-gEdge,signal,edgePtr,dt, &
                                updatedPtr)
        end do

    end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Specific inflow
    !!
    !! 18 February 2016
    !!
    !! use this for solid body rotation where inflow is needed 
    !!
    !do gNode = 1, nNodes
    !    if ( .not. nodeUpdatedH(gNode) ) then
    !       nodeDataH(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeDataH(:,gNode),nodeCoord(:,gNode),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. nodeUpdated(gNode) ) then
    !       nodeData(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeData(:,gNode),nodeCoord(:,gNode),tSim+dtIn)
    !    end if
    !end do
    !do gEdge = 1, nEdges
    !    if ( .not. edgeUpdatedH(gEdge) ) then
    !       edgeDataH(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeDataH(:,gEdge),edgeCoord(:,gEdge),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. edgeUpdated(gEdge) ) then
    !       edgeData(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeData(:,gEdge),edgeCoord(:,gEdge),tSim+dtIn)
    !    end if
    !end do
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! nullify pointers
    nullify( edgePtr, nodePtr, updatedPtr )
                
    ! print out neglected node/edge
    neglectedEdgesH = nEdges - count( edgeUpdatedH )
    neglectedNodesH = nNodes - count( nodeUpdatedH )
    neglectedEdges = nEdges - count( edgeUpdated )
    neglectedNodes = nNodes - count( nodeUpdated )
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        !write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        !write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        !do iNode = 1, nNodes
        !    if ( .not. nodeUpdated(iNode) ) then
        !        write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
        !                                    nodeBC(iNode)
        !        !if ( nodebc(iNode) == 0 ) then
        !        !    read(*,*)
        !        !end if
        !    end if
        !end do
        !write(*,*)
        !write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        !write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        !do iEdge = 1, nEdges
        !    if ( .not. edgeUpdated(iEdge) ) then
        !        write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
        !                                    edgeBC(iEdge)
        !        !if ( edgebc(iedge) == 0 ) then
        !        !    read(*,*)
        !        !end if
        !    end if
        !end do
        !write(*,*)
    end if
    
    ! Check for edges/nodes without an update
#ifdef VERBOSE
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine advectionUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the signal sent from a cell at a specified time
!>
!> @history
!>  26 June 2012 - Initial creation (Eymann)
!>  14 May 2013 - Use local advection speed (Eymann)
!>  1 February 2015 - Pressure-less Euler system. Velocity divergence
!>      correction for density (Maeng)
!>  5 October 2015 - Incorporated a mandatory update flag, and onEdge flag
!>      no longer used. 
!>  9 October 2015 - Euler equations in 2d (Maeng)
!>  11 November 2015 - Remove velocity divergence effect from pressureless Euler
!>      for the implementation of Euler equations (Maeng)
!>
function signalValue(nEqns,nDim,iCell,iNodeEdge,dt,lam,qN,updated,iter,cIn)
   
    use solverVars, only: eps
    use reconstruction, only: reconState, reconValue, gradVal, &
                              reconValueBFdiff  
    use meshUtil, only: cellFaces, faceCells, cellNodes, ref2cart

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations in system
                           nDim,    & !< number of dimensions
                           iCell,   & !< cell index
                           iNodeEdge  !< local node index (negative for edge)

    real(FP), intent(in) :: dt,                 & !< timestep for signal
                            lam(nDim),          & !< local advection speed
                            qN(nEqns)             !< state at time n

    logical, intent(inout) :: updated !< flag that update should be applied

    integer, intent(in), optional :: iter

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    ! Function variables
    real(FP) :: signalValue(nEqns)

    ! Local variables
    integer :: iEq,     & !< equation index
               onEdge     !< edge containing char. origin

    integer :: gFace,   & !< global face index
               gNode,   &
               pEdge,   & !< periodic edge
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: xi0(nDim),          & !< char origin in ref space
                qXi0(nEqns),        & !< state at char. origin
                cVel(nDim),         & !< corrected wave speed
                gradU(nDim,nDim),   & !< velocity gradient
                tol,                & !< tolerance
                divSignal,          & !< divergence effect on density signal 
                divU                  !< velocity divergence

    real(FP) :: xiL(nDim),  &
                xiR(nDim),  &  
                xCart(nDim)    

    logical :: inCell = .false.     !< flag that signal originates in cell

    tol = eps
    signalValue = 0.0_FP
    qXi0 = 0.0_FP

    ! corrected advection velocity
    if ( eqFlag == ADVECTION_EQ ) then
        ! linear advection
        cVel = lam

    else if ( eqFlag == EULER_EQ .or. eqFlag == ISENTEULER_EQ ) then
        ! nonlinear advection for Euler system
        cVel = corrWaveSpeedEuler(nDim,iCell,iNodeEdge,dt,lam)

    else
        ! nonlinear advection - velocity correction
        cVel = corrWaveSpeed(nDim,iCell,iNodeEdge,dt,lam)
    
    end if
    call charOrigin(nDim,iCell,iNodeEdge,dt,cVel,xi0,inCell,onEdge)

    lCell = iCell
    rCell = iCell
    xiL = xi0
    xiR = xi0
    ! only modify values if signal for wave originates in cell
    if ( ( inCell ) .and. ( .not. updated ) ) then  
        ! find characteristic origins in cross wind directions
        !call charOriginCrossWind(nDim,iCell,iNodeEdge,dt,cVel,xi0,xiL,xiR,lCell,rCell)

        do iEq = 1,nEqns
            ! original characteristic origin tracing
            qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:))

            ! bubble function cross wind diffusion applied
            !qXi0(iEq) = reconValueBFdiff(nDim,iEq,iCell,lCell,rCell,xi0,xiL,xiR)

            if ( eqFlag == PLESSEULER_EQ ) then
                if ( iEq == 1 ) then 
                    ! density correction by divergence of vel.
                    divSignal = 0.0_FP
                    if ( nDim == 2 ) then
                        ! velocity gradients
                        gradU(1,:) = gradVal(nDim,2,iCell,xi0) ! grad u
                        gradU(2,:) = gradVal(nDim,3,iCell,xi0) ! grad v
                        ! divergence of velocity
                        divU = velocityDiv( nDim, iCell, xi0, dt )
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU) + &
                                    dt*dt*(gradU(1,1)*gradU(2,2) - gradU(1,2)*gradU(2,1)))
                        !divSignal = qXi0(iEq)*(1.0_FP-dt*(divU) + &
                        !           dt*dt*(gradU(1,1)*gradU(2,2)-gradU(1,2)*gradU(2,1)))
                        ! signal value
                        signalValue(iEq) = divSignal - qN(iEq)
                    else    ! nDim == 1
                        divU = velocityDiv( nDim, iCell, xi0, dt )
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU))
                        signalValue(iEq) = divSignal - qN(iEq)
                    end if
                else    
                    ! velocities
                    signalValue(iEq) = qXi0(iEq) - qN(iEq) 
                end if
            else
                ! advection operator
                signalValue(iEq) = qXi0(iEq) - qN(iEq)
            end if

            if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
        end do

        ! characteristic origin
        xCart = ref2cart(nDim,iCell,xi0)
        if ( iNodeEdge > 0 ) then
            nodeChOrg(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        else
            edgeChOrg(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        end if

        !xCart = ref2cart(nDim,lCell,xiL)
        !if ( iNodeEdge > 0 ) then
        !    nodeChOrgL(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        !else
        !    edgeChOrgL(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        !end if
        !xCart = ref2cart(nDim,rCell,xiR)
        !if ( iNodeEdge > 0 ) then
        !    nodeChOrgR(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        !else
        !    edgeChOrgR(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        !end if

        ! mark node/edge as updated
        updated = .true.
    end if
    
end function signalValue
!-------------------------------------------------------------------------------
!> @purpose
!>  Calculate the origin of characteristic intersecting a space-time face
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  8 May 2012 - Initial creation\n
!>  23 May 2012 - Add Burgers equation\n
!>  3 October 2012 - Add flag for origin lying on interface
!>  4 November 2014 - Add tolerance on origins (Maeng)
!>
subroutine charOrigin(nDim,iCell,iNodeEdge,dt,lam,xi0,inCell,onEdge)

    use solverVars, only: eps
    use meshUtil, only: cellInvJac, xiNode, xiEdge
    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,             & !< time at interface
                            lam(nDim)         !< average velocity in cell

    real(FP), intent(out) :: xi0(nDim) !< characterisitic origin

    logical, intent(out) :: inCell !< flag that cell contains characteristic

    integer, intent(out) :: onEdge !< local index of edge for char. origin

    !< Local variables
    real(FP) :: xi(nDim),     & !< reference coordinate of interface
                xiTemp(nDim), &
                tol

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    ! find originTemp
    xiTemp(:) = -1.0_FP*dt*matmul(cellInvJac(:,:,iCell),lam(:))  
    xi0(:) = xi(:) + xiTemp(:) 
    
    ! determine if char. origin is inside cell
    inCell = .false.
    onEdge = 0
    select case ( nDim )

        case (1)
            ! zero tolerance check
            if ( abs(xi0(1)) <= tol ) xi0(1) = 0.0_FP

            if ( (( xi0(1) > 0.0_FP ) .and. ( xi0(1) < 1.0_FP )) ) then
                inCell = .true.
            elseif ( xi0(1) == 1.0_FP ) then
                inCell = .true.
                onEdge = 2
            elseif ( xi0(1) == 0.0_FP ) then
                inCell = .true.
                onEdge = 1
            end if

        case (2)
            !! zero tolerance check
            !if ( abs(xi0(1)) <= tol ) xi0(1) = 0.0_FP
            !if ( abs(xi0(2)) <= tol ) xi0(2) = 0.0_FP

            ! Cartesian coordinate
            if ( (abs(xi0(1)) <= tol) .and. &
                 ((xi0(2) >= 0.0_FP) .and. (xi0(2) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 2
            elseif ( (abs(xi0(2)) <= tol) .and. &
                     ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 3
            elseif ( (abs(sum(xi0(:))-1.0_FP) <= tol ) .and. &
                     ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 1
            elseif ( ((xi0(1) > 0.0_FP) .and. (xi0(1) < 1.0_FP)) .and. &
                 ((xi0(2) > 0.0_FP ) .and. (sum(xi0(:)) < 1.0_FP)) ) then
                inCell = .true.
                onEdge = 0
            end if

    end select

end subroutine charOrigin
!-------------------------------------------------------------------------------
!> @purpose
!>  Calculate the origin of characteristic in cross wind directions
!>
!> @history
!>  8 May 2012 - Initial creation\n
!>
subroutine charOriginCrossWind(nDim,iCell,iNodeEdge,dt,lam,xi0,xiL,xiR,lCell,rCell)

    use solverVars, only: eps
    use meshUtil, only: xiNode, xiEdge, xMin, xMax, &
                        nodeCoord, cellNodes, &
                        cellJac, cellInvJac, cellFaces, &
                        faceCells, ref2cart, cart2ref
    use mathUtil, only: pointInTri
    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iNodeEdge,   &
                           iCell          !< cell index

    real(FP), intent(in) :: dt,             & !< time at interface
                            lam(nDim),      & !< average velocity in cell
                            xi0(nDim)         !< characterisitic origin

    real(FP), intent(inout) :: xiL(nDim),     & !< left 
                               xiR(nDim)        !< right 

    integer, intent(inout) :: lCell,          & !< left
                              rCell             !< right

    !< Local variables
    integer :: iDir, &
               onEdge

    real(FP) :: xiTemp(nDim),  &
                xCart(nDim),   &
                s1L,           & !< barycentric coordinate for 2D
                s2L,           & !< barycentric coordinate
                s3L,           & !< barycentric coordinate
                s1R,           & !< barycentric coordinate for 2D
                s2R,           & !< barycentric coordinate
                s3R,           & !< barycentric coordinate
                tol

    real(FP) :: alpha,      & !< constant 
                sVec(nDim), & !< streamline vector
                nVec(nDim)    !< cross wind vector

    ! streamline and cross-stream direction vectors
    alpha = 0.7_FP
    sVec(:) = -lam(:)/norm2(lam(:))
    nVec(1) = -sVec(2)
    nVec(2) = sVec(1)
    nVec(:) = matmul(cellInvJac(:,:,iCell),nVec) ! in reference space
    ! find characteristic origin vector in reference coord
    xiTemp(:) = dt*matmul(cellInvJac(:,:,iCell),lam(:)) 
    xiL(:) = xi0(:) + alpha*norm2(xiTemp(:))*(nVec(:)/norm2(nVec))
    xiR(:) = xi0(:) - alpha*norm2(xiTemp(:))*(nVec(:)/norm2(nVec))

    ! barycentric coordinate
    s1L = 1.0_FP - (xiL(1) + xiL(2))
    s2L = xiL(1)
    s3L = xiL(2)
    s1R = 1.0_FP - (xiR(1) + xiR(2))
    s2R = xiR(1)
    s3R = xiR(2)

    !if (iCell == 4) then
    !    write(*,*)
    !    write(*,*) 'total before '
    !    write(*,*) iNodeEdge
    !    write(*,*) nVec, xiTemp
    !    write(*,*) lCell, xiL, ref2cart(nDim,iCell,xiL)
    !    write(*,*) iCell, xi0, ref2cart(nDim,iCell,xi0)
    !    write(*,*) rCell, xiR, ref2cart(nDim,iCell,xiR)
    !end if

    ! check if either xiL or xiR is outside of triangle
    if ( (s1L >= 0.0 .and. s1L <= 1.0 ) .and. &
         (s2L >= 0.0 .and. s2L <= 1.0 ) .and. &
         (s3L >= 0.0 .and. s3L <= 1.0 ) ) then
        ! xiL inside
        xiL = xiL
    else
        !! xiL outside
        !!if (iCell == 110) then
        !!    write(*,*)
        !!    write(*,*) 'left '
        !!    write(*,*) 'cell ', iCell, xi0
        !!    write(*,*) 'cell ', xiL, onEdge
        !!end if
        xCart = ref2cart(nDim,iCell,xiL)
        ! periodic coordinate assignment
        if ( xCart(1) < xMin(1) ) then
            xCart(1) = xMax(1) - abs(xCart(1)-xMin(1)) 
        else if ( xCart(1) > xMax(1) ) then
            xCart(1) = xMin(1) + abs(xCart(1)-xMax(1))
        end if
        if ( xCart(2) < xMin(2) ) then
            xCart(2) = xMax(2) - abs(xCart(2)-xMin(2)) 
        else if ( xCart(2) > xMax(2) ) then
            xCart(2) = xMin(2) + abs(xCart(2)-xMax(2))
        end if
        if ( s1L >= 0.0 ) then
            if ( s2L >= 0.0 ) then
                onEdge = 3
            else
                onEdge = 2
            end if
        else
            onEdge = 1 
        end if
        lCell = abs(faceCells(2,cellFaces(onEdge,iCell)))
        if ( lCell == iCell ) lCell = abs(faceCells(1,cellFaces(onEdge,iCell)))
        xiL = cart2ref(nDim,lCell,xCart)
        !if (iCell == 110) then
        !    write(*,*) 'cell ', lCell, xcart, xiL
        !    write(*,*)
        !end if
    end if

    if ( (s1R >= 0.0 .and. s1R <= 1.0 ) .and. &
         (s2R >= 0.0 .and. s2R <= 1.0 ) .and. &
         (s3R >= 0.0 .and. s3R <= 1.0 ) ) then
        ! xiR inside
        xiR = xiR
    else 
        !! xi0R outside
        !!if (iCell == 110) then
        !!    write(*,*)
        !!    write(*,*) 'right '
        !!    write(*,*) 'cell ', iCell, xi0
        !!    write(*,*) 'cell ', xiR, onEdge
        !!end if
        xCart = ref2cart(nDim,iCell,xiR)
        ! periodic coordinate assignment
        if ( xCart(1) < xMin(1) ) then
            xCart(1) = xMax(1) - abs(xCart(1)-xMin(1)) 
        else if ( xCart(1) > xMax(1) ) then
            xCart(1) = xMin(1) + abs(xCart(1)-xMax(1))
        end if
        if ( xCart(2) < xMin(2) ) then
            xCart(2) = xMax(2) - abs(xCart(2)-xMin(2)) 
        else if ( xCart(2) > xMax(2) ) then
            xCart(2) = xMin(2) + abs(xCart(2)-xMax(2))
        end if

        if ( s1R >= 0.0 ) then
            if ( s2R >= 0.0 ) then
                onEdge = 3
            else
                onEdge = 2
            end if
        else
            onEdge = 1 
        end if
        rCell = abs(faceCells(2,cellFaces(onEdge,iCell)))
        if ( rCell == iCell ) rCell = abs(faceCells(1,cellFaces(onEdge,iCell)))
        xiR = cart2ref(nDim,rCell,xCart)
        !if (iCell == 110) then
        !    write(*,*) 'cell ', rCell, xcart, xiR
        !    write(*,*)
        !end if
    end if

    !if (iCell == 4) then
    !    write(*,*)
    !    write(*,*) 'total after '
    !    write(*,*) iNodeEdge
    !    write(*,*) nVec, xiTemp
    !    write(*,*) lCell, xiL, ref2cart(nDim,lCell,xiL)
    !    write(*,*) iCell, xi0, ref2cart(nDim,iCell,xi0)
    !    write(*,*) rCell, xiR, ref2cart(nDim,rCell,xiR)
    !end if

end subroutine charOriginCrossWind
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 January 2015 - Initial creation
!>  24 February 2015 - Included second order correction term
!>  16 April 2015 - Exact correction terms added for comparison
!>
function corrWaveSpeed(nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: cellInvJac, xiNode, xiEdge, ref2cart
    use reconstruction, only: gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional :: cIn(nEqns, &
        (nDim+2)*(nDim+3)/(2*(3-nDim))) !< function coefficients

    !< Function variable
    real(FP) :: corrWaveSpeed(nDim)  

    !< Local variables
    integer :: iDir       !< dimension index

    real(FP) :: tol,                    & !< tolerance
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                corrMat(nDim,nDim),     & !< correction matrix
                corrMattemp(nDim,nDim), & !< correction matrix, temp
                detMat                    !< determinant of matrix

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    select case ( nDim )

        case ( 1 )

            ! evaluate gradient 
            gradU(1,:) = gradVal( nDim, 2, iCell, xi )

            ! Exact correction
            !corrMat(1,:) = 1.0_FP + 1.0_FP*dt*gradU(1,:) 
            !corrWaveSpeed(:) = lam(:)/corrMat(1,:)  

            ! Approximation of geometric series 
            corrWaveSpeed(:) = lam(:) - lam(:)*gradU(1,:)*dt  

        case ( 2 ) 

            ! evaluate gradient 
            gradU(1,:) = gradVal( nDim, 2, iCell, xi ) 
            gradU(2,:) = gradVal( nDim, 3, iCell, xi ) 

            !! Exact correction by inverting matrix
            !corrMat(:,:) = 1.0_FP*dt*gradU(:,:)
            !! add Identity matrix
            !corrMat(1,1) = 1.0_FP + corrMat(1,1) 
            !corrMat(2,2) = 1.0_FP + corrMat(2,2) 
            !! assign the matrix to temporary variable
            !corrMatTemp(:,:) = corrMat(:,:)
            !! invert the matrix
            !corrMat(:,:) = 0.0_FP
            !detMat = (corrMatTemp(1,1)*corrMatTemp(2,2) - &
            !          corrMatTemp(1,2)*corrMatTemp(2,1))
            !if ( abs(detMat) <= tol ) then
            !    ! in case corrMat is sigular
            !    !corrWaveSpeed(:) = lam(:)
            !    corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1) + &
            !        lam(2)*gradU(1,2))*dt  
            !    corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1) + &
            !        lam(2)*gradU(2,2))*dt  
            !else
            !    ! invert corrMat
            !    corrMat(1,1) = (1.0_FP/detMat)*corrMatTemp(2,2) 
            !    corrMat(1,2) = -(1.0_FP/detMat)*corrMatTemp(1,2) 
            !    corrMat(2,1) = -(1.0_FP/detMat)*corrMatTemp(2,1) 
            !    corrMat(2,2) = (1.0_FP/detMat)*corrMatTemp(1,1) 
            !    ! corrected waveSpeed
            !    corrWaveSpeed(:) = matmul(corrMat(:,:),lam(:))  
            !end if

            ! Approximation of matrix multiplication
            corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt  
            corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt  

    end select

end function corrWaveSpeed
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity with pressure 
!>  gradient correction
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 August 2016 - Initial creation
!>
function corrWaveSpeedEuler(nDim,iCell,iNodeEdge,dt,lam)

    use solverVars, only: eps, tSim
    use meshUtil, only: xiNode, xiEdge, ref2cart
    use reconstruction, only: reconValue, gradVal
    use physics, only: gam, isentropicConst 

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    !< Function variable
    real(FP) :: corrWaveSpeedEuler(nDim)  

    !< Local variables
    real(FP) :: tol,                    & !< tolerance
                rho,                    & !< density
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                gradP(nDim)               !< gradient of pressure

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    gradP(:) = 0.0_FP 
    rho = 0.0_FP

    gradU(1,:) = gradVal( nDim, 2, iCell, xi ) 
    gradU(2,:) = gradVal( nDim, 3, iCell, xi ) 

    rho = reconValue(nDim,1,iCell,xi) 

    select case ( eqFlag )
        
        case ( EULER_EQ )
            gradP(:) = gradVal( nDim, 4, iCell, xi ) 

        case ( ISENTEULER_EQ )
            gradP(:) = gradVal( nDim, 1, iCell, xi ) 

            ! pressure gradient is replaced by density gradient in isentropic Euler
            gradP(1) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(1)
            gradP(2) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(2)

    end select

    ! Approximation of matrix multiplication
    corrWaveSpeedEuler(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt - &
        (gradP(1)/rho)*dt
    corrWaveSpeedEuler(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt - &
        (gradP(2)/rho)*dt

end function corrWaveSpeedEuler
!-------------------------------------------------------------------------------
!> @purpose 
!>  Velocity divergence accounting for the first order term
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  24 September 2015 - Initial creation (Maeng)
!>
function velocityDiv( nDim, iCell, xi, dt )

    use reconstruction, only: reconValue, gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell          !< cell index

    real(FP), intent(in) :: xi(nDim),   & !< evaluation location 
                            dt            !< time

    !< Function variable
    real(FP) :: velocityDiv

    !< Local variables
    real(FP) :: gradU(nDim,nDim) !< gradient of velocity vector

    velocityDiv = 0.0_FP
    select case ( nDim ) 
        
        case (1)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! ux

            ! velocity divergence
            velocityDiv = gradU(1,1) 

        case (2)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! grad u
            gradU(2,:) = gradVal(nDim,3,iCell,xi) ! grad v 

            ! without second order gradients
            ! divergence of velocity
            ! velocity divergence evaluated at char. org contains second order derivatives
            ! no need to evaluate it again
            velocityDiv = (gradU(1,1)+gradU(2,2))

    end select

end function velocityDiv
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use fluxes to update cell values
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  10 April 2015 - Updated for pressureless Euler (Maeng) 
!>
subroutine updateCells( nDim,nEqns,nCells,fluxType,qCellN,qCell,lam )
    
    use solverVars, only: tSim, iRight, iLeft
    use meshUtil, only: faceCells, faceNormal, faceArea, cellVolume, &
                        cellFaces, faceNodes, ref2cart, totalVolume, &
                        faceNormalArea, nodeCoord
    
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,     & !< problem dimension
                           nEqns,    & !< number of equations
                           nCells,   & !< number of cells
                           fluxType    !< flux used for update

    real(FP), intent(in) :: qCellN(nEqns,nCells)       !< state at time n

    real(FP), intent(inout) :: qCell(nEqns,nCells)     !< state at time n+1

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iEq,     & !< equation index
               iEdge,   & !< edge index
               iNode,   &
               gEdge,   &
               lNode,   & !< node index
               rNode,   & !< node index
               iCell,   & !< cell index
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: faceFlux(nDim,nEqns),   & !< flux at edge
                resid(nEqns,nCells),    & !< cell residual
                residNormSum(nEqns),    & !< residual norm storage
                dR(nEqns)                 !< change in residual value

    real(FP) :: fNorm(nDim),  &
                fArea
                
    !write(*,*) 'conservation stage'
    qCell(:,:) = qCellN(:,:)
    resid(:,:) = 0.0_FP
    do iEdge = 1, nEdges
        ! left and right cell index
        lCell = faceCells(iLeft,iEdge)
        rCell = faceCells(iRight,iEdge)

        ! evaluate average flux over a face
        faceFlux(:,:) = averageFlux( nDim,nEqns,fluxType,iEdge,lam )

        ! numerical face flux
        !fluxAvg(:,:,iEdge) = faceFlux(:,:)
        do iEq = 1,nEqns
            dR(iEq) = dot_product(faceFlux(:,iEq),faceNormal(:,iEdge)) * &
                      faceArea(iEdge)
        end do

        ! TODO: cell update counter?
        resid(:,lCell) = resid(:,lCell) - dR(:)/cellVolume(lCell)
        qCell(:,lCell) = qCell(:,lCell) - dtIn*dR(:)/cellVolume(lCell)
        if ( rCell > 0 ) then
            resid(:,rCell) = resid(:,rCell) + dR(:)/cellVolume(rCell)
            qCell(:,rCell) = qCell(:,rCell) + dtIn*dR(:)/cellVolume(rCell)
        end if
        
    end do

    ! Residual calculation 
    residNormSum(:) = 0.0_FP
    residMax(:) = 0.0_FP
    ! TODO: select a range from which the residual is taken
    do iEq = 1, nEqns
        do iCell = 1, nCells
            residNormSum(iEq) = residNormSum(iEq) + &
                cellVolume(iCell)*abs(resid(iEq,iCell))**(1.0_FP)
            residMax(iEq) = max(residMax(iEq),abs(resid(iEq,iCell)))
        end do
    end do
    do iEq = 1, nEqns
        residNorm(iEq) = (residNormSum(iEq)/totalVolume)**(1.0_FP/1.0_FP)   
    end do

end subroutine updateCells
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update edgeData consistently and conservatively by consulting immediate 
!>   neighboring elements.
!>
!> @history
!>  22 February 2016 - Initial creation (Maeng)
!>  25 April 2016 - Re-examine the formulation 
!>
subroutine reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges,qCell,qNode,qEdge )
    
    use solverVars, only: eps, iRight, iLeft
    use physics, only: gam, prim2conserv
    use meshUtil, only: faceCells, cellVolume, cellFaces, cellNodes, &
                        faceNormal, faceArea, faceNodes
    use mathUtil, only: lagrangeIntLoc, numAverageLagrange
    use boundaryConditions, only: edgeBC, periodicID
    use reconstruction, only: reconValue

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,     & !< problem dimension
                           nEqns,    & !< number of equations
                           nCells,   & !< number of cells
                           nNodes,   & !< number of nodes
                           nEdges      !< number of edges

    real(FP), intent(in) :: qCell(nEqns,nCells),    & !< cell average
                            qNode(nEqns,nNodes)       !< node data

    real(FP), intent(inout) :: qEdge(nEqns,nEdges) !< edge data

    ! Local variables
    integer :: iEq,     & !< equation index
               iPt,     & !< point index
               nPts,    & !< number of quadrature point
               iEdge,   & !< edge index
               iCell,   & !< cell index
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: qIncons(nEqns),         &
                qAvgL(nEqns),           &
                qAvgR(nEqns),           &
                coeffL(10),             &
                coeffR(10),             &
                qNodeEdgeL(nEqns,6),    &
                qNodeEdgeR(nEqns,6),    &
                qPrimAvg(nEqns)

    real(FP) :: qEdgeTemp(nEqns,nEdges), & !< temporary edge data storage
                dP(nEqns,nCells)           ! inconsistency in primitive variables

    real(FP), allocatable :: xi(:,:),       &
                             quadL(:,:),    &
                             quadR(:,:),    &
                             qTempL(:,:),   &
                             qTempR(:,:)
    nPts = 7
    allocate( xi(nDim,nPts), quadL(nEqns,nPts), quadR(nEqns,nPts), &
              qTempL(nEqns,nPts), qTempR(nEqns,nPts) )

    xi(:,:) = lagrangeIntLoc(nPts)
    
    qEdgeTemp(:,:) = qEdge(:,:)
    do iEdge = 1, nEdges
        lCell = faceCells(iLeft,iEdge)
        rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell

        if ( rCell < 0 ) then 
            if ( edgeBC(iEdge) == periodicID ) then
                ! for periodic BC, negative cell index
                rCell = abs(faceCells(iRight,iEdge))
            else
                ! free boundary, skip 
                cycle
            end if
        end if
        
        ! inconsistency calculation step
        qNodeEdgeL(:,1) = qNode(:,cellNodes(1,lCell))
        qNodeEdgeL(:,3) = qNode(:,cellNodes(2,lCell))
        qNodeEdgeL(:,5) = qNode(:,cellNodes(3,lCell))
        qNodeEdgeL(:,2) = qEdge(:,cellFaces(1,lCell))
        qNodeEdgeL(:,4) = qEdge(:,cellFaces(2,lCell))
        qNodeEdgeL(:,6) = qEdge(:,cellFaces(3,lCell))

        qNodeEdgeR(:,1) = qNode(:,cellNodes(1,rCell))
        qNodeEdgeR(:,3) = qNode(:,cellNodes(2,rCell))
        qNodeEdgeR(:,5) = qNode(:,cellNodes(3,rCell))
        qNodeEdgeR(:,2) = qEdge(:,cellFaces(1,rCell))
        qNodeEdgeR(:,4) = qEdge(:,cellFaces(2,rCell))
        qNodeEdgeR(:,6) = qEdge(:,cellFaces(3,rCell))

        quadL = 0.0_FP
        quadR = 0.0_FP
        ! assign primitive reconstruction coefficients
        do iEq = 1,nEqns
            coeffL(:) = coeffFromVal(nDim,0.0_FP,qNodeEdgeL(iEq,:))
            coeffL(7:10) = 0.0_FP
            
            coeffR(:) = coeffFromVal(nDim,0.0_FP,qNodeEdgeR(iEq,:))
            coeffR(7:10) = 0.0_FP

            do iPt = 1,nPts
                quadL(iEq,iPt) = reconValue(nDim,iEq,lCell,xi(:,iPt),coeffL)
                quadR(iEq,iPt) = reconValue(nDim,iEq,rCell,xi(:,iPt),coeffR)
            end do
        end do

        do iPt = 1,nPts
            if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
                 (eqFlag == EULER_EQ) ) then
                ! average in conserved variable
                qTempL(:,iPt) = prim2conserv(nDim,nEqns,quadL(:,iPt))                        
                qTempR(:,iPt) = prim2conserv(nDim,nEqns,quadR(:,iPt))                        
            else
                ! average in primivitve variable
                qTempL(:,iPt) = quadL(:,iPt)                        
                qTempR(:,iPt) = quadR(:,iPt)                        
            end if
        end do

        do iEq = 1,nEqns
            ! cell average using 7 point formula
            qAvgL(iEq) = numAverageLagrange(nDim,nPts,qTempL(iEq,:))
            qAvgR(iEq) = numAverageLagrange(nDim,nPts,qTempR(iEq,:))
        end do
 
        ! evaluate inconsistencies in conserved variables
        ! FIXME
        ! SO AS SOON AS THIS IS APPLIED, THE RESIDUAL IN CONSERVATION STAGE INCREASES
        ! INCREMENTALLY. WHAT IS THE PROBLEM?
        ! Linear interpolation - inverse area weighted average
        qIncons(:) = 1.0_FP/3.0_FP*( &
            ( cellVolume(rCell)*(qCell(:,lCell) - qAvgL(:)) + &
              cellVolume(lCell)*(qCell(:,rCell) - qAvgR(:)) ) / &
             (cellVolume(lCell)+cellVolume(rCell)) )
        !! Area averaged
        !qIncons(:) = 1.0_FP/3.0_FP*( (cellVolume(lCell)*qCell(:,lCell) + &
        !                              cellVolume(rCell)*qCell(:,rCell)) - &
        !                              cellVolume(lCell)*(qAvgL(:)) - &
        !                              cellVolume(rCell)*(qAvgR(:)) ) / &
        !            (cellVolume(lCell)+cellVolume(rCell))
        !! cell average density averaged
        !qIncons(:) = 1.0_FP/3.0_FP*( (qCell(1,lCell)*(qCell(:,lCell)-qAvgL(:)) + &
        !                              qCell(1,rCell)*(qCell(:,rCell)-qAvgR(:)) ) / &
        !                           (qCell(1,lCell)+qCell(1,rCell)) )
 
        dP(:,lCell) = (qCell(:,lCell)-qAvgL(:))
        if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
             (eqFlag == EULER_EQ) ) then

            ! evaluate primitive dual cell average
            qPrimAvg(:) = qEdge(:,iEdge)

            ! qIncons is made up of conserved values
            qEdgeTemp(1,iEdge) = qEdge(1,iEdge) + qIncons(1)
            qEdgeTemp(2,iEdge) = qEdge(2,iEdge) + &
                    (qIncons(2)-qPrimAvg(2)*qIncons(1))/qPrimAvg(1)
            qEdgeTemp(3,iEdge) = qEdge(3,iEdge) + &
                    (qIncons(3)-qPrimAvg(3)*qIncons(1))/qPrimAvg(1)
            qEdgeTemp(4,iEdge) = qEdge(4,iEdge) + & 
                    (gam-1.0_FP)*(qIncons(1)*(qPrimAvg(2)*qPrimAvg(2) + &
                                  qPrimAvg(3)*qPrimAvg(3))/2.0_FP - &
                    qIncons(2)*qPrimAvg(2) - qIncons(3)*qPrimAvg(3) + qIncons(4))
        else
            qEdgeTemp(:,iEdge) = qEdge(:,iEdge) + qIncons(:)
        end if
    end do 
    ! apply changes to edgeData
    qEdge(:,:) = qEdgeTemp(:,:)

    deallocate( xi, quadL, quadR, qTempL, qTempR )

end subroutine reconcileEdgeData
!-------------------------------------------------------------------------------
!> @purpose
!>  Use node and interface values to calculate an average flux
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  10 January 2014 - l/rNode correction for 1D (Maeng)
!>  11 March 2015 - 1D flux calculation (Maeng)
!>
function averageFlux(nDim,nEqns,fluxType,iEdge,lam)

    use meshUtil, only: faceNodes, faceCells
    use physics, only: flux
    use mathutil, only: simpson2d
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           nEqns,       & !< equation for flux
                           fluxType,    & !< flux equation
                           iEdge          !< interface

    real(FP), intent(in), optional :: lam(nDim) !< advection speed

    ! Function variable
    real(FP) :: averageFlux(nDim,nEqns)

    ! Local variables
    integer :: lNode,   & !< left face node
               rNode,   & !< right face node
               iCell,   &
               lCell,   &
               iDir,    & !< direction index
               iEq,     & !< equation index
               i,       & !< row index
               j          !< column index

    real(FP) :: fluxQuad(3,3,nDim,nEqns),   & !< analytical fluxes at quadrature points
                fluxSlice(3,3)                !< flux values for given dimen and eqn

    select case ( nDim )

        case (1)
            lNode = faceNodes(1,iEdge)
            rNode = faceNodes(1,iEdge)

        case (2)
            ! assuming ccw ordering of node
            lNode = faceNodes(2,iEdge)
            rNode = faceNodes(1,iEdge)

    end select

    ! The 'flux' function expects primitive state vectors !
    if ( present(lam) ) then
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode),lam )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge),lam )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode),lam )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode),lam )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge),lam )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode),lam )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode),lam )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge),lam )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode),lam )
    else
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode) )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge) )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode) )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode) )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge) )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode) )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode) )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge) )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode) )
    end if

    ! TODO: optimize loop for speed
    averageFlux(:,:) = 0.0_FP
    do iDir = 1,nDim
        do iEq = 1,nEqns
            fluxSlice(:,:) = 0.0_FP
            do i = 1,3
                do j = 1,3
                    fluxSlice(i,j) = fluxQuad(i,j,iDir,iEq)
                end do 
            end do
            averageFlux(iDir,iEq) = simpson2d( fluxSlice(:,:) )
        end do
    end do

end function averageFlux
!-------------------------------------------------------------------------------
!> @purpose 
!>  Loop around the domain and evaluate conservation
!>
!> @history
!>  25 November 2014 - Initial creation (Maeng)
!>  30 October 2015 - simplified
!>
subroutine conservationCheck(nEqns,nCells,qPrim,qCons)

    use meshUtil, only: faceCells, cellVolume, totalVolume

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations
                           nCells     !< number of cells

    real(FP), intent(in) :: qPrim(nEqns,nCells),    & !< primitive variables
                            qCons(nEqns,nCells)        !< cell average

    ! Local variables
    integer :: iCell,   & !< cell index
               iEdge,   & !< edge index
               iEq        !< equation index
    
    real(FP) :: primQ(nEqns),   & !< primitive variable sum
                consQ(nEqns)      !< conserved variable sum

    primQ(:) = 0.0_FP
    consQ(:) = 0.0_FP
    do iEq = 1,nEqns
        do iCell = 1,nCells
            primQ(iEq) = primQ(iEq) + (qPrim(iEq,iCell))*cellVolume(iCell) 
            consQ(iEq) = consQ(iEq) + (qCons(iEq,iCell))*cellVolume(iCell) 
        end do
        primNorm(iEq) = (primQ(iEq)/totalVolume)**(1.0_FP)
        conservNorm(iEq) = (consQ(iEq)/totalVolume)**(1.0_FP)
    end do
    !write(*,*) 'Conserved variables'
    !write(*,'(*(e24.16e02))') consQ

end subroutine conservationCheck
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate primitive cell average from conservative variables 
!>  for coupled systems. The primitive cell average are used for 
!>  constructing cell reconstruction coefficients.
!>  
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 March 2015 - Initial Creation
!>  21 September 2015 - Implementation of a new conservative 
!>      variable conversion for 1D pressureless euler
!>  23 September 2015 - Implementation of a new conservative
!>      variable conversion for 2D pressureless euler
!>  9 October 2015 - 2d euler equations, polytropic EOS for p
!>
subroutine conserv2primAvg(nDim,nEqns,nNodes,nEdges,nCells, &
                                    qNode,qEdge,qCons,qPrim)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces
    use physics, only: isentropicConst, gam

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nNodes,  & !< number of nodes
                           nEdges,  & !< number of edges
                           nCells,  & !< number of cells
                           nEqns      !< number of equations

    real(FP), intent(in) :: qNode(nEqns,nNodes),  & !< pointer for nodeData 
                            qEdge(nEqns,nEdges),  & !< pointer for edgeData
                            qCons(nEqns,nCells)     !< conservative state variable

    real(FP), intent(out) :: qPrim(nEqns,nCells)  !< primitive state variable

    !< Local variables
    integer :: iEq,     & !< equation index
               node1,   &
               node2,   &
               node3,   &
               iCell      !< cell index

    real(FP) :: qAvg(nEqns),  & !< local cell average
                rDiff(nDim),  & !< density derivatives
                uDiff(nDim),  & !< u derivatives
                vDiff(nDim),  & !< v derivatives
                sCorrU,       & !< second order vel. correction for conservation 
                sCorrV,       & !< second order vel. correction for conservation 
                sCorrE          !< second order energy correction for conservation 

    qPrim(:,:) = 0.0_FP
    select case ( nDim )

        case (1)
            ! new method
            do iCell = 1,nCells
                node1 = cellNodes(1,iCell)
                node2 = cellNodes(2,iCell)
                sCorrU = 1.0_FP/12.0_FP*(qNode(1,node2)-qNode(1,node1))*&
                                        (qNode(2,node2)-qNode(2,node1)) 
                qAvg(1) = qCons(1,iCell) 
                qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU)
                qPrim(:,iCell) = qAvg(:) 
            end do

        case (2)

            if ( (eqFlag == ISENTEULER_EQ) .or. (eqFlag == PLESSEULER_EQ) ) then
                ! pressureless euler/isentropic euler equations; rho, u, v
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    rDiff(1) = qNode(1,node2)-qNode(1,node1) ! drho/dxi
                    rDiff(2) = qNode(1,node3)-qNode(1,node1) ! drho/deta
                    uDiff(1) = qNode(2,node2)-qNode(2,node1)
                    uDiff(2) = qNode(2,node3)-qNode(2,node1)
                    vDiff(1) = qNode(3,node2)-qNode(3,node1)
                    vDiff(2) = qNode(3,node3)-qNode(3,node1)
                    ! correction for velocity
                    sCorrU = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*uDiff(1)-uDiff(2)) + &
                        rDiff(2)*(2.0_FP*uDiff(2)-uDiff(1)) )
                    sCorrV = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*vDiff(1)-vDiff(2)) + &
                        rDiff(2)*(2.0_FP*vDiff(2)-vDiff(1)) )
                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU) ! u average
                    qAvg(3) = 1.0_FP/qCons(1,iCell)*(qCons(3,iCell)-sCorrV) ! v average
                    qPrim(1:3,iCell) = qAvg(1:3)
                end do
            else if ( (eqFlag == EULER_EQ) ) then
                ! full Euler equations
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    rDiff(1) = qNode(1,node2)-qNode(1,node1) ! drho/dxi
                    rDiff(2) = qNode(1,node3)-qNode(1,node1) ! drho/deta
                    uDiff(1) = qNode(2,node2)-qNode(2,node1)
                    uDiff(2) = qNode(2,node3)-qNode(2,node1)
                    vDiff(1) = qNode(3,node2)-qNode(3,node1)
                    vDiff(2) = qNode(3,node3)-qNode(3,node1)

                    ! correction for velocity
                    sCorrU = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*uDiff(1)-uDiff(2)) + &
                        rDiff(2)*(2.0_FP*uDiff(2)-uDiff(1)) )
                    sCorrV = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*vDiff(1)-vDiff(2)) + &
                        rDiff(2)*(2.0_FP*vDiff(2)-vDiff(1)) )
                    sCorrE = 1.0_FP/36.0_FP*( &
                        qNode(1,node1)*((uDiff(1)-uDiff(2))**2.0_FP + uDiff(1)*uDiff(2) + &
                                        (vDiff(1)-vDiff(2))**2.0_FP + vDiff(1)*vDiff(2)) )

                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU) ! u average
                    qAvg(3) = 1.0_FP/qCons(1,iCell)*(qCons(3,iCell)-sCorrV) ! v average
                    qAvg(4) = (gam-1.0_FP)*(qCons(4,iCell) - &
                               0.5_FP*(qCons(2,iCell)*qCons(2,iCell) + &
                               qCons(3,iCell)*qCons(3,iCell))/qCons(1,iCell)-sCorrE) ! pressure average 
                    qPrim(1:4,iCell) = qAvg(1:4) 
                end do
            else
                ! scalar conservation laws. Linear advection
                qPrim(:,:) = qCons(:,:)
            end if

    end select

end subroutine conserv2primAvg
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients for each equation for a 
!>  given cell.
!>  
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial Creation\n
!>  8 May 2012 - Extend to 1-D
!>  6 January 2016 - Modified to get rid of bubble function (Maeng)
!>
function coefficients(nDim,iEq,iCell)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iEq,     & !< number of equations
                           iCell      !< cell index

    ! Function variable
    real(FP) :: coefficients((nDim+2)*(nDim+3)/(2*(3-nDim)))

    ! Local variables
    real(FP) :: uPoint(nDim*(nDim+1)),  & !< vector of edge and node values
                uAvg                      !< cell average

    coefficients = 0.0_FP

    uAvg = primAvgN(iEq,iCell)

    select case ( nDim )

        case (1)
           uPoint(1) = edgeDataN(iEq,cellFaces(1,iCell))
           uPoint(2) = edgeDataN(iEq,cellFaces(2,iCell))

        case (2)
           uPoint(1) = nodeDataN(iEq,cellNodes(1,iCell))
           uPoint(3) = nodeDataN(iEq,cellNodes(2,iCell))
           uPoint(5) = nodeDataN(iEq,cellNodes(3,iCell))

           uPoint(2) = edgeDataN(iEq,cellFaces(1,iCell))
           uPoint(4) = edgeDataN(iEq,cellFaces(2,iCell))
           uPoint(6) = edgeDataN(iEq,cellFaces(3,iCell))

    end select

    ! set reconstruction coefficients
    coefficients(:) = coeffFromVal(nDim,uAvg,uPoint)

end function coefficients
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients \f$c_i$\f given point values and 
!>  conserved quantity\n
!>  \n
!>  1d Equation (in reference coordinates):\n
!>  \f{equation*}{ 
!>      u(\xi) = c_1 \left(1-2\xi\right)\left(1-\xi\right) + 
!>               c_2 4\xi\left(1-\xi\right) + 
!>               c_3 \xi\left(2\xi-1\right)
!>  \f}
!>  \n
!>  2d Equation (in reference coordinates):\n 
!>  See basis function definitions in 'reconValue' function
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  4 June 2012 - Initial Creation
!>  27 July 2013 - Add tolerance for bubble coefficient
!>
function coeffFromVal(nDim,uAvg,uPoint)

    use solverVars, only: eps
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim !< problem dimension

    real(FP), intent(in) :: uAvg,                 & !< primitive average
                            uPoint(nDim*(nDim+1))   !< point values

    ! Function variable
    real(FP) :: coeffFromVal((nDim+2)*(nDim+3)/(2*(3-nDim)))
    real(FP) :: uNumAvg

    ! Local variables
    real(FP), parameter :: tol = 2.0e-10_FP !< tolerance for bubble coeff (FIXME)


    coeffFromVal(:) = 0.0_FP
    select case ( nDim )

        case (1)
            coeffFromVal(1) = uPoint(1)
            coeffFromVal(2) = 0.25_FP*(6.0_FP*uAvg-uPoint(1)-uPoint(2))
            coeffFromVal(3) = uPoint(2)

        case (2)

            coeffFromVal(1) = uPoint(1)
            coeffFromVal(2) = uPoint(2)
            coeffFromVal(3) = uPoint(3)
            coeffFromVal(4) = uPoint(4)
            coeffFromVal(5) = uPoint(5)
            coeffFromVal(6) = uPoint(6)

            ! FIXME: What is a good value for bubble function tolerance
            uNumAvg = (1.0_FP/3.0_FP)*(uPoint(2)+uPoint(4)+uPoint(6)) ! 2nd order accurate average
            !! bubble function
            !coeffFromVal(7) = (20.0_FP/9.0_FP)*(uAvg - uNumAvg)
            !!!if ( abs(coeffFromVal(7)) <= tol ) coeffFromVal(7) = 0.0_FP
            !if ( abs(coeffFromVal(7)) <= 1.0_FP*eps ) coeffFromVal(7) = 0.0_FP
            !coeffFromVal(8:10) = 0.0_FP

            ! NO bubble coefficient
            coeffFromVal(7:10) = 0.0_FP

    end select

end function coeffFromVal
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Apply crosswind diffusion to node points 
!!>
!!> @history
!!>  7 January 2016 - Initial creation (Maeng)
!!>
!subroutine nodeDiff( nDim,nEqns,nCells,qAvg )
!
!    use solverVars, only: eps
!    use meshUtil, only: cellNodes, cellFaces, nodeCoord, &
!                        cellVolume
!    use physics, only: conserv2prim
!    use boundaryConditions, only: applyBC
!    
!    implicit none
!
!    !> Interface variables
!    integer, intent(in) :: nDim,     & !< problem dimension
!                           nEqns,    & !< number of equations
!                           nCells      !< number of cells
!
!    real(FP), pointer, intent(in) :: qAvg(:,:)     !< pointer for nodeData 
!
!    !> Local variables
!    integer :: iEq,     & !< equation index
!               iDir,    & !< dimension index
!               iEdge,   & !< edge index
!               lEdge,   & !< edge index
!               rEdge,   & !< edge index
!               gEdge,   & !< edge index
!               gNode,   & !< node index
!               iNode,   & !< node index
!               lNode,   & !< node index
!               rNode,   & !< node index
!               glNode,  & !< node index
!               grNode,  & !< node index
!               iCell      !< cell index
!
!    real(FP) :: lam(nDim),     & !< average stream velocity 
!                sVec(nDim),    & !< streamline vector
!                nVec(nDim),    & !< normal vector
!                dx(nDim),      & !< edge difference in cartesian coordinate
!                ds(3),         & !< edge length in streamline coordinate
!                dn(3),         & !< edge length in streamline coordinate
!                curv(nEqns),   & !< second derivative in cross wind direction
!                coef             !< damping coeff.
!
!    real(FP) :: qPrim(nEqns) 
!
!    logical, target :: nUpdated(nNodes)     !< flag that a node has been updated
!
!    logical, pointer :: updatedPtr(:)         !< node/edge updated flag pointer
!
!    nUpdated(:) = .false.
!
!    coef = 0.5_FP
!
!    !qPrim = nodeData(:,:)
!
!    do iCell = 1, nCells
!        
!        !! set average stream velocity
!        !lam(1) = 1.0_FP/3.0_FP*(sum(edgeData(2,cellFaces(:,iCell)))) 
!        !lam(2) = 1.0_FP/3.0_FP*(sum(edgeData(3,cellFaces(:,iCell)))) 
!
!        !! streamline and normal vector
!        !if ( norm2(lam(:)) <= 2.0*eps ) cycle
!        !sVec(:) = lam(:)/norm2(lam(:))
!        !nVec(1) = -sVec(2)
!        !nVec(2) = sVec(1)
!
!        !! difference of Cartesian coordinates for edge index
!        !do iEdge = 1,3
!        !    lNode = iEdge-1 
!        !    if ( lNode == 0 ) lNode = 3
!        !    rNode = iEdge+1
!        !    if ( rNode > 3 ) rNode = 1
!        !    dx(:) = nodeCoord(:,cellNodes(lNode,iCell)) - &
!        !            nodeCoord(:,cellNodes(rNode,iCell))
!        !    ds(iEdge) = dot_product(sVec(:),dx(:))  
!        !end do
!
!        !! calculate second derivative in cross-wind direction
!        !curv(:) = 0.0_FP
!        !do iNode = 1,3
!        !    iEdge = iNode
!        !    lEdge = iEdge-1 
!        !    if ( lEdge == 0 ) lEdge = 3
!        !    rEdge = iEdge+1
!        !    if ( rEdge > 3 ) rEdge = 1
!
!        !    ! node diffusion
!        !    curv(:) = curv(:) + nodeData(:,cellNodes(iNode,iCell))*ds(iNode)*ds(iNode) 
!        !    ! edge diffusion
!        !    curv(:) = curv(:) + 2.0_FP*edgeData(:,cellFaces(iEdge,iCell))* &
!        !        ds(lEdge)*ds(rEdge)
!
!        !end do
!        !curv = curv/(cellVolume(iCell)*cellVolume(iCell))
!
!        !if ( iCell == 130 ) then
!        !    write(*,*) iCell, cellNodes(1,iCell), ds, &
!        !        curv(1:2)
!        !end if
!
!        !do iNode = 1,3
!        !    gNode = cellNodes(iNode,iCell)
!        !    nodeData(:,gNode) = nodeData(:,gNode) - 0.0*coef*curv(:)*ds(iNode)*ds(iNode) !*cellVolume(iCell)*cellVolume(iCell)
!        !    !nUpdated(gNode) = .true.
!        !    !if (gNode == 1000 ) then
!        !    !    write(*,*) iCell, gNode, nodeData(:,gNode), curv
!        !    !end if
!        !    !if ( nodeData(1,gNode) <= 0.0_FP ) then
!        !    !    write(*,*) 'WARNING: non physical density'
!        !    !    write(*,*) 'Node: ', gNode, 'Cell: ', iCell
!        !    !    write(*,*) nodeData(:,gNode)
!        !    !end if
!        !    
!        !    !! pointer allocation
!        !    !nodePtr => nodeData
!        !    !updatedPtr => nUpdated
!        !    !call applyBC(nEqns,gNode,curv(:),nodePtr,dtIn,updated=updatedPtr)
!        !end do
!
!        ! calculate diffusion for all primitive variables
!        ! very crude averaging method applied on node
!        !curv(:) = qAvg(:,iCell) - 1.0/3.0*(edgeData(:,cellFaces(1,iCell)) + &
!        !    edgeData(:,cellFaces(2,iCell))+edgeData(:,cellFaces(3,iCell)))
!
!        do iEdge = 1,3
!            gEdge = cellFaces(iEdge,iCell)
!            qPrim(:) = conserv2prim( nDim,nEqns,qAvg(:,gEdge) ) 
!            curv(:) = qPrim(:) - edgeData(:,gEdge)
!
!            lNode = iEdge - 1
!            if ( lNode == 0 ) lNode = 3
!            glNode = cellNodes(lNode,iCell)
!
!            rNode = iEdge + 1
!            if ( rNode == 4 ) rNode = 1
!            grNode = cellNodes(rNode,iCell)
!
!            nodeData(:,glNode) = nodeData(:,glNode) + 1.0_FP/6.0_FP*curv(:)
!            nodeData(:,grNode) = nodeData(:,grNode) + 1.0_FP/6.0_FP*curv(:)
!        end do
!        
!    end do
!    
!    nullify( nodePtr,updatedPtr )
!
!end subroutine nodeDiff
!-------------------------------------------------------------------------------
!> @purpose 
!>  Test 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 April 2015 - Initial creation
!>
subroutine test()

    use solverVars, only: eps, nIter
    use meshUtil, only: nDim, cellNodes, cellFaces, ref2cart, &
                        nodeCoord, edgeCoord, &
                        faceNodes, faceCells, &
                        cellInvJac, cellJac, cellVolume, &
                        faceNormal, faceArea, &
                        cellCentroid, faceNormalArea
    use reconstruction, only: reconValue, gradVal, secondGradVal
    use boundaryConditions, only: nodeBC, edgeBC

    implicit none

    !> Local variables
    integer :: iEq,     & !< equation index 
               iCell,   & !< cell index
               iFace,   & !< face index
               iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: xi(nDim),           & 
                sumNormal(nDim),    &
                vel(nDim),          &
                gradient(nDim),     &
                gradU(nDim,nDim),   &
                sGradU(nDim,nDim),  &
                sGradV(nDim,nDim)

    write(*,*) "fortran "
    write(*,*) nCells
    write(*,*) nEqns
    write(*,*) shape(nodeDataN), size(nodeDataN)
    write(*,*) shape(nodeBC), size(nodeBC)
    write(*,*) nodeDataN(:,124)
    write(*,*) nodeBC(5)

    !!! check second order gradient evaluation routines !!!
    !iCell = 111
    !xi = (/1.0_FP, 0.0_FP/)
    !write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)

    !vel(1) = reconValue( nDim,2,iCell,xi )
    !vel(2) = reconValue( nDim,3,iCell,xi )
    !write(*,*) 'U'
    !write(*,*) vel

    !gradU(1,:) = gradVal( nDim, 2, iCell, xi ) 
    !!write(*,*) 'dU/dXi'
    !!gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(1,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(1,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(1,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !gradU(2,:) = gradVal( nDim, 3, iCell, xi ) 
    !!write(*,*) gradient
    !!gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(2,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(2,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !write(*,*) 
    !write(*,*) 'dU/dX'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) gradU(2,1), gradU(2,2)

    !sGradU = secondGradVal( nDim,2,iCell,xi )
    !write(*,*) 'd2U/dX2'
    !write(*,*) sGradU(1,1), sGradU(1,2)
    !write(*,*) sGradU(2,1), sGradU(2,2)

    !sGradV = secondGradVal( nDim,3,iCell,xi )
    !write(*,*) 'd2V/dX2'
    !write(*,*) sGradV(1,1), sGradV(1,2)
    !write(*,*) sGradV(2,1), sGradV(2,2)

    !! check gradient evaluation routines !!!
    !iCell = 10
    !xi = (/1.0_FP, 0.0_FP/)
    !write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)
    !write(*,*) 'eps', eps
    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !write(*,*) 'dU/dXi'
    !write(*,*) gradient
    !gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! du/dx, du/dy
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !write(*,*) gradient
    !gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !write(*,*) 
    !write(*,*) 'cellInvJac'
    !write(*,*) cellInvJac(1,1,iCell), cellInvJac(1,2,iCell)
    !write(*,*) cellInvJac(2,1,iCell), cellInvJac(2,2,iCell)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)

    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !gradU(1,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(1,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !gradU(2,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(2,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !write(*,*)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac by manual multiplication'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)
    !! check gradient evaluation routines !!!
    
end subroutine test
!-------------------------------------------------------------------------------
end module update
!-------------------------------------------------------------------------------
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Update edge average values conservatively and update edgeData consistently
!!>
!!> @history
!!>  29 December 2015 - Initial creation (Maeng)
!!>  15 February 2015 - Consistently define edgeData
!!>
!subroutine updateEdges( nDim,nEqns,nEdges,fluxType,qEdgeN,qEdge,lam )
!    
!    use solverVars, only: dtIn, iRight, iLeft
!    use meshUtil, only: faceCells, cellVolume, cellFaces, cellNodes, &
!                        edgeCoord, nodeCoord, totalVolume, faceNodes, &
!                        maxNodesPerCell, maxFacesPerCell, faceNormalArea
!    use boundaryConditions, only: edgeBC, pEdgePair, periodicID
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,     & !< problem dimension
!                           nEqns,    & !< number of equations
!                           nEdges,   & !< number of edges
!                           fluxType    !< flux used for update
!
!    real(FP), intent(in) :: qEdgeN(nEqns,nEdges)           !< state at time n
!
!    real(FP), intent(inout) :: qEdge(nEqns,nEdges)         !< state at time n+1
!
!    real(FP), intent(in), optional :: lam(nDim) !< advection speed
!
!    ! Local variables
!    integer :: iEq,     & !< equation index
!               iEdge,   & !< edge index
!               jEdge,   & !< edge index
!               gEdge,   & !< edge index
!               pEdge,   & !< periodic edge pair index
!               iCell,   & !< cell index
!               lCell,   & !< left cell index
!               rCell      !< right cell index
!
!    real(FP) :: faceFlux(nDim,nEqns),   & !< flux at edge
!                resid(nEqns,nEdges),    & !< edge residual
!                dR(nEqns),              & !< change in residual value
!                fNormal(nDim),          & !< local face normal vector
!                fArea,                  & !< local face area
!                dualVolume                !< volume surrounding edge
!
!    real(FP) :: residNormSum(nEqns)       !< residual norm storage
!
!    resid(:,:) = 0.0_FP
!    qEdge(:,:) = qEdgeN(:,:)
!    do iEdge = 1, nEdges
!        lCell = faceCells(iLeft,iEdge)
!        rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell
!        if ( rCell < 0 ) then 
!            ! for periodic BC, negative cell index
!            if ( edgeBC(iEdge) == periodicID ) then
!                rCell = abs(faceCells(iRight,iEdge))
!                pEdge = pEdgePair(iEdge)
!            else
!                cycle
!                ! free boundary
!                !TODO: should implement boundary conditions
!            end if
!        end if
!
!        ! cell volume containing iEdge
!        dualVolume = (cellVolume(lCell) + cellVolume(rCell))
!
!        ! find residual surrounding edge, two from lCell two from rCell
!        ! left cell
!        do jEdge = 1,maxFacesPerCell
!            gEdge = cellFaces(jEdge,lCell)
!            if ( gEdge == iEdge ) cycle
!
!            ! find face normal and face area
!            call faceNormalArea(nDim,gEdge,lCell,fNormal,fArea)
!
!            faceFlux(:,:) = averageFlux( nDim,nEqns,fluxType,gEdge,lam )
!            do iEq = 1,nEqns
!                dR(iEq) = dot_product(faceFlux(:,iEq),fNormal(:))*fArea
!            end do
!            resid(:,iEdge) = resid(:,iEdge) + dR(:)
!        end do
!
!        ! right cell
!        do jEdge = 1,maxFacesPerCell
!            gEdge = cellFaces(jEdge,rCell)
!            if ( edgeBC(iEdge) == periodicID ) then
!                ! periodic boundary edge 
!                ! TODO: ghost cell needed 
!                if ( gEdge == pEdge ) cycle
!            else
!                if ( gEdge == iEdge ) cycle
!            end if 
!
!            call faceNormalArea(nDim,gEdge,rCell,fNormal,fArea)
!
!            faceFlux(:,:) = averageFlux( nDim,nEqns,fluxType,gEdge,lam )
!            do iEq = 1,nEqns
!                dR(iEq) = dot_product(faceFlux(:,iEq),fNormal(:))*fArea
!            end do
!            resid(:,iEdge) = resid(:,iEdge) + dR(:)
!        end do
!
!        ! update edge conservative variables
!        qEdge(:,iEdge) = qEdge(:,iEdge) - dtIn*resid(:,iEdge)/dualVolume
!
!    end do
!
!    ! Extra: edge residual
!    residNormSum(:) = 0.0_FP
!    do iEdge = 1, nEdges
!        do iEq = 1, nEqns
!            residNormSum(iEq) = residNormSum(iEq) + &
!                abs(resid(iEq,iEdge))**(1.0_FP)
!        end do
!    end do
!    do iEq = 1, nEqns
!        residNorm(iEq) = (residNormSum(iEq)/(3.0_FP*totalVolume))**(1.0_FP/1.0_FP)   
!    end do
!        
!end subroutine updateEdges
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Update edgeData consistently.
!!>
!!> @history
!!>  17 February 2016 - Initial creation (Maeng)
!!>
!subroutine updateEdgeData( nDim,nEqns,nCells,nEdges,qCell,qEdge )
!    
!    use solverVars, only: dtIn, iRight, iLeft
!    use physics, only: prim2conserv, conserv2prim
!    use meshUtil, only: faceCells, cellVolume, cellFaces, cellNodes, &
!                        edgeCoord, nodeCoord, totalVolume, faceNodes, &
!                        maxNodesPerCell, maxFacesPerCell, faceNormalArea
!    use boundaryConditions, only: edgeBC, pEdgePair, periodicID
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,     & !< problem dimension
!                           nEqns,    & !< number of equations
!                           nCells,   & !< number of cells
!                           nEdges      !< number of edges
!
!    real(FP), intent(in) :: qCell(nEqns,nCells),    &  !< cell avg
!                            qEdge(nEqns,nEdges)        !< edge avg
!
!    ! Local variables
!    integer :: iEq,     & !< equation index
!               iEdge,   & !< edge index
!               jEdge,   & !< edge index
!               gEdge,   & !< edge index
!               pEdge,   & !< periodic edge pair index
!               iCell,   & !< cell index
!               lCell,   & !< left cell index
!               rCell,   & !< right cell index
!               node1,   &
!               node2,   &
!               node3,   &
!               node4
!
!    real(FP) :: qConsPrim(nEqns)
!
!    do iEdge = 1, nEdges
!        lCell = faceCells(iLeft,iEdge)
!        rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell
!        node1 = faceNodes(iLeft,iEdge)
!        node2 = faceNodes(iRight,iEdge)
!        if ( rCell < 0 ) then 
!            ! for periodic BC, negative cell index
!            if ( edgeBC(iEdge) == periodicID ) then
!                rCell = abs(faceCells(iRight,iEdge))
!                pEdge = pEdgePair(iEdge)
!            else
!                cycle
!                ! free boundary
!            end if
!        end if
!
!        ! find residual surrounding edge, two from lCell two from rCell
!        ! left cell
!        do jEdge = 1,maxFacesPerCell
!            gEdge = cellFaces(jEdge,lCell)
!            if ( gEdge == iEdge ) then
!                node3 = cellNodes(jEdge,lCell)
!                cycle
!            end if
!        end do
!
!        ! right cell
!        do jEdge = 1,maxFacesPerCell
!            gEdge = cellFaces(jEdge,rCell)
!            if ( edgeBC(iEdge) == periodicID ) then
!                ! periodic boundary edge 
!                ! TODO: ghost cell needed 
!                if ( gEdge == pEdge ) then
!                    node4 = cellNodes(jEdge,rCell) ! not sure completely
!                    cycle
!                end if
!            else
!                if ( gEdge == iEdge ) then 
!                    node4 = cellNodes(jEdge,rCell)
!                    cycle
!                end if
!            end if 
!        end do
!
!        !! Detect any inconsistency in conservation and redistribute
!        ! original
!        qConsPrim(:) = 0.0_FP
!        if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
!             (eqFlag == EULER_EQ) ) then
!            ! evaluate conserved edgeData
!            qConsPrim(:) = 9.0_FP/2.0_FP*( qEdge(:,iEdge) - &
!                1.0_FP/3.0_FP*( qCell(:,lCell)*cellVolume(lCell) + &
!                qCell(:,rCell)*cellVolume(rCell) ) / &
!                (0.5_FP*(cellVolume(lCell)+cellVolume(rCell)))  - &
!                1.0_FP/36.0_FP*( prim2conserv(nDim,nEqns,nodeData(:,node1)) + &
!                prim2conserv(nDim,nEqns,nodeData(:,node2)) + &
!                prim2conserv(nDim,nEqns,nodeData(:,node3)) + &
!                prim2conserv(nDim,nEqns,nodeData(:,node4)) ) ) 
!
!            !! evaluate conserved edgeData
!            !qConsPrim(:) = 9.0_FP/2.0_FP*( qEdge(:,iEdge) - &
!            !    1.0_FP/3.0_FP*( qCell(:,lCell) + qCell(:,rCell) ) - &
!            !    1.0_FP/36.0_FP*( prim2conserv(nDim,nEqns,nodeData(:,node1)) + &
!            !    prim2conserv(nDim,nEqns,nodeData(:,node2)) + &
!            !    prim2conserv(nDim,nEqns,nodeData(:,node3)) + &
!            !    prim2conserv(nDim,nEqns,nodeData(:,node4)) ) ) 
!
!            ! return primitive edgeData value
!            edgeData(:,iEdge) = conserv2prim(nDim,nEqns,qConsPrim) 
!        else
!            edgeData(:,iEdge) = 9.0_FP/2.0_FP*( qEdge(:,iEdge) - &
!                1.0_FP/3.0_FP*( qCell(:,lCell)*cellVolume(lCell) + &
!                qCell(:,rCell)*cellVolume(rCell) )  / &
!                (0.5_FP*(cellVolume(lCell)+cellVolume(rCell)))  - &
!                1.0_FP/36.0_FP*( nodeData(:,node1) + nodeData(:,node2) + &
!                nodeData(:,node3) + nodeData(:,node4) ) )
!            !edgeData(:,iEdge) = 9.0_FP/2.0_FP*( qEdge(:,iEdge) - &
!            !    1.0_FP/3.0_FP*( qCell(:,lCell) + qCell(:,rCell) ) - &
!            !    1.0_FP/36.0_FP*( nodeData(:,node1) + &
!            !    nodeData(:,node2) + &
!            !    nodeData(:,node3) + &
!            !    nodeData(:,node4) ) ) 
!            !if ( iEdge > 10 .and. iEdge < 12 ) then
!            !    write(*,*) qEdge(1,iEdge), qCell(1,lCell), qCell(1,rCell), &
!            !                edgeData(1,iEdge)
!            !end if
!
!        end if
!    end do
!        
!end subroutine updateEdgeData
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Update edgeData consistently and conservatively by consulting immediate 
!!>   neighboring elements.
!!>
!!> @history
!!>  22 February 2016 - Initial creation (Maeng)
!!>
!subroutine reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges,qCell,qNode,qEdge )
!    
!    use solverVars, only: eps, iRight, iLeft
!    use physics, only: gam, prim2conserv
!    use meshUtil, only: faceCells, cellVolume, cellFaces, cellNodes, &
!                        faceNormal, faceArea, faceNodes
!    use mathUtil, only: lagrangeIntLoc, numAverageLagrange
!    use boundaryConditions, only: edgeBC, periodicID
!    use reconstruction, only: reconValue
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,     & !< problem dimension
!                           nEqns,    & !< number of equations
!                           nCells,   & !< number of cells
!                           nNodes,   & !< number of nodes
!                           nEdges      !< number of edges
!
!    real(FP), intent(in) :: qCell(nEqns,nCells),    & !< cell average
!                            qNode(nEqns,nNodes)       !< node data
!
!    real(FP), intent(inout) :: qEdge(nEqns,nEdges) !< edge data
!
!    ! Local variables
!    integer :: iEq,     & !< equation index
!               iPt,     & !< point index
!               nPts,    & !< number of quadrature point
!               iEdge,   & !< edge index
!               iCell,   & !< cell index
!               lCell,   & !< left cell index
!               rCell      !< right cell index
!
!    real(FP) :: dU(nEqns,nCells),       & ! inconsistency in conservative variables
!                dP(nEqns,nCells)          ! inconsistency in primitive variables
!
!    real(FP) :: qIncons(nEqns),         &
!                qAvgL(nEqns),           &
!                qAvgR(nEqns),           &
!                coeff(10),              &
!                uPoint(6),              &
!                qConsAvg(nEqns),        &
!                qPrimAvg(nEqns)
!
!    real(FP) :: qEdgeTemp(nEqns,nEdges)  !< temporary edge data storage
!    
!    real(FP), allocatable :: xi(:,:),       &
!                             quad(:,:),     &
!                             qTempL(:,:),   &
!                             qTempR(:,:)
!    nPts = 7
!    allocate( xi(nDim,nPts), quad(nEqns,nPts), &
!              qTempL(nEqns,nPts), qTempR(nEqns,nPts) )
!
!    xi(:,:) = lagrangeIntLoc(nPts)
!    
!    !write(*,*) 'correction stage'
!    dU(:,:) = 0.0_FP
!    dP(:,:) = 0.0_FP
!    ! cell loop. Loop through cells and find conservative inconsistency
!    do iCell = 1,nCells
!        quad(:,:) = 0.0_FP
!        ! assign primitive reconstruction coefficients
!        do iEq = 1,nEqns
!            uPoint(1) = qNode(iEq,cellNodes(1,iCell))
!            uPoint(3) = qNode(iEq,cellNodes(2,iCell))
!            uPoint(5) = qNode(iEq,cellNodes(3,iCell))
!            uPoint(2) = qEdge(iEq,cellFaces(1,iCell))
!            uPoint(4) = qEdge(iEq,cellFaces(2,iCell))
!            uPoint(6) = qEdge(iEq,cellFaces(3,iCell))
!
!            coeff(:) = coeffFromVal(nDim,0.0_FP,uPoint)
!            coeff(7:10) = 0.0_FP
!            do iPt = 1,nPts
!                quad(iEq,iPt) = reconValue(nDim,iEq,lCell,xi(:,iPt),coeff)
!            end do
!        end do
!
!        qTempR(:,:) = quad(:,:)
!        do iPt = 1,nPts
!            if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
!                 (eqFlag == EULER_EQ) ) then
!                ! average in conserved variable
!                qTempL(:,iPt) = prim2conserv(nDim,nEqns,quad(:,iPt))                        
!            else
!                ! average in primivitve variable
!                qTempL(:,iPt) = quad(:,iPt)
!            end if
!        end do
!
!        do iEq = 1,nEqns
!            ! numerical approximation of cell average at n+1 using 7 point formula
!            qConsAvg(iEq) = numAverageLagrange(nDim,nPts,qTempL(iEq,:))
!            qPrimAvg(iEq) = numAverageLagrange(nDim,nPts,qTempR(iEq,:))
!            ! inconsistency in conservative variables
!            dU(iEq,iCell) = (qCell(iEq,iCell) - qConsAvg(iEq))
!        end do
!
!        if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
!             (eqFlag == EULER_EQ) ) then
!            ! inconsistency in primitive variables
!            dP(1,iCell) = (dU(1,iCell))
!            dP(2,iCell) = (dU(2,iCell) - qPrimAvg(2)*dU(1,iCell))/qPrimAvg(1) 
!            dP(3,iCell) = (dU(3,iCell) - qPrimAvg(3)*dU(1,iCell))/qPrimAvg(1) 
!            dP(4,iCell) = (gam-1.0_FP)*(dU(1,iCell)*(qPrimAvg(2)*qPrimAvg(2) + &
!                           qPrimAvg(3)*qPrimAvg(3))/2.0_FP -  &
!                           dU(2,iCell)*qPrimAvg(2) - dU(3,iCell)*qPrimAvg(3) + &
!                           dU(4,iCell)) 
!        else
!            dP(:,iCell) = dU(:,iCell)
!        end if
!
!    end do
!    ! for debugging
!    debugVar(:,:) = dP(:,:)
!    
!    do iEdge = 1,nEdges
!        lCell = faceCells(iLeft,iEdge)
!        rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell
!
!        if ( rCell < 0 ) then 
!            if ( edgeBC(iEdge) == periodicID ) then
!                ! for periodic BC, negative cell index
!                rCell = abs(faceCells(iRight,iEdge))
!            else
!                ! free boundary, skip 
!                cycle
!            end if
!        end if
!
!        ! FIXME
!        ! SO AS SOON AS THIS IS APPLIED, THE RESIDUAL IN CONSERVATION STAGE INCREASES
!        ! INCREMENTALLY. WHAT IS THE PROBLEM?
!        ! Area averaged - creates streaks
!        !qIncons(:) = 1.0_FP/3.0_FP*( cellVolume(lCell)*dP(:,lCell) + &
!        !                             cellVolume(rCell)*dP(:,rCell) ) / &
!        !                           (cellVolume(lCell) + cellVolume(rCell))
!        !! mass averaged
!        !qIncons(:) = 1.0_FP/3.0_FP*( cellVolume(lCell)*qCell(1,lCell)*dP(:,lCell) + &
!        !                             cellVolume(rCell)*qCell(1,rCell)*dP(:,rCell) ) / &
!        !                           ( cellVolume(lCell)*qCell(1,lCell) + &
!        !                             cellVolume(rCell)*qCell(1,rCell) )
!        ! cell average density averaged
!        qIncons(:) = 1.0_FP/3.0_FP*( qCell(1,lCell)*dP(:,lCell) + &
!                                     qCell(1,rCell)*dP(:,rCell) ) / &
!                                   ( qCell(1,lCell) + qCell(1,rCell) )
!        ! apply changes to edgeData
!        qEdge(:,iEdge) = qEdge(:,iEdge) + qIncons(:)
!    end do
!
!    !!------------------------------------------------------------------------------------
!    !! check if inconsistencies have been applied
!    !do iCell = 1,nCells
!    !    quad(:,:) = 0.0_FP
!    !    ! assign primitive reconstruction coefficients
!    !    do iEq = 1,nEqns
!    !        uPoint(1) = qNode(iEq,cellNodes(1,iCell))
!    !        uPoint(3) = qNode(iEq,cellNodes(2,iCell))
!    !        uPoint(5) = qNode(iEq,cellNodes(3,iCell))
!    !        ! these have been changed from above step
!    !        uPoint(2) = qEdge(iEq,cellFaces(1,iCell))
!    !        uPoint(4) = qEdge(iEq,cellFaces(2,iCell))
!    !        uPoint(6) = qEdge(iEq,cellFaces(3,iCell))
!    !        coeff(:) = coeffFromVal(nDim,0.0_FP,uPoint)
!    !        coeff(7:10) = 0.0_FP
!    !        do iPt = 1,nPts
!    !            quad(iEq,iPt) = reconValue(nDim,iEq,lCell,xi(:,iPt),coeff)
!    !        end do
!    !    end do
!    !    qTempR(:,:) = quad(:,:)
!    !    do iPt = 1,nPts
!    !        if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
!    !             (eqFlag == EULER_EQ) ) then
!    !            ! average in conserved variable
!    !            qTempL(:,iPt) = prim2conserv(nDim,nEqns,quad(:,iPt))                        
!    !        else
!    !            ! average in primivitve variable
!    !            qTempL(:,iPt) = quad(:,iPt) 
!    !        end if
!    !    end do
!    !    do iEq = 1,nEqns
!    !        ! numerical approximation of cell average at n+1 using 7 point formula
!    !        qConsAvg(iEq) = numAverageLagrange(nDim,nPts,qTempL(iEq,:))
!    !        qPrimAvg(iEq) = numAverageLagrange(nDim,nPts,qTempR(iEq,:))
!    !        ! inconsistency in conservative variables
!    !        dU(iEq,iCell) = (qCell(iEq,iCell) - qConsAvg(iEq)) 
!    !    end do
!
!    !    if ( (eqFlag == PLESSEULER_EQ) .or. (eqFlag == ISENTEULER_EQ) .or. &
!    !         (eqFlag == EULER_EQ) ) then
!    !        ! inconsistency in primitive variables
!    !        dP(1,iCell) = (dU(1,iCell))
!    !        dP(2,iCell) = (dU(2,iCell) - qPrimAvg(2)*dU(1,iCell))/qPrimAvg(1) 
!    !        dP(3,iCell) = (dU(3,iCell) - qPrimAvg(3)*dU(1,iCell))/qPrimAvg(1) 
!    !        dP(4,iCell) = (gam-1.0_FP)*(dU(1,iCell)*(qPrimAvg(2)*qPrimAvg(2) + &
!    !                       qPrimAvg(3)*qPrimAvg(3))/2.0_FP -  &
!    !                       dU(2,iCell)*qPrimAvg(2) - dU(3,iCell)*qPrimAvg(3) + &
!    !                       dU(4,iCell)) 
!    !    else
!    !        dP(:,iCell) = dU(:,iCell)
!    !    end if
!    !end do
!    ! for debugging
!    !debugVar2(:,:) = dP(:,:)
!    ! check if inconsistencies have been applied
!    !------------------------------------------------------------------------------------
!end subroutine reconcileEdgeData
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Return exact fluxes and related
!!>
!!> @history
!!>  21 March 2016 - Initial creation 
!!>
!subroutine exactFluxAverage( nDim,nEqns,nCells,fluxType,lam )
!    
!    use solverVars, only: tSim, iRight, iLeft
!    use meshUtil, only: faceCells, faceNormal, faceArea, cellVolume, &
!                        cellFaces, faceNodes, ref2cart, totalVolume, &
!                        nNodes, nodeCoord, edgeCoord
!    use physics, only: flux
!    use analyticFunctions, only: evalFunction
!    use mathUtil, only: gaussQuadLoc, gaussQuadWt 
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,     & !< problem dimension
!                           nEqns,    & !< number of equations
!                           nCells,   & !< number of cells
!                           fluxType    !< flux used for update
!
!    real(FP), intent(in), optional :: lam(nDim)
!
!    ! Local variables
!    integer :: iEq,     & !< equation index
!               iEdge,   & !< edge index
!               iNode,   &
!               lNode,   & !< node index
!               rNode,   & !< node index
!               iCell,   & !< cell index
!               lCell,   & !< left cell index
!               rCell      !< right cell index
!
!    ! exact flux calculation temp
!    integer :: iPoint,  & 
!               jPoint,  &
!               nPts = 5
!
!    real(FP) :: fluxSum(nDim,nEqns),    &
!                fluxQuad(nDim,nEqns),   &
!                dRExact(nEqns),         & !< change in residual value
!                qTemp(nEqns),   &
!                xCart(nDim),    & !< physical coordinate
!                dL(nDim),       &
!                tVal,           &
!                xi(5),          & !< reference coorindate
!                wt(5)
!
!    xi(:) = gaussQuadLoc(5)
!    wt(:) = gaussQuadWt(5)
!
!    residExactVol(:,:) = 0.0_FP
!    residExact(:,:) = 0.0_FP
!
!    do iEdge = 1, nEdges
!   
!        ! left and right cell index
!        lCell = faceCells(iLeft,iEdge)
!        rCell = faceCells(iRight,iEdge)
!        ! left and right node of face/edge
!        lNode = faceNodes(2,iEdge)
!        rNode = faceNodes(1,iEdge)
!
!        ! error stuff 
!        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        ! point flux
!        edgePtFlux(:,:,iEdge) = flux(nDim,nEqns,fluxType,edgeData(:,iEdge),lam) 
!        !qTemp(:) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge),t=(tSim+dtIn))
!        qTemp(:) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge),t=0.0_FP)
!        edgePtFluxExact(:,:,iEdge) = flux(nDim,nEqns,fluxType,qTemp,lam)
!
!        ! exact face flux average in space and time
!        fluxSum(:,:) = 0.0_FP
!        dL(:) = (nodeCoord(:,rNode)-nodeCoord(:,lNode))
!        do iPoint = 1, nPts
!            ! space integration
!            xCart(:) = nodeCoord(:,lNode) + dL*0.5_FP*(xi(iPoint) + 1.0_FP)
!            do jPoint = 1,nPts
!                ! time integration
!                !tVal = tSim + dtIn*0.5_FP*(xi(jPoint) + 1.0_FP)
!                !qTemp(:) = evalFunction(nDim,nEqns,xCart,t=tVal)
!
!                ! for moving vortex
!                tVal = -dtIn + dtIn*0.5_FP*(xi(jPoint) + 1.0_FP)
!                qTemp(:) = evalFunction(nDim,nEqns,xCart,t=tVal)
!                ! for moving vortex
!
!                !qTemp(:) = evalFunction(nDim,nEqns,xCart,t=0.0_FP)
!                fluxQuad(:,:) = flux(nDim,nEqns,fluxType,qTemp,lam)
!                fluxSum(1,:) = fluxSum(1,:) + (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*fluxQuad(1,:)
!                fluxSum(2,:) = fluxSum(2,:) + (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*fluxQuad(2,:)
!            end do
!        end do
!        fluxAvgExact(:,:,iEdge) = fluxSum(:,:)
!        ! residual error in cell
!        do iEq = 1,nEqns
!            dRExact(iEq) = dot_product(fluxAvgExact(:,iEq,iEdge),faceNormal(:,iEdge)) * &
!                      faceArea(iEdge)
!        end do
!        residExact(:,lCell) = residExact(:,lCell) - dRExact(:)/cellVolume(lCell)
!        residExactVol(:,lCell) = residExactVol(:,lCell) - dRExact(:)
!        if ( rCell > 0 ) then
!            residExact(:,rCell) = residExact(:,rCell) + dRExact(:)/cellVolume(rCell)
!            residExactVol(:,rCell) = residExactVol(:,rCell) + dRExact(:)
!        end if
!        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    end do
! 
!    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    do iNode = 1,nNodes
!        nodePtFlux(:,:,iNode) = flux(nDim,nEqns,fluxType,nodeData(:,iNode),lam) 
!        !qTemp(:) = evalFunction(nDim,nEqns,nodeCoord(:,iNode),t=(tSim+dtIn))
!        qTemp(:) = evalFunction(nDim,nEqns,nodeCoord(:,iNode),t=0.0_FP)
!        nodePtFluxExact(:,:,iNode) = flux(nDim,nEqns,fluxType,qTemp,lam) 
!    end do
!    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     
!end subroutine exactFluxAverage
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Find the scaled inward normal of vertex. 
!!>
!!> @history
!!>  24 September 2014 - Initial creation (Maeng)
!!>
!function vertNorm(nDim,iCell,nodeEdge)
!
!    use solverVars, only: eps
!    use meshUtil, only: maxFacesPerCell, maxNodesPerCell, &
!                        cellFaces, nodeCoord, edgeCoord, &
!                        faceNodes, cellNodes
!    use mathUtil, only: pointInTri
!
!    implicit none
!
!    !> Interface variables
!    integer :: nDim,         & !< number of dimensions
!               iCell,        & !< cell index
!               nodeEdge(maxNodesPerCell)     !< node/edge index for subcell
!
!    !> Function variable
!    real(FP) :: vertNorm(nDim,maxNodesPerCell) !< vertex inward normals
!
!    !> Local variables
!    integer :: iNodeEdge,   & !< current local node/edge index
!               gNodeEdge,   & !< global node/edge index
!               lNodeEdge,   & !< left local node/edge
!               rNodeEdge      !< right local node/edge
!
!    real(FP) :: xNodeEdge1(nDim),   & !< node/edge coord
!                xNodeEdge2(nDim),   & !< node edge coord 
!                xDiff(nDim)           !< difference in coord
!
!    real(FP) :: xN(nDim),                    & !< 
!                xVert(nDim,maxNodesPerCell)
!
!    do iNodeEdge = 1,maxNodesPerCell
!        ! find local index for edge left and right of node
!        ! counter-clockwise orderiing for left and right node index
!        lNodeEdge = mod((iNodeEdge+1),3)
!        if ( lNodeEdge == 0 ) lNodeEdge = 3
!        rNodeEdge = mod((iNodeEdge+2),3)
!        if ( rNodeEdge == 0 ) rNodeEdge = 3       
!
!        ! node/edge global index and coordinates 
!        if ( nodeEdge(lNodeEdge) > 0 ) then  ! node index
!            gNodeEdge = cellNodes(nodeEdge(lNodeEdge),iCell)
!            xNodeEdge1(:) = nodeCoord(:,gNodeEdge) 
!        else ! edge index 
!            gNodeEdge = cellFaces(abs(nodeEdge(lNodeEdge)),iCell) 
!            xNodeEdge1(:) = edgeCoord(:,gNodeEdge) 
!        end if
!
!        if ( nodeEdge(rNodeEdge) > 0 ) then  ! node index
!            gNodeEdge = cellNodes(nodeEdge(rNodeEdge),iCell)
!            xNodeEdge2(:) = nodeCoord(:,gNodeEdge) 
!        else ! edge index 
!            gNodeEdge = cellFaces(abs(nodeEdge(rNodeEdge)),iCell) 
!            xNodeEdge2(:) = edgeCoord(:,gNodeEdge) 
!        end if              
!        xDiff(:) = xNodeEdge2(:)-xNodeEdge1(:)
!        
!        ! inward scaled normal from vertex to opposite face
!        ! outward scaled normal of edge, to neighboring cell
!        ! This has a different sign from the RD formulation
!        vertNorm(1,iNodeEdge) = xDiff(2) 
!        vertNorm(2,iNodeEdge) = -xDiff(1)
!
!        xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
!        xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
!        xN(:) = 0.5_FP*(xNodeEdge2(:)+xNodeEdge1(:)) + dtIn*vertNorm(:,iNodeEdge)
!        if ( pointInTri(xN,xVert) ) then
!            vertNorm(:,iNodeEdge) = -vertNorm(:,iNodeEdge)
!        end if
!
!    end do
!   
!end function vertNorm
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Returns the index of downstream node, or inflow edge (which is opposite of 
!!>  downstream node). If two inflow edges, returns upstream node with negative
!!>  sign. 
!!>  Always returns the index of node that is opposite of 1 inflow or 1 outflow edge!
!!>  1 inflow edge - 2 outflow edges
!!>  2 inflow edges - 1 outflow edge
!!>
!!> @history
!!>  2 October 2014 - Initial creation (Maeng)
!!>  20 October 2014 - k conditioning function included
!!>
!function nodeFlowCheck(k) result(iDownNode) 
!
!    use solverVars, only: eps
!    use meshUtil, only: maxNodesPerCell
!
!    implicit none
!
!    ! Interface variables
!    real(FP) :: k(maxNodesPerCell) !< vertex normal 
!
!    !> Function variable
!    integer :: iDownNode !< Downstream node index
!
!    !> Local variables
!    integer :: iNode,       & !< local node index
!               downCount,   & !< downstream node counter
!               iUpNode        !< upstream node index
!
!    iUpNode = 0
!    downCount = 0
!    do iNode = 1, maxNodesPerCell
!        ! Downstream node index is the same as inflow edge index 
!        ! TODO: Need to establish the best tolerance for k
!        if (  ( k(iNode) ) < -1.0_FP*eps ) then 
!            ! Downstream node - outflow side 
!            downCount = downCount + 1
!            iDownNode = iNode
!        else
!            ! Upstream node - inflow side
!            ! INCLUDES streamline edge
!            iUpNode = iNode
!        end if
!    end do
!    
!    ! IF two inflow edges, return upstream node index
!    if ( downCount > 1 ) iDownNode = -iUpNode
!
!end function nodeFlowCheck 
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Vector conditioner
!!>
!!> @author
!!>  Brad Maeng
!!>
!!> @history
!!>  20 October 2014 - Initial creation 
!!>
!function vecCond(k,kLen)
!    
!    use solverVars, only: eps
!
!    implicit none
!    
!    !> Interface variables
!    integer, intent(in) :: kLen         !< length of k vector
!
!    real(FP), intent(inout) :: k(kLen)     !< k vector
!
!    !> Output variable
!    real(FP) :: vecCond(kLen)
!
!    !> Local variables
!    integer :: iLen     !< vector index
!
!    real(FP) :: tol
!
!    tol = eps
!
!    do iLen = 1,kLen
!    
!        if ( abs(k(iLen)) <= tol ) then
!            ! allow room for zeroes
!            vecCond(iLen) = 0.0_FP
!        else
!            vecCond(iLen) = k(iLen)
!        end if
!
!    end do
!    
!end function vecCond
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Damp advection bubble function value and correct edge values to maintain
!!>  conservation of the entire computational domain
!!>
!!> @author
!!>  Brad Maeng
!!>
!!> @history
!!>  18 September 2014 - Initial creation
!!>  3 October 2014 - Bubble function tolerance included
!!>  26 November 2014 - Finalized to keep conservation before and after damping
!!>
!subroutine bubbleDamp(lam,iter)
!
!    use solverVars, only: eps, iLeft, iRight
!
!    use meshUtil, only: nDim, cellVolume, cellNodes, cellFaces, &
!                        faceNodes, faceCells, edgeCoord, &
!                        maxFacesPerCell, maxNodesPerCell
!
!    use boundaryConditions, only: applyBC, edgeBC, pEdgePair
!
!    use reconstruction, only: reconData
!    
!    implicit none
!
!    !> Interface variables
!    real(FP), intent(in), optional :: lam(nDim)
!
!    integer, intent(in), optional :: iter           !< iteration
!
!    !> Local variables
!    integer :: iEq,     & !< equation index
!               iCell,   & !< cell index
!               jCell,   & !< cell index
!               iNode,   & !< local node index
!               iEdge,   & !< local edge index
!               edge1,   & !< cell edge 1 
!               edge2,   & !< cell edge 2
!               lCell,   & !< current cell index
!               rCell1,  & !< neighbor cell index 1
!               rCell2,  & !< neighbor cell index 2
!               gFace,   & !< global face index
!               gFace1,  & !< global face index 1
!               gFace2,  & !< global face index 2
!               neglectedEdges     !< number of edges without an update
!
!    integer :: countUpdatedEdges(nEdges)   !< number of bubble updated edges
!
!    logical, target :: edgeUpdated(nEdges)    !< edge update flag
!
!    logical, pointer :: updatedPtr(:)         !< node/edge updated flag pointer
!
!    real(FP) :: waveSpeed(nDim),    & !< wave speed 
!                cDamp,              & !< damping coefficient
!                sRef,               & !< edge signal weight for conservation
!                gamma1,             & !< edge signal weight 1
!                gamma2,             & !< edge signal weight 2
!                signal(nEqns),      & !< bubble coefficient change 
!                bubbleCoef(nEqns),  & !< bubble coefficient 
!                k(maxNodesPerCell), & !< scaled vertex normal
!                nf(nDim,maxNodesPerCell) !< vertex normals into opposite edge
!
!    integer, parameter, dimension(3) :: &
!                nodes = (/ 1, 2, 3 /) !< local node index
!
!    cDamp = 1.0_FP/2.0_FP   ! damping coefficient
!    countUpdatedEdges(:) = 0
!    edgeUpdated(:) = .false.
!
!    !iCell = 1200
!    !jCell = 1200
!    !if ( iCell == jCell ) write(*,*) 'before', reconData(1,7,iCell), &
!    !        1.0/3.0*(reconData(1,2,iCell)+reconData(1,4,iCell)+reconData(1,6,iCell))+ &
!    !        9.0/20.0*reconData(1,7,iCell)
!    !if ( iCell == jCell ) write(*,*) 'before', reconData(1,7,iCell), &
!    !        reconData(1,2,iCell), reconData(1,4,iCell), reconData(1,6,iCell)
!    !        !reconData(:,2,iCell), reconData(:,4,iCell), reconData(:,6,iCell), &
!
!    do iCell = 1,nCells
!        if ( present(lam) ) then
!            waveSpeed(:) = lam(:)
!        else
!            waveSpeed(1) = 1.0_FP/3.0_FP*(sum(edgeDataN(2,cellFaces(:,iCell)))) 
!            waveSpeed(2) = 1.0_FP/3.0_FP*(sum(edgeDataN(3,cellFaces(:,iCell)))) 
!        end if
!
!        ! Calculate the signal to change the bubble function
!        bubbleCoef(:) = reconData(:,7,iCell) 
!        signal(:) = cDamp*bubbleCoef(:) 
!        
!        ! check k coefficient for inflow/outflow determination 
!        nf = vertNorm(nDim,iCell,nodes)
!        k(1) = 0.5_FP*dot_product(waveSpeed,nf(:,1))
!        k(2) = 0.5_FP*dot_product(waveSpeed,nf(:,2))
!        k(3) = 0.5_FP*dot_product(waveSpeed,nf(:,3))
!
!        !if ( iCell == jCell ) write(*,*) 'k', k, sum(k)
!
!        ! node index for inflow edge check
!        iNode = nodeFlowCheck(k)
!        if ( iNode > 0 ) then 
!            ! one inflow, two outflow - contributions to two outflow edges
!            gFace = cellFaces(iNode,iCell) ! face index opposite of iNode
!
!            ! outflow edge local and global indices
!            edge1 = mod((iNode+1),3) ! outflow edge 1
!            if ( edge1 == 0 ) edge1 = 3
!            edge2 = mod((iNode+2),3) ! outflow edge 2
!            if ( edge2 == 0 ) edge2 = 3       
!            gFace1 = cellFaces(edge1,iCell) ! outflow edge 1
!            gFace2 = cellFaces(edge2,iCell) ! outflow edge 2
!
!            rCell1 = abs(faceCells(iRight,gFace1)) ! neighbor cell of outflow edge 1
!            if ( rCell1 == iCell ) rCell1 = faceCells(iLeft,gFace1) ! flip side
!                
!            rCell2 = abs(faceCells(iRight,gFace2)) ! neighbor cell of outflow edge 2
!            if ( rCell2 == iCell ) rCell2 = faceCells(iLeft,gFace2) ! flip side
!
!            ! edge weight, approach 1
!            gamma1 = abs(k(edge2))/abs(k(iNode)) ! edge weight 1
!            if ( abs(gamma1) <= 1.0_FP*eps ) gamma1 = 0.0_FP
!            gamma2 = 1.0_FP - gamma1
!            !gamma2 = abs(k(edge1))/abs(k(iNode)) ! edge weight 2 
!            !if ( abs(gamma2) <= eps ) gamma2 = 0.0_FP
!
!            ! edge weight, approach 2, doesn't work
!            !gamma1 = abs(k(edge1))/abs(k(iNode)) ! edge weight 1
!            !if ( abs(gamma1) <= eps ) gamma1 = 0.0_FP
!            !gamma2 = 1.0_FP - gamma1
!            !gamma2 = abs(k(edge2))/abs(k(iNode)) ! edge weight 2 
!            !if ( abs(gamma2) <= eps ) gamma2 = 0.0_FP
!
!            ! edge weight for conservation
!            sRef = (27.0_FP/20.0_FP) / &
!                   ( gamma1*(1.0_FP + cellVolume(rCell1)/cellVolume(iCell)) + &
!                     gamma2*(1.0_FP + cellVolume(rCell2)/cellVolume(iCell)) )
!            
!            ! Apply changes to outflow edge 1
!            edgeDataN(:,gFace1) = edgeDataN(:,gFace1) + &
!                        (gamma1*sRef)*signal(:)
!            countUpdatedEdges(gFace1) = countUpdatedEdges(gFace1) + 1
!            edgeUpdated(gFace1) = .true.
!            edgePtr => edgeDataN
!            updatedPtr => edgeUpdated
!            ! Apply boundary condition, periodic BC
!            call applyBC(nEqns,-gFace1,(gamma1*sRef)*signal(:),&
!                    edgePtr,dtIn,iter,updatedPtr)
!
!            ! Apply changes to outflow edge 2
!            edgeDataN(:,gFace2) = edgeDataN(:,gFace2) + &
!                        (gamma2*sRef)*signal(:) 
!            countUpdatedEdges(gFace2) = countUpdatedEdges(gFace2) + 1
!            edgeUpdated(gFace2) = .true.
!            ! Apply boundary condition, periodic BC 
!            call applyBC(nEqns,-gFace2,(gamma2*sRef)*signal(:), &
!                    edgePtr,dtIn,iter,updatedPtr)
!
!            do iEq = 1,nEqns
!                ! update bubble coefficient in neighbor cells
!                reconData(iEq,:,rCell1) = coefficients(nDim,iEq,rCell1)
!                reconData(iEq,:,rCell2) = coefficients(nDim,iEq,rCell2)
!            end do
!            
!            !if ( iCell == jCell ) write(*,*) 'one', cellFaces(iNode,iCell), &
!            !    gFace1, gFace2, rCell1, rCell2, gamma1, gamma2, &
!            !    gamma1*sRef*signal(1), gamma2*sRef*signal(1), signal(1) 
!
!        else
!            ! two inflow and one outflow - bubble contribute to ONE OUTFLOW edge only
!            gFace = cellFaces(abs(iNode),iCell) ! face index opposite of iNode
!
!            rCell1 = abs(faceCells(iRight,gFace))
!            if ( rCell1 == iCell ) then
!                rCell1 = faceCells(iLeft,gFace) ! flip side
!            end if
!
!            gamma1 = 1.0_FP  ! edge weight 1
!            ! edge weight for conservation
!            sRef = (27.0_FP/20.0_FP) / &
!                   ( gamma1*(1.0_FP + cellVolume(rCell1)/cellVolume(iCell)) )
!
!            ! Apply changes to outflow edge 1
!            edgeDataN(:,gFace) = edgeDataN(:,gFace) + &
!                                (gamma1*sRef)*signal(:)
!            edgePtr => edgeDataN
!            updatedPtr => edgeUpdated
!            edgeUpdated(gFace) = .true.
!            countUpdatedEdges(gFace) = countUpdatedEdges(gFace) + 1
!            ! Apply boundary condition, periodic BC
!            call applyBC(nEqns,-gFace,(gamma1*sRef)*signal(:), &
!                    edgePtr,dtIn,iter,updatedPtr)
!            
!            do iEq = 1,nEqns
!                ! update bubble coefficient in neighbor cells
!                reconData(iEq,:,rCell1) = coefficients(nDim,iEq,rCell1)
!            end do 
!
!            !if ( iCell == jCell ) write(*,*) 'two', cellFaces(abs(iNode),iCell), &
!            !    gFace, rCell1, gamma1, &
!            !    gamma1*sRef*signal(1), signal(1) 
!
!        end if
!
!        !if ( iCell == jCell ) write(*,*) 'during', reconData(1,7,iCell), &
!        !    reconData(1,2,iCell), reconData(1,4,iCell), reconData(1,6,iCell)
!        !if ( iCell == jCell ) write(*,*) 'during', reconData(1,7,iCell), &
!        !    edgeDataN(1,cellFaces(1,iCell)), edgeDataN(1,cellFaces(2,iCell)), &
!        !    edgeDataN(1,cellFaces(3,iCell))
!
!        !update reconData in current cell
!        do iEq = 1, nEqns
!            reconData(iEq,:,iCell) = coefficients(nDim,iEq,iCell)
!        end do
!
!        !if ( iCell == jCell ) write(*,*) 'during', reconData(1,7,iCell), &
!        !    edgeDataN(1,cellFaces(1,iCell)), edgeDataN(1,cellFaces(2,iCell)), &
!        !    edgeDataN(1,cellFaces(3,iCell))
!        !if ( iCell == jCell ) write(*,*) 'during', reconData(1,7,iCell), &
!        !    reconData(1,2,iCell), reconData(1,4,iCell), reconData(1,6,iCell)
!        !    !1.0/3.0*(reconData(1,2,iCell)+reconData(1,4,iCell)+reconData(1,6,iCell))+ &
!        !    !9.0/20.0*reconData(1,7,iCell)
!        !    !reconData(:,2,iCell), reconData(:,4,iCell), reconData(:,6,iCell), &
!
!    end do
!
!    !iCell = jCell
!    !if ( iCell == jCell ) write(*,*) 'after ', reconData(1,7,iCell), &
!    !        1.0/3.0*(reconData(1,2,iCell)+reconData(1,4,iCell)+reconData(1,6,iCell))+ &
!    !        9.0/20.0*reconData(1,7,iCell)
!    !        !reconData(:,2,iCell), reconData(:,4,iCell), reconData(:,6,iCell), &
!
!    ! Set all values equal in time
!    edgeData = edgeDataN
!    edgeDataH = edgeDataN
!
!    !neglectedEdges = nEdges - count( edgeUpdated )
!    !if (( neglectedEdges > 0 ) ) then
!    !    write(*,*)
!    !    write(*,'(2(a,i0),a)') 'WARNING: ', &
!    !               neglectedEdges,' edges have not been updated in Bubble damp.'
!    !    write(*,*)
!    !    !write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
!    !    !write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
!    !    !do iEdge = 1, nEdges
!    !    !    if ( .not. edgeUpdated(iEdge) ) then
!    !    !        write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
!    !    !                                    edgeBC(iEdge)
!    !    !    end if
!    !    !end do
!    !    write(*,*)
!    !end if
!
!    ! Check for edges/nodes without an update
!#ifdef VERBOSE
!    neglectedEdges = nEdges - count( edgeUpdated )
!    if (( neglectedEdges > 0 ) ) then
!        write(*,*)
!        write(*,'(2(a,i0),a)') 'WARNING: ', &
!                   neglectedEdges,' edges have not been updated in Bubble damp.'
!        write(*,*)
!        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
!        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
!        do iEdge = 1, nEdges
!            if ( .not. edgeUpdated(iEdge) ) then
!                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
!                                            edgeBC(iEdge)
!                !if ( edgebc(iedge) == 0 ) then
!                !    read(*,*)
!                !end if
!            end if
!        end do
!        write(*,*)
!    end if
!#endif
!
!end subroutine bubbleDamp
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Damp bubble function by averaging between two neighboring cells
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  20 January 2014 - Initial creation
!!>
!subroutine bubbleDampTest(nDim,nEqns,nCells,nEdges,iter,qCell,qEdge)
!
!    use solverVars, only: eps, iLeft, iRight
!    use meshUtil, only: cellVolume, cellNodes, cellFaces, &
!                        faceNodes, faceCells, edgeCoord, &
!                        maxFacesPerCell, maxNodesPerCell
!    use boundaryConditions, only: applyBC, edgeBC, pEdgePair
!    use physics, only: prim2conserv, conserv2prim
!    
!    implicit none
!
!    !> Interface variables
!    integer, intent(in) :: nDim,    & !< problem dimension
!                           nEqns,   & !< number of equations
!                           nCells,  & !< number of cells
!                           nEdges,  & !< number of edges
!                           iter       !< iteration number
!
!    real(FP), intent(in) :: qCell(nEqns,nCells)    !< bubble functions 
!
!    real(FP), intent(inout) :: qEdge(nEqns,nEdges)
!
!    !> Local variables
!    integer, parameter, dimension(3) :: &
!                nodes = (/ 1, 2, 3 /) !< local node index
!
!    integer :: iEq,     & !< equation index
!               iCell,   & !< cell index
!               jCell,   & !< cell index
!               iNode,   & !< local node index
!               iEdge,   & !< local edge index
!               edge1,   & !< cell edge 1 
!               edge2,   & !< cell edge 2
!               lCell,   & !< current cell index
!               rCell1,  & !< neighbor cell index 1
!               rCell2,  & !< neighbor cell index 2
!               gFace,   & !< global face index
!               gFace1,  & !< global face index 1
!               gFace2,  & !< global face index 2
!               neglectedEdges     !< number of edges without an update
!
!    integer :: countUpdatedEdges(nEdges)   !< number of bubble updated edges
!
!    logical, target :: edgeUpdated(nEdges)    !< edge update flag
!
!    logical, pointer :: updatedPtr(:)         !< node/edge updated flag pointer
!
!    real(FP) :: waveSpeed(nDim),    & !< wave speed 
!                cDamp,              & !< damping coefficient
!                sRef,               & !< edge signal weight for conservation
!                gamma1,             & !< edge signal weight 1
!                gamma2,             & !< edge signal weight 2
!                signal(nEqns),      & !< bubble coefficient change 
!                bubbleCoef(nEqns),  & !< bubble coefficient 
!                k(maxNodesPerCell), & !< scaled vertex normal
!                nf(nDim,maxNodesPerCell) !< vertex normals into opposite edge
!
!    real(FP), target :: qEdgeCons(nEqns,nEdges)
!
!    real(FP) :: bubble(nEqns,nCells)
!
!    cDamp = 1.0_FP/2.0_FP   ! damping coefficient
!    countUpdatedEdges(:) = 0
!    edgeUpdated(:) = .false.
!
!    ! convert primitive edge values to conserved
!    do iEdge = 1, nEdges
!        qEdgeCons(:,iEdge) = prim2conserv(nDim,nEqns,qEdge(:,iEdge))
!    end do
!
!    bubble = 0.0_FP
!    do iCell = 1,nCells
!        do iEq = 1,nEqns
!            ! conservative bubble functions
!            bubble(iEq,iCell) = 20.0_FP/9.0_FP*(qCell(iEq,iCell) - &
!                1.0_FP/3.0_FP*( qEdgeCons(iEq,cellFaces(1,iCell)) + &
!                qEdgeCons(iEq,cellFaces(2,iCell)) + &
!                qEdgeCons(iEq,cellFaces(3,iCell)) )) 
!        end do
!    end do
!
!    do iCell = 1,nCells
!
!        !waveSpeed(1) = 1.0_FP/3.0_FP*(sum(qEdge(2,cellFaces(:,iCell)))) 
!        !waveSpeed(2) = 1.0_FP/3.0_FP*(sum(qEdge(3,cellFaces(:,iCell)))) 
!        waveSpeed(1) = qCell(2,iCell)/qCell(1,iCell) 
!        waveSpeed(2) = qCell(3,iCell)/qCell(1,iCell) 
!
!        ! Calculate the signal to change the bubble function
!        bubbleCoef(:) = bubble(:,iCell)
!        signal(:) = cDamp*bubbleCoef(:) 
!        
!        ! check k coefficient for inflow/outflow determination 
!        nf = vertNorm(nDim,iCell,nodes)
!        k(1) = 0.5_FP*dot_product(waveSpeed,nf(:,1))
!        k(2) = 0.5_FP*dot_product(waveSpeed,nf(:,2))
!        k(3) = 0.5_FP*dot_product(waveSpeed,nf(:,3))
!
!        ! node index for inflow edge check
!        iNode = nodeFlowCheck(k)
!        if ( iNode > 0 ) then 
!            ! one inflow, two outflow - contributions to two outflow edges
!            gFace = cellFaces(iNode,iCell) ! face index opposite of iNode
!
!            ! outflow edge local and global indices
!            edge1 = mod((iNode+1),3) ! outflow edge 1
!            if ( edge1 == 0 ) edge1 = 3
!            edge2 = mod((iNode+2),3) ! outflow edge 2
!            if ( edge2 == 0 ) edge2 = 3       
!            gFace1 = cellFaces(edge1,iCell) ! outflow edge 1
!            gFace2 = cellFaces(edge2,iCell) ! outflow edge 2
!
!            rCell1 = abs(faceCells(iRight,gFace1)) ! neighbor cell of outflow edge 1
!            if ( rCell1 == iCell ) rCell1 = faceCells(iLeft,gFace1) ! flip side
!                
!            rCell2 = abs(faceCells(iRight,gFace2)) ! neighbor cell of outflow edge 2
!            if ( rCell2 == iCell ) rCell2 = faceCells(iLeft,gFace2) ! flip side
!
!            ! edge weight, approach 1
!            gamma1 = abs(k(edge2))/abs(k(iNode)) ! edge weight 1
!            if ( abs(gamma1) <= 1.0_FP*eps ) gamma1 = 0.0_FP
!            gamma2 = 1.0_FP - gamma1
!            !gamma2 = abs(k(edge1))/abs(k(iNode)) ! edge weight 2 
!            !if ( abs(gamma2) <= eps ) gamma2 = 0.0_FP
!
!            ! edge weight for conservation
!            sRef = (27.0_FP/20.0_FP) / &
!                   ( gamma1*(1.0_FP + cellVolume(rCell1)/cellVolume(iCell)) + &
!                     gamma2*(1.0_FP + cellVolume(rCell2)/cellVolume(iCell)) )
!            
!            ! Apply changes to outflow edge 1
!            qEdgeCons(:,gFace1) = qEdgeCons(:,gFace1) + &
!                        (gamma1*sRef)*signal(:)
!            countUpdatedEdges(gFace1) = countUpdatedEdges(gFace1) + 1
!            edgeUpdated(gFace1) = .true.
!            edgePtr => qEdgeCons
!            updatedPtr => edgeUpdated
!            ! Apply boundary condition, periodic BC
!            call applyBC(nEqns,-gFace1,(gamma1*sRef)*signal(:), &
!                    edgePtr,dtIn,iter,updatedPtr)
!
!            ! Apply changes to outflow edge 2
!            qEdgeCons(:,gFace2) = qEdgeCons(:,gFace2) + &
!                        (gamma2*sRef)*signal(:) 
!            countUpdatedEdges(gFace2) = countUpdatedEdges(gFace2) + 1
!            edgeUpdated(gFace2) = .true.
!            ! Apply boundary condition, periodic BC 
!            call applyBC(nEqns,-gFace2,(gamma2*sRef)*signal(:), &
!                    edgePtr,dtIn,iter,updatedPtr)
!
!            !if ( iCell == jCell ) write(*,*) 'one', cellFaces(iNode,iCell), &
!            !    gFace1, gFace2, rCell1, rCell2, gamma1, gamma2, &
!            !    gamma1*sRef*signal(1), gamma2*sRef*signal(1), signal(1) 
!
!        else
!            ! two inflow and one outflow - bubble contribute to ONE OUTFLOW edge only
!            gFace = cellFaces(abs(iNode),iCell) ! face index opposite of iNode
!
!            rCell1 = abs(faceCells(iRight,gFace))
!            if ( rCell1 == iCell ) then
!                rCell1 = faceCells(iLeft,gFace) ! flip side
!            end if
!
!            gamma1 = 1.0_FP  ! edge weight 1
!            ! edge weight for conservation
!            sRef = (27.0_FP/20.0_FP) / &
!                   ( gamma1*(1.0_FP + cellVolume(rCell1)/cellVolume(iCell)) )
!
!            ! Apply changes to outflow edge 1
!            qEdgeCons(:,gFace) = qEdgeCons(:,gFace) + &
!                                (gamma1*sRef)*signal(:)
!            edgePtr => qEdgeCons
!            updatedPtr => edgeUpdated
!            edgeUpdated(gFace) = .true.
!            countUpdatedEdges(gFace) = countUpdatedEdges(gFace) + 1
!            ! Apply boundary condition, periodic BC
!            call applyBC(nEqns,-gFace,(gamma1*sRef)*signal(:), &
!                    edgePtr,dtIn,iter,updatedPtr)
!            
!            !if ( iCell == jCell ) write(*,*) 'two', cellFaces(abs(iNode),iCell), &
!            !    gFace, rCell1, gamma1, &
!            !    gamma1*sRef*signal(1), signal(1) 
!
!        end if
!        
!    end do
!
!    do iEdge = 1,nEdges
!        ! convert back to primitive edge values
!        qEdge(:,iEdge) = conserv2prim(nDim,nEqns,qEdgeCons(:,iEdge))
!    end do
!
!
!    ! Check for edges/nodes without an update
!#ifdef VERBOSE
!    neglectedEdges = nEdges - count( edgeUpdated )
!    if (( neglectedEdges > 0 ) ) then
!        write(*,*)
!        write(*,'(2(a,i0),a)') 'WARNING: ', &
!                   neglectedEdges,' edges have not been updated in Bubble damp.'
!        write(*,*)
!        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
!        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
!        do iEdge = 1, nEdges
!            if ( .not. edgeUpdated(iEdge) ) then
!                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
!                                            edgeBC(iEdge)
!                !if ( edgebc(iedge) == 0 ) then
!                !    read(*,*)
!                !end if
!            end if
!        end do
!        write(*,*)
!    end if
!#endif
!
!end subroutine bubbleDampTest
