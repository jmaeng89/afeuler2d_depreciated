!-------------------------------------------------------------------------------
!> Functions used to iterate and find nonlinear advection solutions for 
!>  pressureless Euler equations and Burgers' equations
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  8 July 2016 - Initial creation
!>
module iterativeFunctions

    use solverVars, only: FP

    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Solve nonlinear primitive pressureless Euler equations iteratively 
!>  Can double as iterative method to evaluate solutions for nonconservative 
!>   nonlinear advection equations
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial creation
!>  31 August 2015 - Implement Newton's method of iteration
!>  26 September 2015 - Implement Fixed-Point iteration
!>
function pLessEulerSol(nDim,nEqns,qSim,xCart,tf)

    use solverVars, only: initSolnType, inLength, pi, eps
    use analyticFunctions

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    real(FP), intent(in) :: qSim(nEqns),    & !< final solution from simulation 
                            xCart(nDim),    & !< physical coordinates
                            tf                !< final time

    ! Function variable
    real(FP) :: pLessEulerSol(nEqns)
  
    ! Local variables
    integer, parameter:: DENSITY = 1, & !< density index
                         UVEL = 2,    & !< u velocity index
                         VVEL = 3       !< v velocity index

    integer :: iter,    &   !< iteration counter 
               nIter = 50  !< total iteration count

    real(FP) :: xStar(nDim),      & !< iteration point 
                xStarX(nDim),     & !< iteration point 
                xStarY(nDim),     & !< iteration point 
                qVarT(nEqns),     & !< primitive variables 
                qVar(nEqns),      & !< primitive variables 
                func(nDim),       & !< function to iterate
                funx1(nDim),      & !< 
                funx2(nDim),      & !< 
                funx3(nDim),      & !< 
                funx4(nDim),      & !< 
                funy1(nDim),      & !< 
                funy2(nDim),      & !< 
                funy3(nDim),      & !< 
                funy4(nDim),      & !< 
                detJ,             & !< function jacobian derivative
                u1(nDim),         &
                u2(nDim),         &
                du(nDim),         & !< difference
                ux, uy, vx, vy,   & !< derivatives at the ch. origin
                J(nDim,nDim),     & !< Jacobian
                ds,               & !< length scale
                tol,              &
                r, d, kk, k, r1, r2

    tol = eps
    ds = 1.0_FP*sqrt(abs(tol))
    pLessEulerSol(:) = 0.0_FP

    if ( nDim == 2 ) then
        ! initial guesses
        qVar = qSim
        u1 = qSim(UVEL:VVEL) 
        ! iteration for velocities
        do iter = 1, nIter

            xStar = xCart - tf*u1
            ! step size based on data
            ds = sqrt(abs(tol))*max(max(abs(xStar(1)),abs(xStar(2))),1.0_FP)

            ! call function to iterate
            qVar = evalFunction(nDim,nEqns,xStar)
            func = u1 - qVar(UVEL:VVEL) 

            ! calculate derivatives of function
            xStarX(1) = xCart(1) - tf*(u1(1)+ds) 
            xStarX(2) = xCart(2) - tf*(u1(2))
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx1(1) = (u1(1)+ds) - qVar(UVEL) 
            funx1(2) = (u1(2)) - qVar(VVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)-ds)
            xStarX(2) = xCart(2) - tf*(u1(2))
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx2(1) = (u1(1)-ds) - qVar(UVEL) 
            funx2(2) = (u1(2)) - qVar(VVEL) 

            !xStarX(1) = xCart(1) - tf*(u1(1)+2.0_FP*ds) 
            !xStarX(2) = xCart(2) - tf*(u1(2))
            !qVar = evalFunction(nDim,nEqns,xStarX(:))
            !funx3(1) = (u1(1)+2.0_FP*ds) - qVar(UVEL) 
            !funx3(2) = (u1(2)) - qVar(VVEL)

            !xStarX(1) = xCart(1) - tf*(u1(1)-2.0_FP*ds) 
            !xStarX(2) = xCart(2) - tf*(u1(2))
            !qVar = evalFunction(nDim,nEqns,xStarX(:))
            !funx4(1) = (u1(1)-2.0_FP*ds) - qVar(UVEL) 
            !funx4(2) = (u1(2)) - qVar(VVEL)

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)+ds)
            qVar = evalFunction(nDim,nEqns,xStarY(:))
            funy1(1) = (u1(1)) - qVar(UVEL) 
            funy1(2) = (u1(2)+ds) - qVar(VVEL) 

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)-ds)
            qVar = evalFunction(nDim,nEqns,xStarY(:))
            funy2(1) = (u1(1)) - qVar(UVEL) 
            funy2(2) = (u1(2)-ds) - qVar(VVEL) 

            !xStarY(1) = xCart(1) - tf*(u1(1))
            !xStarY(2) = xCart(2) - tf*(u1(2)+2.0_FP*ds)
            !qVar = evalFunction(nDim,nEqns,xStarY(:))
            !funy3(1) = (u1(1)) - qVar(UVEL) 
            !funy3(2) = (u1(2)+2.0_FP*ds) - qVar(VVEL) 

            !xStarY(1) = xCart(1) - tf*(u1(1))
            !xStarY(2) = xCart(2) - tf*(u1(2)-2.0_FP*ds)
            !qVar = evalFunction(nDim,nEqns,xStarY(:))
            !funy4(1) = (u1(1)) - qVar(UVEL) 
            !funy4(2) = (u1(2)-2.0_FP*ds) - qVar(VVEL) 

            ! second order accurate first derivatives
            J(1,1) = (funx1(1)-funx2(1))/(2.0*ds)  !fu
            J(1,2) = (funy1(1)-funy2(1))/(2.0*ds)  !fv
            J(2,1) = (funx1(2)-funx2(2))/(2.0*ds)  !gu
            J(2,2) = (funy1(2)-funy2(2))/(2.0*ds)  !gv

            !! fourth order accurate first derivatives
            !J(1,1) = (8.0_FP*funx1(1)-8.0_FP*funx2(1)-funx3(1)+funx4(1))/(12.0*ds)  !fu
            !J(1,2) = (8.0_FP*funy1(1)-8.0_FP*funy2(1)-funy3(1)+funy4(1))/(12.0*ds)  !fv
            !J(2,1) = (8.0_FP*funx1(2)-8.0_FP*funx2(2)-funx3(2)+funx4(2))/(12.0*ds)  !gu
            !J(2,2) = (8.0_FP*funy1(2)-8.0_FP*funy2(2)-funy3(2)+funy4(2))/(12.0*ds)  !gv

            ! function determinant
            detJ = J(1,1)*J(2,2)-J(1,2)*J(2,1)   
            if ( abs(detJ) < tol ) exit
            du(1) = -(func(1)*J(2,2)-func(2)*J(1,2))/detJ
            du(2) = -(-func(1)*J(2,1)+func(2)*J(1,1))/detJ

            ! Newton Raphson iteration
            u2 = u1 + du ! new velocity
            xStar = xCart - tf*u2
            qVar = evalFunction(nDim,nEqns,xStar)
            u1 = qVar(UVEL:VVEL)
            if ( abs(du(1)) <= tol .and. abs(du(2)) <= tol ) exit 
            !if ( abs(du(1)) < ds .and. abs(du(2)) < ds ) exit 

            !! Fixed-point iteration
            !u1 = qVar(UVEL:VVEL)
            !if ( abs(func(1)) < ds .and. abs(func(2)) < ds ) exit
            
        end do
        xStar = xCart - tf*u1
        qVar = evalFunction(nDim,nEqns,xStar)

        ! velocity derivatives
        !select case ( trim(adjustl(funcName)) )
        !    case ( 'plEu2dt1' )
        !        ! plEu2dt1
        !        ux = 1.0_FP/3.0_FP*pi/(5.0_FP)*sin(pi*(xStar(1))/(5.0_FP))
        !        uy = 0.0_FP
        !        vx = 0.0_FP
        !        vy = 1.0_FP/3.0_FP*pi/(5.0_FP)*cos(pi*(xStar(2))/(5.0_FP))
        !    case ( 'plEu2dt2' )
        !        ! plEu2dt2
        !        ux = 1.0_FP/3.0_FP*pi/(5.0_FP)*sin(pi*(xStar(1)+xStar(2))/(5.0_FP))
        !        uy = 1.0_FP/3.0_FP*pi/(5.0_FP)*sin(pi*(xStar(1)+xStar(2))/(5.0_FP))
        !        vx = 1.0_FP/3.0_FP*pi/(5.0_FP)*cos(pi*(xStar(1)+xStar(2))/(5.0_FP))
        !        vy = 1.0_FP/3.0_FP*pi/(5.0_FP)*cos(pi*(xStar(1)+xStar(2))/(5.0_FP))
        !    case ( 'solidRot' )
        !        ! solid body rotation
        !        k = 0.5_FP
        !        ux = 0.0_FP
        !        uy = -k
        !        vx = k 
        !        vy = 0.0_FP
        !    case ( 'svortex' )
        !        ! stationary Vortex
        !        k = 5.0_FP
        !        r = sqrt(xStar(1)**2.0_FP + xStar(2)**2.0_FP)
        !        ux =  k/(2.0_FP*pi)*xStar(1)*xStar(2)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
        !        uy =  k/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))*(xStar(2)**2.0_FP-1.0_FP)
        !        vx =  k/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))*(1.0_FP-xStar(1)**2.0_FP)
        !        vy =  -k/(2.0_FP*pi)*xStar(1)*xStar(2)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
        !end select

        ! numerical approx of gradients
        xStarX(1) = xStar(1) + ds 
        xStarX(2) = xStar(2)
        qVarT = evalFunction(nDim,nEqns,xStarX)
        funx1 = qVarT(UVEL:VVEL) 
        xStarX(1) = xStar(1) - ds 
        xStarX(2) = xStar(2)
        qVarT = evalFunction(nDim,nEqns,xStarX)
        funx2 = qVarT(UVEL:VVEL) 
        ux = (funx1(1)-funx2(1))/(2.0*ds)
        vx = (funx1(2)-funx2(2))/(2.0*ds)

        xStarX(1) = xStar(1) 
        xStarX(2) = xStar(2) + ds
        qVarT = evalFunction(nDim,nEqns,xStarX)
        funx1 = qVarT(UVEL:VVEL) 
        xStarX(1) = xStar(1) 
        xStarX(2) = xStar(2) - ds
        qVarT = evalFunction(nDim,nEqns,xStarX)
        funx2 = qVarT(UVEL:VVEL)
        uy = (funx1(1)-funx2(1))/(2.0*ds)
        vy = (funx1(2)-funx2(2))/(2.0*ds)

        ! Converged solutions
        pLessEulerSol(1) = qVar(1)/(1.0_FP + (ux + vy)*tf + (ux*vy - uy*vx)*tf*tf) ! for pressureless
        pLessEulerSol(2) = qVar(2)
        pLessEulerSol(3) = qVar(3)
  
    else
        ! 1D 
        ! initial guesses
        qVar = qSim
        u1 = qVar(UVEL)
        ! iteration for velocities
        do iter = 1, nIter 

            xStar(1) = xCart(1) - tf*u1(1)
            ! step size based on data
            ds = sqrt(abs(tol))*max(abs(xStar(1)),1.0_FP)

            ! call function to iterate
            qVar = evalFunction(nDim,nEqns,xStar(1))
            func = u1 - qVar(UVEL) 

            ! calculate derivatives of function
            xStarX(1) = xCart(1) - tf*(u1(1)+ds) 
            qVar = evalFunction(nDim,nEqns,xStarX(1))
            funx1 = (u1(1)+ds) - qVar(UVEL) 
            xStarX(1) = xCart(1) - tf*(u1(1)-ds) 
            qVar = evalFunction(nDim,nEqns,xStarX(1))
            funx2 = (u1(1)-ds) - qVar(UVEL) 

            J(1,1) = (funx1(1)-funx2(1))/(2.0*ds)  !fx

            ! function derivative
            detJ = J(1,1)  !fx
            if ( detJ < tol ) exit
            du(1) = -func(1)/detJ

            ! Newton Raphson iteration
            u2 = u1 + du ! new velocity
            xStar(1) = xCart(1) - tf*u2(1)
            qVar = evalFunction(nDim,nEqns,xStar(1))
            u1 = qVar(UVEL)
            if ( abs(du(1)) < tol ) exit
            
            !! Fixed-point iteration
            !u1 = qVar(UVEL)
            !if ( abs(func(1)) < ds ) exit

        end do
        xStar(1) = xCart(1) - tf*u1(1)
        qVar = evalFunction(nDim,nEqns,xStar(1))

        ! velocity derivative
        ! numerical approx
        xStarX(1) = xStar(1) + ds 
        qVarT = evalFunction(nDim,nEqns,xStarX(1))
        funx1 = qVarT(UVEL) 
        xStarX(1) = xStar(1) - ds 
        qVarT = evalFunction(nDim,nEqns,xStarX(1))
        funx2 = qVarT(UVEL) 
        ux = (funx1(1)-funx2(1))/(2.0*ds)

        ! Converged solutions
        pLessEulerSol(1) = qVar(1)/(1.0_FP + (ux)*tf)
        pLessEulerSol(2) = qVar(2)
    
    end if

end function pLessEulerSol
!-------------------------------------------------------------------------------
!> @purpose 
!>  Solve nonlinear Burgers' equations iteratively using method of 
!>  characteristics 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial creation
!>  31 August 2015 - Implement Newton's method of iteration
!>  26 September 2015 - Implement Fixed-Point iteration
!>
function burgSol(nDim,nEqns,qSim,xCart,tf,funcN)

    use solverVars, only: pi, eps, inLength
    use analyticFunctions

    implicit none
    
    ! Interface variables
    character(inLength), intent(in), optional :: funcN !< name of function

    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    real(FP), intent(in) :: qSim(nEqns),    & !< final solution from simulation 
                            xCart(nDim),    & !< physical coordinates
                            tf                !< final time

    ! Function variable
    real(FP) :: burgSol(nEqns)
  
    ! Local variables
    integer, parameter:: DENSITY = 1, & !< density index
                         UVEL = 2,    & !< u velocity index
                         VVEL = 3       !< v velocity index

    integer :: iter,    &   !< iteration counter 
               nIter = 200  !< total iteration count

    real(FP) :: xStar(nDim),      & !< iteration point 
                xStarX(nDim),     & !< iteration point 
                xStarY(nDim),     & !< iteration point 
                qVar(nEqns),      & !< primitive variables 
                func(nDim),       & !< function to iterate
                funx1(nDim),     & !< function to iterate, x derivative
                funx2(nDim),     & !< function to iterate, x derivative
                funx3(nDim),     & !< function to iterate, x derivative
                funx4(nDim),     & !< function to iterate, x derivative
                funy1(nDim),     & !< function to iterate, y derivative
                funy2(nDim),     & !< function to iterate, y derivative
                funy3(nDim),     & !< function to iterate, y derivative
                funy4(nDim),     & !< function to iterate, y derivative
                u1(nDim),         &
                u2(nDim),         &
                du(nDim),         & !< difference
                J(nDim,nDim),     & !< Jacobian
                detJ,             & !< Jacobian determinant
                ds,               & !< length scale
                tol                

    tol = eps
    ds = 1.0_FP*sqrt(abs(tol))
    burgSol(:) = 0.0_FP

    if ( nDim == 2 ) then
        ! initial guesses
        qVar = qSim
        u1 = qSim(UVEL:VVEL) 
        ! iteration for velocities
        do iter = 1, nIter

            xStar = xCart - tf*u1
            ! step size based on data
            !ds = sqrt(abs(tol))*max(max(abs(xStar(1)),abs(xStar(2))),1.0_FP)

            ! call function to iterate
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStar,funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStar)
            end if
            func = u1 - qVar(UVEL:VVEL) 

            ! calculate approximate derivatives of function
            xStarX(1) = xCart(1) - tf*(u1(1)+ds) 
            xStarX(2) = xCart(2) - tf*(u1(2))
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarX(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarX(:))
            end if
            funx1(1) = (u1(1)+ds) - qVar(UVEL) 
            funx1(2) = (u1(2)) - qVar(VVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)-ds)
            xStarX(2) = xCart(2) - tf*(u1(2))
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarX(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarX(:))
            end if
            funx2(1) = (u1(1)-ds) - qVar(UVEL) 
            funx2(2) = (u1(2)) - qVar(VVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)+2.0_FP*ds) 
            xStarX(2) = xCart(2) - tf*(u1(2))
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarX(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarX(:))
            end if
            funx3(1) = (u1(1)+2.0_FP*ds) - qVar(UVEL) 
            funx3(2) = (u1(2)) - qVar(VVEL)

            xStarX(1) = xCart(1) - tf*(u1(1)-2.0_FP*ds) 
            xStarX(2) = xCart(2) - tf*(u1(2))
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarX(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarX(:))
            end if
            funx4(1) = (u1(1)-2.0_FP*ds) - qVar(UVEL) 
            funx4(2) = (u1(2)) - qVar(VVEL)

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)+ds)
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarY(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarY(:))
            end if
            funy1(1) = (u1(1)) - qVar(UVEL) 
            funy1(2) = (u1(2)+ds) - qVar(VVEL) 

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)-ds)
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarY(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarY(:))
            end if
            funy2(1) = (u1(1)) - qVar(UVEL) 
            funy2(2) = (u1(2)-ds) - qVar(VVEL) 

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)+2.0_FP*ds)
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarY(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarY(:))
            end if
            funy3(1) = (u1(1)) - qVar(UVEL) 
            funy3(2) = (u1(2)+2.0_FP*ds) - qVar(VVEL) 

            xStarY(1) = xCart(1) - tf*(u1(1))
            xStarY(2) = xCart(2) - tf*(u1(2)-2.0_FP*ds)
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStarY(:),funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStarY(:))
            end if
            funy4(1) = (u1(1)) - qVar(UVEL) 
            funy4(2) = (u1(2)-2.0_FP*ds) - qVar(VVEL) 

            ! second order accurate first order derivatives
            !J(1,1) = (funx1(1)-funx2(1))/(2.0*ds)  !fu
            !J(1,2) = (funy1(1)-funy2(1))/(2.0*ds)  !fv
            !J(2,1) = (funx1(2)-funx2(2))/(2.0*ds)  !gu
            !J(2,2) = (funy1(2)-funy2(2))/(2.0*ds)  !gv

            ! fourth order accurate first derivatives
            J(1,1) = (8.0_FP*funx1(1)-8.0_FP*funx2(1)-funx3(1)+funx4(1))/(12.0*ds)  !fu
            J(1,2) = (8.0_FP*funy1(1)-8.0_FP*funy2(1)-funy3(1)+funy4(1))/(12.0*ds)  !fv
            J(2,1) = (8.0_FP*funx1(2)-8.0_FP*funx2(2)-funx3(2)+funx4(2))/(12.0*ds)  !gu
            J(2,2) = (8.0_FP*funy1(2)-8.0_FP*funy2(2)-funy3(2)+funy4(2))/(12.0*ds)  !gv

            detJ = J(1,1)*J(2,2)-J(1,2)*J(2,1)   
            if ( abs(detJ) < tol ) exit
            du(1) = -(func(1)*J(2,2)-func(2)*J(1,2))/detJ
            du(2) = -(-func(1)*J(2,1)+func(2)*J(1,1))/detJ

            ! Newton Raphson iteration
            u2 = u1 + du ! new velocity
            xStar = xCart - tf*u2
            if ( present(funcN) ) then
                qVar = evalFunction(nDim,nEqns,xStar,funcName=funcN)
            else
                qVar = evalFunction(nDim,nEqns,xStar)
            end if
            u1 = qVar(UVEL:VVEL)
            if ( abs(du(1)) <= tol .and. abs(du(2)) <= tol ) exit 
            !if ( abs(du(1)) < ds .and. abs(du(2)) < ds ) exit 

        end do
        xStar = xCart - tf*u1
        if ( present(funcN) ) then
            qVar = evalFunction(nDim,nEqns,xStar,funcName=funcN)
        else
            qVar = evalFunction(nDim,nEqns,xStar)
        end if

        ! Converged solutions
        burgSol(1) = qVar(1) 
        burgSol(2) = qVar(2)
        burgSol(3) = qVar(3)
        burgSol(4) = qVar(4)
  
    else
        ! 1D 
        ! initial guesses
        qVar = qSim
        u1 = qVar(UVEL)
        ! iteration for velocities
        do iter = 1, nIter 

            xStar(1) = xCart(1) - tf*u1(1)
            ! step size based on data
            !ds = sqrt(abs(tol))*max(abs(xStar(1)),1.0_FP)

            ! call function to iterate
            qVar = evalFunction(nDim,nEqns,xStar(1))
            func = u1 - qVar(UVEL) 

            ! calculate derivatives of function
            xStarX(1) = xCart(1) - tf*(u1(1)+ds) 
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx1(1) = (u1(1)+ds) - qVar(UVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)-ds)
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx2(1) = (u1(1)-ds) - qVar(UVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)+2.0_FP*ds) 
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx3(1) = (u1(1)+2.0_FP*ds) - qVar(UVEL) 

            xStarX(1) = xCart(1) - tf*(u1(1)-2.0_FP*ds) 
            qVar = evalFunction(nDim,nEqns,xStarX(:))
            funx4(1) = (u1(1)-2.0_FP*ds) - qVar(UVEL) 

            ! fourth order accurate first derivatives
            J(1,1) = (8.0_FP*funx1(1)-8.0_FP*funx2(1)-funx3(1)+funx4(1))/(12.0*ds)  !fu

            ! function derivative
            detJ = J(1,1)  !fx
            if ( detJ < tol ) exit
            du(1) = -func(1)/detJ

            ! Newton Raphson iteration
            u2 = u1 + du ! new velocity
            xStar(1) = xCart(1) - tf*u2(1)
            qVar = evalFunction(nDim,nEqns,xStar(1))
            u1 = qVar(UVEL)
            if ( abs(du(1)) < tol ) exit
            
        end do
        xStar(1) = xCart(1) - tf*u1(1)
        qVar = evalFunction(nDim,nEqns,xStar(1))

        ! Converged solutions
        burgSol(1) = qVar(1)
        burgSol(2) = qVar(2)
    
    end if

end function burgSol
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return acoustic simple wave exact solution given coordinate and time
!>  "I do like CFD, vol 1" pp. 180
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 December 2015 - Initial creation
!>
function simpWaveSol(nDim,nEqns,finalSol,xCart,tf)

    use solverVars, only: pi, eps, inLength
    use physics, only: gam, isentropicConst, soundSpeed

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    real(FP), intent(in) :: xCart(nDim),        & !< physical coordinates
                            finalSol(nEqns),    & !< final solution
                            tf                    !< final time

    ! Function variable
    real(FP) :: simpWaveSol(nEqns)
  
    ! Local variables
    character(inLength) :: funcname

    real(FP) :: aInf,           & !< freestream speed of sound
                rhoInf,         & !< freestream density
                pInf,           & !< freestream pressure
                MInf,           & !< freestream Mach number
                uaInf,          & !< velocity over aInf
                a,              & !< speed of sound
                tempSol(nEqns), & !< temporary solution
                VBurg(nEqns)      !< solution to simple wave Burgers' equation

    ! Simple wave solution 
    ! trace characteristic to find Burgers' solution
    funcname = 'burgers'
    a = soundSpeed(nEqns,finalSol(:))
    tempSol(1) = finalSol(1)
    tempSol(2) = finalSol(2)+a
    tempSol(3) = finalSol(3)
    tempSol(4) = finalSol(4)

    rhoInf = 1.0_FP
    pInf = isentropicConst*rhoInf**(gam)
    aInf = sqrt(gam*pInf/rhoInf)
    MInf = 0.0_FP
    simpWaveSol(:) = 0.0_FP

    ! u+a
    VBurg(:) = burgSol(nDim,nEqns,tempSol(:), &
        xCart(:),tf,funcN=funcname)
    ! this is u/aInf
    uaInf = 2.0_FP/(gam+1.0_FP)*(VBurg(2)/aInf - &
        (1.0_FP - (gam-1.0_FP)/2.0_FP*MInf)) 

    ! rho for u+a
    simpWaveSol(1) = rhoInf*(1.0_FP + 0.5_FP*(gam-1.0_FP)* &
                      (uaInf-MInf))**(2.0_FP/(gam-1.0_FP))
    ! rho for u-a
    !simpWaveSol(1) = rhoInf*(1.0_FP - 0.5_FP*(gam-1.0_FP)* &
    !                  (uaInf-MInf))**(2.0_FP/(gam-1.0_FP))
    !! rho when gam = 1
    !simpWaveSol(1) = rhoInf*(exp(uaInf-MInf))
    simpWaveSol(2) = aInf*uaInf
    simpWaveSol(3) = 0.0_FP
    simpWaveSol(4) = pInf*(1.0_FP + 0.5_FP*(gam-1.0_FP)* &
                      (uaInf-MInf))**(2.0_FP*gam/(gam-1.0_FP))
    ! p for u-a
    !simpWaveSol(4) = pInf*(1.0_FP - 0.5_FP*(gam-1.0_FP)* &
    !                  (uaInf-MInf))**(2.0_FP/(gam-1.0_FP))

end function simpWaveSol
!-------------------------------------------------------------------------------
end module iterativeFunctions
!-------------------------------------------------------------------------------
