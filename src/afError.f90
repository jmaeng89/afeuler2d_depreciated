!-------------------------------------------------------------------------------
!> @purpose 
!>  Stand-alone error analysis program for use with active-flux solver.
!>  Assumes first and last iterations in file are at the same location
!>
!> @history
!>  18 April 2012 - Initial creation (Eymann)
!>  8 October 2013 - Added error data writing option (Maeng)
!>  22 January 2015 - Burgers' equations (Maeng)
!>  30 January 2015 - pressureless Euler equations (Maeng)
!>  23 March 2015 - added a routine to evaluate error for 
!>                   conserved variables (Maeng)
!>  1 October 2015 - Euler equations (Maeng)
!>
program afError

    use solverVars
    use update, only: eqFlag, ADVECTION_EQ, PLESSEULER_EQ, &
                      ISENTEULER_EQ, EULER_EQ, &
                      cellAvgN, primAvgN
    use meshUtil, only: nDim, nNodes, nEdges, nFaces, nCells, &
                        totalVolume, cellVolume, connect
    use mathUtil, only: numAverage
    use physics, only: gam, Rgas, prim2conserv
    use inputOutput, only: reconFile, reconFileName, getDriverFile, &
                           parseDriverFile, deallocateMeshVars, &
                           filePath, drvFileName, getIterNum, &
                           skipConnectivity, skipData, readData
    use postProc

    implicit none

    ! Local variables
    integer :: DOF,         & !< number of degrees of freedom 
               nPts,        & !< number of integration points
               nRec,        & !< number of reconstruction coef.
               nPtsQ,       & !< number of integration points, quadratic
               maxIter,     & !< maximum iteration number in file
               iPt,         &
               iEq,         &
               iErr,        & !< error flag
               iCell,       & !< cell index
               iter           !< iteration number

    real(FP) :: solTime       !< final solution time

    real(FP) :: h,          & !< mesh spacing
                area,       & !< domain area
                quad,       & !< quadrature sum
                errSum        !< sum of error terms

    real(FP), allocatable :: l1err(:),          & !< L1 error of cell reconstruction
                             l2err(:),          & !< L2 error of cell reconstruction
                             conservL1err(:),   & !< L1 error of conserved variables
                             conservL2err(:),   & !< L2 error of conserved variables
                             primL1err(:),      & !< L1 error of primitive cell average
                             primL2err(:),      & !< L2 error of primitive cell average
                             cellAvgRef(:,:),   & !< Reference cell averages at final time
                             primAvgRef(:,:),   & !< Reference primitive averages at final time
                             dunPt0(:,:,:),     & !< initial value at cell Dunavant point
                             dunPt(:,:,:),      & !< final value at cell Dunavant point
                             quadPt0(:,:,:),    & !< initial value at quadratic poly. point
                             quadPt(:,:,:),     & !< final value at quadratic poly. point
                             quadPtRef(:,:,:),  & 
                             dunPtRef(:,:,:),   & !< Reference at cell Dunavant point
                             conservDunPtRef(:,:,:) !< Reference conserved value at cell Dunavant point

    ! fast vortex entropy and velocity magnitude error
    real(FP) :: rhoInf, aInf, pInf

    real(FP), allocatable :: quadPtNormalize(:,:,:),  & 
                             quadPt0Normalize(:,:,:), &
                             l1errTemp(:),        &
                             l2errTemp(:)         

    real(FP), allocatable :: entropy0(:,:,:),     & !< entropy at initial 
                             entropy(:,:,:),      & !< entropy at final 
                             velMag0(:,:,:),      & !< velocity magnitude at initial
                             velMag(:,:,:),       & !< velocity magnitude at final
                             l1errVelMag(:),      & !< L1 error of entropy
                             l2errVelMag(:),      & !< L2 error of velocity magnitude
                             l1errEntropy(:),     & !< L1 error of entropy
                             l2errEntropy(:)        !< L2 error of velocity magnitude

    ! vortex related error
    real(FP), allocatable :: errorAmp(:),   & !< amplitude error  
                             errorPhs(:),   & !< phase error
                             qVort0(:),     & !< initial value at extremum point
                             qVort(:),      & !< final value at extremumpoint
                             xMin0(:),      & !< initial minimum value location 
                             xMin(:)          !< final minimum value location

    character(120) :: varfmt,            &
                      errFileName,       &
                      errFileName2,      &
                      errFileName3,      &
                      errFileName4,      &
                      cErrFileName

    integer :: errFile = 180,   & !< error data file unit
               errFile2 = 181,  &
               errFile3 = 182,  &
               errFile4 = 183,  &
               cErrFile = 184     !< conservative error data file unit 

    ! get connectivity information/allocate data
    call calcConstants
    call getDriverFile
    call parseDriverFile 
    call connect

    ! Open a file to store error data
    errFileName = 'errData.dat'
    errFileName = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName))
    open( unit = errFile, file = errFileName, form = 'formatted', &
          access = 'append', iostat = iErr  )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(errFileName))
        stop
    end if
    cerrFileName = 'conserverrData.dat'
    cErrFileName = trim(adjustl(filePath))//'/'//trim(adjustl(cerrFileName))
    open( unit = cErrFile, file = cErrFileName, form = 'formatted', &
          access = 'append', iostat = iErr  )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(cErrFileName))
        stop
    end if

    !! Vortex Error -----------------------------------------------------------------
    !errFileName2 = 'vortexExtremaData.dat'
    !errFileName2 = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName2))
    !open( unit = errFile2, file = errFileName2, form = 'formatted', &
    !      access = 'append', iostat = iErr  )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(errFileName2))
    !    stop
    !end if
    !errFileName3 = 'vortexErrorData.dat'
    !errFileName3 = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName3))
    !open( unit = errFile3, file = errFileName3, form = 'formatted', &
    !      access = 'append', iostat = iErr  )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(errFileName3))
    !    stop
    !end if
    !!errFileName4 = 'errData_normalized.dat'
    !!errFileName4 = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName4))
    !!open( unit = errFile4, file = errFileName4, form = 'formatted', &
    !!      access = 'append', iostat = iErr  )
    !!if ( iErr /= 0 ) then
    !!    write(*,*) 'ERROR: opening ', trim(adjustl(errFileName4))
    !!    stop
    !!end if
    !! temp -----------------------------------------------------------------

    ! Get number of iterations in file
    open( unit = reconFile, file = trim(adjustl(reconFileName)), form = 'unformatted', &
         access = 'stream', status = 'OLD', iostat = iErr )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(reconFileName))
        stop
    end if
    maxIter = getIterNum(reconFile)

    ! set number of integration points
    if ( nDim == 2 ) then
        nPts = 6    !< for conservative averaged quantity
        nPtsQ = 6   !< for quadratic reconstruction coefficients
    else
        nPts = 4
        nPtsQ = 3   !< for quadratic reconstruction coefficients
    end if

    allocate( cellAvgRef(nEqns,nCells), primAvgRef(nEqns,nCells), &
              dunPt0(nEqns,nPts,nCells), dunPt(nEqns,nPts,nCells), &
              dunPtRef(nEqns,nPts,nCells), &
              quadPt0(nEqns,nPtsQ,nCells), quadPt(nEqns,nPtsQ,nCells), &
              quadPtRef(nEqns,nPtsQ,nCells), &
              conservDunPtRef(nEqns,nPts,nCells), &
              l1err(nEqns), l2err(nEqns), &
              l1errTemp(nEqns), l2errTemp(nEqns), &
              conservL1err(nEqns), conservL2err(nEqns), &
              primL1err(nEqns), primL2err(nEqns), &
              qVort0(nEqns), qVort(nEqns), xMin0(nDim), xMin(nDim), &
              errorAmp(nEqns), errorPhs(nEqns) )

    allocate ( entropy0(1,nPtsQ,nCells), entropy(1,nPtsQ,nCells), &
               velMag0(1,nPtsQ,nCells), velMag(1,nPtsQ,nCells), & 
               quadPt0Normalize(nEqns,nPtsQ,nCells), &
               quadPtNormalize(nEqns,nPtsQ,nCells), &
               l1errVelMag(1), l2errVelMag(1), &
               l1errEntropy(1), l2errEntropy(1) )

    ! store values at integration points for initial data
    rewind(reconFile)

    ! iteration 0 - output 1
    call skipConnectivity(reconFile)
    call readData(reconFile)
    ! initial data
    dunPt0(:,:,:) = intData(nDim,nEqns,nPts,nCells) 
    quadPt0(:,:,:) = quadIntData(nDim,nEqns,nPtsQ,nCells)
    !call vortexExtrema(nDim,nEqns,nCells,qVort0,xMin0) ! initial vortex extrema quantities
    
    ! skip to last iteration in file (maxIter-2 since read iter 0 already)
    do iter = 1, maxIter-2  
        iErr = skipData(reconFile)
    end do

    ! final iteration - final output
    call readData(reconFile,iter,solTime)
    write(*,*) 'solTime:', solTime

    dunPt(:,:,:) = intData(nDim,nEqns,nPts,nCells) ! finial data
    quadPt(:,:,:) = quadIntData(nDim,nEqns,nPtsQ,nCells)
    !call vortexExtrema(nDim,nEqns,nCells,qVort,xMin) !< final time vortex extrema quantity

    ! Reference numerical/analytic solution to compare numerical solution
    !dunPtRef(:,:,:) = iterateSol(nDim,nEqns,nPts,nCells,dunPt,solTime) ! iterated solution data
    dunPtRef(:,:,:) = dunPt0(:,:,:) ! initial data 
    !quadPtRef(:,:,:) = iterateSol(nDim,nEqns,nPtsQ,nCells,quadPt,solTime,nPtsQ)  ! lagrange points
    quadPtRef(:,:,:) = quadPt0(:,:,:) ! intial data

    ! compute error in reconstructions in Lagrange points at final step
    l1err(:) = cellReconError(nDim,nEqns,nPtsQ,nCells,quadPt,quadPtRef,1.0,nPtsQ)
    l2err(:) = cellReconError(nDim,nEqns,nPtsQ,nCells,quadPt,quadPtRef,2.0,nPtsQ)

    ! calculate reference cell average
    do iCell = 1,nCells
        do iPt = 1,nPts
            ! assume quadratic variation of conserved variables
            if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or.&
                 ( eqFlag == PLESSEULER_EQ ) ) then 
                conservDunPtRef(:,iPt,iCell) = prim2conserv(nDim,nEqns,dunPtRef(:,iPt,iCell))
            else
                conservDunPtRef(:,iPt,iCell) = dunPtRef(:,iPt,iCell)
            end if
        end do

        do iEq = 1,nEqns
            cellAvgRef(iEq,iCell) = numAverage(nDim,nPts,conservDunPtRef(iEq,:,iCell))
            primAvgRef(iEq,iCell) = numAverage(nDim,nPts,dunPtRef(iEq,:,iCell))
        end do
    end do
    ! compute error in conservative averages
    conservL1err(:) = cellAvgError(nDim,nEqns,nCells,cellAvgN,cellAvgRef,1.0)
    conservL2err(:) = cellAvgError(nDim,nEqns,nCells,cellAvgN,cellAvgRef,2.0)
    primL1err(:) = cellAvgError(nDim,nEqns,nCells,primAvgN,primAvgRef,1.0)
    primL2err(:) = cellAvgError(nDim,nEqns,nCells,primAvgN,primAvgRef,2.0)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Vortex Error -----------------------------------------------------------------
    !pInf = 1.0e+05
    !rhoInf = pInf/(Rgas*300.0_FP) 
    !aInf = sqrt(gam*Rgas*300.0_FP)
    !do iCell = 1,nCells
    !    do iPt = 1,nPtsQ
    !        !entropy0(1,iPt,iCell) = log(quadPt0(4,iPt,iCell)) - gam*log(quadPt0(1,iPt,iCell))
    !        !entropy(1,iPt,iCell) = log(quadPt(4,iPt,iCell)) - gam*log(quadPt(1,iPt,iCell))
    !        entropy0(1,iPt,iCell) = Rgas/(gam-1.0_FP)*log(quadPt0(4,iPt,iCell)/(quadPt0(1,iPt,iCell))**gam)
    !        entropy(1,iPt,iCell) = Rgas/(gam-1.0_FP)*log(quadPt(4,iPt,iCell)/(quadPt(1,iPt,iCell))**gam)
    !        velMag0(1,iPt,iCell) = sqrt(quadPt0(2,iPt,iCell)*quadPt0(2,iPt,iCell) + &
    !                                  quadPt0(3,iPt,iCell)*quadPt0(3,iPt,iCell)) 
    !        velMag(1,iPt,iCell) = sqrt(quadPt(2,iPt,iCell)*quadPt(2,iPt,iCell) + &
    !                                 quadPt(3,iPt,iCell)*quadPt(3,iPt,iCell)) 
    !        !quadPtNormalize(1,iPt,iCell) = quadPt(1,iPt,iCell)/rhoInf
    !        !quadPtNormalize(2,iPt,iCell) = quadPt(2,iPt,iCell)/aInf
    !        !quadPtNormalize(3,iPt,iCell) = quadPt(3,iPt,iCell)/aInf
    !        !quadPtNormalize(4,iPt,iCell) = quadPt(4,iPt,iCell)/pInf
    !        !quadPt0Normalize(1,iPt,iCell) = quadPt0(1,iPt,iCell)/rhoInf
    !        !quadPt0Normalize(2,iPt,iCell) = quadPt0(2,iPt,iCell)/aInf
    !        !quadPt0Normalize(3,iPt,iCell) = quadPt0(3,iPt,iCell)/aInf
    !        !quadPt0Normalize(4,iPt,iCell) = quadPt0(4,iPt,iCell)/pInf
    !    end do
    !end do
    !l1errVelMag(:) = cellReconError(nDim,1,nPtsQ,nCells,velMag,velMag0,1.0,nPtsQ)
    !l2errVelMag(:) = cellReconError(nDim,1,nPtsQ,nCells,velMag,velMag0,2.0,nPtsQ)
    !l1errEntropy(:) = cellReconError(nDim,1,nPtsQ,nCells,entropy,entropy0,1.0,nPtsQ)
    !l2errEntropy(:) = cellReconError(nDim,1,nPtsQ,nCells,entropy,entropy0,2.0,nPtsQ)
    !!l1errTemp(:) = cellReconError(nDim,nEqns,nPtsQ,nCells,quadPtNormalize,quadPt0Normalize,1.0,6)
    !!l2errTemp(:) = cellReconError(nDim,nEqns,nPtsQ,nCells,quadPtNormalize,quadPt0Normalize,2.0,6)
    !! temp -----------------------------------------------------------------
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !write(errFile,*) '% nCell, DOF, 1/sqrt(DOF), L1err, L2err' 
    !write(cErrFile,*) '% nCell, DOF, 1/sqrt(DOF), L1err, L2err' 
    if ( nDim == 2 ) then
        ! calculate DOF and h
        DOF = nCells+nNodes+nEdges
        h = sqrt(1.0_FP/real(DOF,FP))

        write(varfmt,'(a,i0,a)') '(i7,i7,e24.15e2,', 2*nEqns,'e24.15e2,a80)'
        write(*,*) 'primitive variables'
        write(*,varfmt) nCells, DOF, h, l1err,l2err
        write(errFile,varfmt) nCells, DOF, h, &
                              l1err,l2err,'%'//trim(adjustl(drvFileName))

        write(*,*) 'conserved variables'
        write(*,varfmt) nCells, DOF, h, conservL1err,conservL2err
        write(cErrFile,varfmt) nCells, DOF, h, & 
                               conservL1err,conservL2err,'%'//trim(adjustl(drvFileName))   

        !write(*,*) 'primitive cell average'
        !write(*,varfmt) nCells, DOF,  &
        !                          sqrt(1.0_FP/real(nCells+nNodes+nEdges)), &
        !                          primL1err,primL2err

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! write VORTEX related error  
        !write(errFile2,'(2i7,e24.15e2,12e24.15e2)') nCells, DOF, h, &
        !                                    xMin0, qVort0, xMin, qVort
        !write(errFile3,'(2i7,e24.15e2,6e24.15e2)') nCells, DOF, h, &
        !    l1err(1), l1errVelMag, l1errEntropy, l2err(1), l2errVelMag, l2errEntropy
        !!write(errFile4,varfmt) nCells, DOF, h, &
        !!    l1errTemp, l2errTemp, '%'//trim(adjustl(drvFileName))   
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    else
        ! calculate DOF and h
        DOF = nCells+nNodes
        h = 1.0_FP/real(DOF,FP)

        write(varfmt,'(a,i0,a)') '(i7,i7,e24.15e2,', 2*nEqns, 'e24.15e2,a40)'
        write(*,*) 'primitive variables'
        write(*,varfmt) nCells, DOF, h, l1err,l2err
        write(errFile,varfmt) nCells, DOF, h, &
                              l1err,l2err,'%'//trim(adjustl(drvFileName))  
        write(*,*) 'conserved variables'
        write(*,varfmt) nCells, DOF, h, conservL1err,conservL2err
        write(cErrFile,varfmt) nCells, DOF, h, &
                               conservL1err,conservL2err,'%'//trim(adjustl(drvFileName))  
    end if

    ! close files after writing
    close(reconFile)
    close(errFile)
    close(errFile2)
    close(errFile3)
    close(errFile4)
    close(cErrFile)

    deallocate( dunPt0, dunPt, dunPtRef, &
                quadPt0, quadPt, quadPtRef, &
                cellAvgRef, primAvgRef, conservDunPtRef, & 
                l1errTemp, l2errTemp, &
                l1err, l2err, conservL1err, conservL2err, &
                primL1err, primL2err, &
                qVort0, qVort, xMin0, xMin, errorAmp, errorPhs )

    deallocate ( entropy0, entropy, &
                 velMag0, velMag, & 
                 quadPt0Normalize, &
                 quadPtNormalize, &
                 l1errVelMag, l2errVelMag, &
                 l1errEntropy, l2errEntropy )

    call deallocateMeshVars

end program afError
!-------------------------------------------------------------------------------
