!-------------------------------------------------------------------------------
!> @purpose 
!>  Defines variables shared throughout the active flux solver
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 March 2012 - Initial creation
!>  5 September 2013 - Added constant linear advection speed, waveSpeed (Maeng)
!>
module solverVars

    implicit none

    integer, parameter :: SHORTINT = selected_int_kind(6),  & !< int32, default
                          LONGINT = selected_int_kind(15)     !< int64

    integer, parameter :: QP = selected_real_kind(33,4931), & !< quad precision
                          DP = selected_real_kind(15,307),  & !< double precision
                          SP = selected_real_kind(6,37),    & !< single precision
                          FP = DP                             !< floating-point precision

    integer, parameter  :: inLength = 8,    & !< length of an input string
                           iLeft = 1,       & !< left array index
                           iRight = 2         !< right array index

    real(FP), parameter :: pi = 3.1415926535897932384626433832795028841971_FP !< pi

    character(inLength) :: govEqn,          & !< governing equation
                           initSolnType       !< initial solution to apply

    integer :: nEqns = 4       !< number of equations in system

    real(FP) :: eps            !< machine zero

    ! time step related variables
    integer :: nIter           !< number of iterations

    real(FP) :: dtIn,        & !< global time step
                dtMax,       & !< global max. time step
                cfl,         & !< global cfl number
                tFinal,      & !< final simulation time
                tSim = 0.0_FP  !< simulation time

    ! linear advection wave speed    
    real(FP), dimension(2) :: waveSpeed = (/1.0_FP, 0.0_FP/)

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate various program constants to precision of machine
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  25 January 2012
!>
subroutine calcConstants

    implicit none

    ! Local variables
    real(FP) :: del = 1.0_FP !< quantity used to determine precision

    eps = 1.0_FP

    ! machine precision
    do
        eps = 0.5_FP*eps
        del = eps + 1.0_FP
        if ( del > 1.0_FP ) then
            continue
        else
            eps = abs(2.0_FP*eps)
            exit
        end if
    end do
end subroutine calcConstants
!-------------------------------------------------------------------------------
!> @purpose 
!>  Truncate trailing decimal point values
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 November 2014
!>
function truncDecPts(q,f)

    implicit none
    
    !< Interface variables
    real(FP) :: q,  & !< real type data that needs conditioning
                f     !< factor beyond which decimal points are not needed

    !< Local variable
    integer(LONGINT) :: qInt  
    
    !< Function variable
    real(FP) :: truncDecPts

    qInt = nint((q)/abs(f), LONGINT)

    truncDecPts = dble(qInt*abs(f))

end function truncDecPts
!-------------------------------------------------------------------------------
end module solverVars
!-------------------------------------------------------------------------------
