!-------------------------------------------------------------------------------
!> @purpose 
!>  Start-up and shut-down routines
!>
!> @author
!>  Timothy A. Eymann
!>
module startStop

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Start simulation
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>  14 May 2013 - Adapt for Euler re-write
!>  27 April 2016 - Time control module implemented (Maeng)
!>
subroutine startUp

    use solverVars
    use meshUtil, only: nDim, nCells, connect, nEdges, &
                        cellVolume 
    use inputOutput, only: getDriverFile, parseDriverFile, parseBCfile, &
                           openFiles, histFile, bcFileName, &
                           solInput, inputFileName
    use update
    use timeControl
    use reconstruction
    !use boundaryConditions, only: setAcNodeEdgeBC

    implicit none

    ! Local variables
    !real(FP) :: dtMax !< maximum time step

    integer :: iCell,   & !< cell index
               iFace      !< face index
    real(FP) :: temp
    
    ! Set constants to precision of machine
    call calcConstants

    ! Gather inputs
    call getDriverFile
    call parseDriverFile ! initialize mesh variables

    ! Establish mesh connectivity and set edgeCoord values 
    call connect

    ! Flag boundary elements
    call parseBCfile

    !! set node/edge to acoustics cell connectivity information
    !! set periodic node/edge to acoustics cell connectivity information
    !call setAcNodeEdge         ! sets only interior nodes/edges
    !call setAcNodeEdgeBC       ! sets boundary conditions for nodes/edges connectivity, use with setAcNodeEdge

    ! Initialize solution values
    if ( .not. solInput ) then
        ! Initialize with specified analytic function
        call initPointVals
        call initCellAverages(nDim,nEqns)
    else
        ! Initialize with existing data
        nodeData = nodeDataN
        edgeData = edgeDataN
        cellAvg = cellAvgN
        primAvg = primAvgN
    end if

    ! Initialize time step parameters 
    call initTimeStep

    if ( dtIn > dtMax ) then
        write(*,*)
        write(*,*) 'ERROR: Time step is too large.'
        write(*,'(7x,a,e12.4e2)') '       dtMax: ', dtMax
        write(*,'(7x,a,e12.4e2)') '        dtIn: ', dtIn
        stop
    else
        !write(*,'(7x,a,e12.4e2)') '       dtMax: ',dtMax
        write(*,'(7x,a,e12.4e2)') '        dtIn: ', dtIn
        write(*,'(7x,a,e12.4e2)') '  Final time: ', tFinal
        write(*,'(7x,a,f8.4)')    '         CFL: ', cfl  ! dtIn/dtMax
        write(*,'(7x,a,i0)')      '       nIter: ', nIter
        write(*,*)
    end if

    if ( .not. solInput ) then
        write(*,'(2a)') '   Initial solution: ', trim(adjustl(initSolnType))
    else
        write(*,'(3a)') '   Continued from "', trim(adjustl(inputFileName)), '"'
        write(*,'(2a)') '   Initial solution: ', trim(adjustl(initSolnType))
    end if

    write(*,'(2a)') ' Governing equation: ', trim(adjustl(govEqn))
    write(*,'(2a)') ' Boundary file name: ', trim(adjustl(bcFileName))
    write(*,*)

    ! Open files for output
    call openFiles

    ! write header to histFile
    if ( .not. solInput ) then
        write(histFile,'(2a)') '#  Initial solution: ', trim(adjustl(initSolnType))
    else
        write(histFile,'(3a)') '#  Continued from "', trim(adjustl(inputFileName)), '"'
        write(histFile,'(2a)') '#  Initial solution: ', trim(adjustl(initSolnType))
    end if

    write(histFile,'(2a)') '#Governing equation: ', trim(adjustl(govEqn))
    write(histFile,'(a)') '# '
    !write(histFile,'(a,e12.4e2)') '#      dtMax: ', dtMax
    write(histFile,'(a,e12.4e2)') '#       dtIn: ', dtIn
    write(histFile,'(a,e12.4e2)') '# Final time: ', tFinal
    write(histFile,'(a,f8.4)')    '#        CFL: ', cfl !dtIn/dtMax
    write(histFile,'(a,i0)')      '#      nIter: ', nIter

end subroutine startUp
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gracefully shutdown code
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>
subroutine shutDown(t0,tf)

    use solverVars, only: nIter
    use inputOutput, only: histFile, deallocateMeshVars, closeFiles
    implicit none

    ! Interface variables
    real(FP), intent(in) :: t0, & !< starting time
                            tf    !< ending time


    ! write time to history file
    write(histFile,'(a)') '#'
    write(histFile,'(a,e24.16e2,a)' ) '# Total solution time: ',tf-t0,' seconds'
    write(histFile,'(a,e24.16e2,a)' ) '#  Time per iteration: ',(tf-t0)/dble(nIter), &
                                     ' seconds'
    
    ! free allocated memory
    call deallocateMeshVars

    ! Close output files
    call closeFiles

end subroutine shutDown
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize point values at nodes and edges
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>
subroutine initPointVals

    use solverVars, only: nEqns
    use analyticFunctions
    use meshUtil, only: nDim, nNodes, nEdges, &
                        nodeCoord, edgeCoord
    use update, only: nodeDataN, nodeData, &
                      edgeDataN, edgeData
    implicit none

    ! Local variables
    integer :: iNode,   & !< node index
               iEdge      !< edge index

    do iNode = 1, nNodes
        nodeDataN(:,iNode) = evalFunction(nDim,nEqns,nodeCoord(:,iNode))
    end do
    
    do iEdge = 1, nEdges
        edgeDataN(:,iEdge) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge))
    end do
   
    nodeData(:,:) = nodeDataN(:,:)
    edgeData(:,:) = edgeDataN(:,:)

end subroutine initPointVals
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize cell averages use numerical integration.  Code uses Dunavant
!>  points and weights.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation (Eymann)
!>  17 March 2015 - Primitive and conservative variables added (Maeng)
!>  15 April 2015 - Consistent intialization for primitive variables (Maeng)
!>
subroutine initCellAverages(nDim,nEqns)

    use update, only: cellAvg, cellAvgN, &
                      primAvg, primAvgN, &
                      eqFlag, EULER_EQ, ISENTEULER_EQ, PLESSEULER_EQ
    use meshUtil, only: nCells, nEdges, ref2cart, cellVolume, faceCells
    use mathUtil, only: dunavantLoc, gaussQuadLoc, numAverage, &
                        lagrangeIntLoc, lagrangeIntWt, numAverageLagrange
    use physics, only: prim2conserv
    use analyticFunctions
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    ! Local variables
    integer :: iEq,     & !< equatoin index
               nPts,    & !< number of integration points
               iCell,   & !< cell index
               lCell,   & !< cell index
               rCell,   & !< cell index
               iEdge,   & !< edge index
               iPoint     !< integration point

    real(FP), allocatable :: xi(:,:),       & !< evaluation coordinates (in reference space)
                             qPrim(:,:),    & !< sampled primitive variable vector
                             qCons(:,:)       !< sampled conserved state vector
    
    real(FP) :: x(nDim)   !< evaluation points in Cartesian space

    if ( nDim == 2 ) then
        nPts = 7  ! for 3rd order integration using symmetric quadrature
        !nPts = 6  
    else
        nPts = 5
    end if

    allocate( xi(nDim,nPts), qPrim(nEqns,nPts), qCons(nEqns,nPts) )
    xi = 0.0_FP

    ! Keep dimension check outside of cell loop for efficiency
    if ( nDim == 1 ) then
        xi(1,:) = gaussQuadLoc(nPts)
        do iCell = 1, nCells
            qPrim = 0.0_FP
            qCons = 0.0_FP
            do iPoint = 1, nPts
                x(:) = ref2cart(nDim,iCell,0.5_FP*(xi(:,iPoint)+1.0_FP))
                qPrim(:,iPoint) = evalFunction(nDim,nEqns,x(:))
                if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or. &
                     ( eqFlag == PLESSEULER_EQ ) ) then 
                    ! conserved cell variables
                    qCons(:,iPoint) = prim2conserv(nDim,nEqns,qPrim(:,iPoint)) 
                else
                    ! primitive variables for non-Euler system
                    qCons(:,iPoint) = qPrim(:,iPoint)
                end if
            end do
            do iEq = 1, nEqns
                ! conserved cell state variables
                cellAvgN(iEq,iCell) = numAverage(nDim,nPts,qCons(iEq,:))
                primAvgN(iEq,iCell) = numAverage(nDim,nPts,qPrim(iEq,:))
            end do
        end do
    else
        !xi(:,:) = dunavantLoc(nPts) ! dunavant points for high-order integration
        xi(:,:) = lagrangeIntLoc(nPts) !< nPts=6 gives 2nd order integration
        do iCell = 1, nCells
            qPrim = 0.0_FP
            qCons = 0.0_FP
            do iPoint = 1, nPts
                x(:) = ref2cart(nDim,iCell,xi(:,iPoint))
                qPrim(:,iPoint) = evalFunction(nDim,nEqns,x(:))
                if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or.&
                     ( eqFlag == PLESSEULER_EQ ) ) then 
                    ! conserved cell state variables
                    qCons(:,iPoint) = prim2conserv(nDim,nEqns,qPrim(:,iPoint))
                else
                    ! primitive variables for non-Euler system
                    qCons(:,iPoint) = qPrim(:,iPoint)
                end if
            end do
            do iEq = 1, nEqns
                ! 2nd order accuarate average calculation
                cellAvgN(iEq,iCell) = numAverageLagrange(nDim,nPts,qCons(iEq,:))
                primAvgN(iEq,iCell) = numAverageLagrange(nDim,nPts,qPrim(iEq,:))
            end do
            !do iEq = 1, nEqns
            !    ! high order accuarate average calculation - using Dunavant
            !    cellAvgN(iEq,iCell) = numAverage(nDim,nPts,qCons(iEq,:))
            !    primAvgN(iEq,iCell) = numAverage(nDim,nPts,qPrim(iEq,:))
            !end do
        end do

    end if

    deallocate( xi, qPrim, qCons )

    cellAvg(:,:) = cellAvgN(:,:) 
    primAvg(:,:) = primAvgN(:,:)

end subroutine initCellAverages
!-------------------------------------------------------------------------------
end module startStop
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Initialize edge averages. Evaluate the average in two elements that share 
!!>  the edge using tensor Gaussian quadrature.
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  11 February 2016 - Initial Creation
!!>  16 February 2016 - Boundary edge averages 
!!>
!subroutine initEdgeAverages(nDim,nEqns)
! 
!    use update, only: eqFlag, EULER_EQ, &
!                      ISENTEULER_EQ, PLESSEULER_EQ, &
!                      cellAvgN
!    use meshUtil, only: nCells, nEdges, cellFaces, &
!                        cellNodes, faceNodes, faceCells, &
!                        nodeCoord, edgeCoord, ref2cart, cellVolume
!    use mathUtil, only: gaussQuadLoc, gaussQuadWt, &
!                        bilinQuadriLoc, bilinQuadriJac, &
!                        sortQuadVertex
!    use physics, only: prim2conserv
!    use boundaryConditions, only: edgeBC, pEdgePair
!    use analyticFunctions
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,    & !< problem dimension
!                           nEqns      !< number of equations
!
!    ! Local variables
!    integer :: iEq,     & !< equatoin index
!               nPts,    & !< number of integration points
!               iCell,   & !< cell index
!               iFace,   &
!               iEdge,   &
!               jPoint,  & !< integration point
!               iPoint     !< integration point
!
!    integer :: lCell,   &
!               rCell,   &
!               lNode,   &
!               rNode,   &
!               glFace,  &
!               glNode,  &
!               grFace,  &
!               grNode,  &
!               pEdge
!
!    integer :: nodeOrder(4)
!
!    real(FP), allocatable :: xi(:,:),       & !< evaluation coordinates (in reference space)
!                             wt(:)            !< 
!
!    real(FP) :: qPrim(nEqns),      & !< sampled primitive variable vector
!                qCons(nEqns),      & !< sampled conserved state vector
!                qAvg(nEqns)
!    
!    real(FP) :: x(nDim),        & !<
!                xCart(nDim,4),  & !<
!                detJac,         &
!                jacobian(nDim,nDim),&
!                xVert(nDim,4),  & !< node locations, physical
!                xiQuad(nDim)      !< reference quadrature location in 2D
!
!    real(FP) :: area
!
!    nPts = 5  ! high order accurate integration points 
!
!    allocate( xi(nDim,nPts), wt(nPts) )
!    xi = 0.0_FP
!    xi(1,:) = gaussQuadLoc(nPts)
!    wt(:) = gaussQuadWt(nPts)
!
!    ! Keep dimension check outside of cell loop for efficiency
!    if ( nDim == 1 ) then
!        do iEdge = 1, nEdges
!            qPrim = 0.0_FP
!            qCons = 0.0_FP
!            qAvg = 0.0_FP
!            do iPoint = 1, nPts
!                x(:) = ref2cart(nDim,iCell,0.5_FP*(xi(:,iPoint)+1.0_FP))
!                qPrim(:) = evalFunction(nDim,nEqns,x(:))
!                if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or. &
!                     ( eqFlag == PLESSEULER_EQ ) ) then 
!                    ! conserved cell variables
!                    qCons(:) = prim2conserv(nDim,nEqns,qPrim(:)) 
!                else
!                    ! primitive variables for non-Euler system
!                    qCons(:) = qPrim(:)
!                end if
!                qAvg(:) = qAvg(:) + (0.5_FP*wt(iPoint))*qCons(:)
!            end do
!            edgeAvgN(:,iEdge) = qAvg(:)
!        end do
!    else
!
!        do iEdge = 1, nEdges
!            lCell = faceCells(1,iEdge)
!            rCell = abs(faceCells(2,iEdge)) ! ghost cell or physical periodic cell
!            edgeAvgN(:,iEdge) = (cellAvgN(:,lCell)*cellVolume(lCell) + &
!                        cellAvgN(:,rCell)*cellVolume(rCell)) / &
!                        (cellVolume(lCell)+cellVolume(rCell))
!        end do
!
!        ! FIXME: why doesn't this work for unstructured?
!        !do iEdge = 1, nEdges
!        !    lCell = faceCells(1,iEdge) ! current cell
!        !    rCell = faceCells(2,iEdge) 
!        !    rNode = faceNodes(2,iEdge) 
!        !    lNode = faceNodes(1,iEdge) 
!
!        !    !if ( edgeBC(iEdge) == 0 ) then
!        !    if ( rCell > 0 ) then
!        !    ! only interior edges !
!        !        do iFace = 1,3
!        !            glFace = cellFaces(iFace,lCell)
!        !            if ( iEdge == glFace ) then
!        !                glNode = cellNodes(iFace,lCell)
!        !                exit    
!        !            end if
!        !        end do
!        !        do iFace = 1,3
!        !            grFace = cellFaces(iFace,rCell)
!        !            if ( iEdge == grFace ) then
!        !                grNode = cellNodes(iFace,rCell)
!        !                exit
!        !            end if
!        !        end do
!        !        ! assign quadrilateral nodes 
!        !        ! lCell centroid, lNode, rCell centroid, rNode
!        !        ! counter clock-wise ordering of physical coordinates
!        !        xCart(:,1) = nodeCoord(:,glNode) 
!        !        xCart(:,2) = nodeCoord(:,lNode) 
!        !        xCart(:,3) = nodeCoord(:,grNode) 
!        !        xCart(:,4) = nodeCoord(:,rNode) 
!        !        nodeOrder(:) = (/glNode,lNode,grNode,rNode/)
!        !        ! check the order of coordinates
!        !        call sortQuadVertex(xCart,nodeOrder)
!
!        !        qAvg(:) = 0.0_FP
!        !        jacobian(:,:) = 0.0_FP
!        !        area = 0.0_FP
!        !        do iPoint = 1,nPts
!        !            do jPoint = 1,nPts
!        !                xiQuad(1) = xi(1,iPoint)
!        !                xiQuad(2) = xi(1,jPoint)
!
!        !                x(:) = bilinQuadriLoc(nDim,xCart,xiQuad)
!        !                jacobian(:,:) = bilinQuadriJac(nDim,xCart,xiQuad)
!        !                detJac = abs(jacobian(1,1)*jacobian(2,2) - &
!        !                    jacobian(2,1)*jacobian(1,2))
!        !                qPrim(:) = evalFunction(nDim,nEqns,x(:))
!        !                if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or.&
!        !                     ( eqFlag == PLESSEULER_EQ ) ) then 
!        !                    qCons(:) = prim2conserv(nDim,nEqns,qPrim) 
!        !                else
!        !                    qCons(:) = qPrim(:) 
!        !                end if               
!
!        !                ! average in the reference coordinate
!        !                ! wt*wt*detJac == element area in physical coordinate
!        !                ! must divide qAvg by physical area in this case
!        !                qAvg(:) = qAvg(:) + &
!        !                    (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*detJac*qCons(:)
!        !                !! qAvg in reference normalized coordinate 
!        !                !qAvg(:) = qAvg(:) + &
!        !                !    (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*qCons(:)
!        !                area = area + wt(iPoint)*wt(jPoint)*detJac
!        !            end do
!        !        end do
!        !        qAvg(:) = qAvg(:)/area 
!
!        !    else
!        !        ! edge is a boundary edge. Approximate using extrapolated coordinate
!        !        do iFace = 1,3
!        !            glFace = cellFaces(iFace,lCell)
!        !            if ( iEdge == glFace ) then
!        !                glNode = cellNodes(iFace,lCell)
!        !                exit    
!        !            end if
!        !        end do
!        !        
!        !        ! assign quadrilateral nodes 
!        !        ! lCell centroid, lNode, rCell centroid, rNode
!        !        ! counter clock-wise ordering of physical coordinates
!        !        xCart(:,1) = nodeCoord(:,glNode) 
!        !        xCart(:,2) = nodeCoord(:,lNode) 
!        !        ! approximate one node coordinate since it is outside, 
!        !        ! it should really be of a ghost cell coordinate
!        !        xCart(:,3) = edgeCoord(:,iEdge) + &
!        !            (edgeCoord(:,iEdge)-nodeCoord(:,glNode))
!        !        xCart(:,4) = nodeCoord(:,rNode) 
!        !        nodeOrder(:) = (/glNode,lNode,1,rNode/)
!        !        ! check the order of coordinates
!        !        call sortQuadVertex(xCart,nodeOrder)
!
!        !        qAvg(:) = 0.0_FP
!        !        jacobian(:,:) = 0.0_FP
!        !        area = 0.0_FP
!        !        do iPoint = 1,nPts
!        !            do jPoint = 1,nPts
!        !                xiQuad(1) = xi(1,iPoint)
!        !                xiQuad(2) = xi(1,jPoint)
!
!        !                x(:) = bilinQuadriLoc(nDim,xCart,xiQuad)
!        !                jacobian(:,:) = bilinQuadriJac(nDim,xCart,xiQuad)
!        !                detJac = abs(jacobian(1,1)*jacobian(2,2) - &
!        !                    jacobian(2,1)*jacobian(1,2))
!        !                qPrim(:) = evalFunction(nDim,nEqns,x(:))
!        !                if ( ( eqFlag == EULER_EQ ) .or. ( eqFlag == ISENTEULER_EQ ) .or.&
!        !                     ( eqFlag == PLESSEULER_EQ ) ) then 
!        !                    qCons(:) = prim2conserv(nDim,nEqns,qPrim) 
!        !                else
!        !                    qCons(:) = qPrim(:) 
!        !                end if               
!
!        !                ! average in the reference coordinate
!        !                ! wt*wt*detJac == element area in physical coordinate
!        !                ! must divide qAvg by physical area in this case
!        !                qAvg(:) = qAvg(:) + &
!        !                    (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*detJac*qCons(:)
!        !                area = area + wt(iPoint)*wt(jPoint)*detJac
!        !            end do
!        !        end do
!        !        qAvg(:) = qAvg(:)/area 
!
!        !    end if
!        !    ! edge average is a third of the whole integral
!        !    edgeAvgN(:,iEdge) = qAvg(:)
!        !end do
!
!    end if
!
!    edgeAvg(:,:) = edgeAvgN(:,:)
!
!    deallocate( xi, wt )
!
!end subroutine initEdgeAverages
!-------------------------------------------------------------------------------

